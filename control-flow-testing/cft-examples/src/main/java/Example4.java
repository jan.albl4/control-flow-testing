import cz.zcu.kiv.cft.AnalyzedType;
import cz.zcu.kiv.cft.Configuration;
import cz.zcu.kiv.cft.UTGCFG;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.tools.OSPlatform;

public class Example4 {

    private static Configuration createConfiguration(){
        String s = OSPlatform.getPathSeparator();

        return new Configuration.Builder()
                .setAnalyzedType(AnalyzedType.METHOD)
                .setDetailedAnalysis(true)
                .setProjectPath(System.getProperty("user.dir") +s+ "cft-examples" +s+ "src" +s+ "main" +s+ "java" +s)
                .setPackagePath("example.classes")
                .setClassName("SecondExampleClass")
                .setMethodName("method")
                .setMaxDepth(2)
                .setMaximumLines(100000)
                .setOutputPath(System.getProperty("user.dir") +s+ "cft-examples" +s+ "src" +s+ "test" +s+ "java" +s)
                .setLoadNecessaryClasses(true)
                .build();
    }

    public static void main(String[] args) {

        Configuration configuration = createConfiguration();

        UTGCFG utgcfg = new UTGCFG.Builder().setConfiguration(configuration).build();

        ControlFlowAnalysisOutput cfo = utgcfg.run();

        //======== data =======
        String s = OSPlatform.getPathSeparator();
        String exportPath = System.getProperty("user.dir") +s+ "cft-examples" +s;

        OutputConverter.convertToJsonFile(cfo, exportPath + configuration.getClassName() + ".json");
        OutputConverter.convertToXmlFile(cfo, exportPath + configuration.getClassName() + ".xml");
    }

}
