import cz.zcu.kiv.cft.AnalyzedType;
import cz.zcu.kiv.cft.Configuration;
import cz.zcu.kiv.cft.UTGCFG;
import cz.zcu.kiv.cft.tools.OSPlatform;

import java.util.List;
import java.util.Map;

public class Example5 {

    private static Configuration createConfiguration(){
        String s = OSPlatform.getPathSeparator();

        return new Configuration.Builder()
                .setAnalyzedType(AnalyzedType.FOLDER)
                .setDetailedAnalysis(true)
                .setProjectPath(System.getProperty("user.dir") +s+ "cft-examples" +s+ "mortgage-calculator-class" +s)
                .setOutputPath(System.getProperty("user.dir") +s+ "cft-examples" +s+ "src" +s+ "test" +s+ "java" +s)
                .setLoadNecessaryClasses(true)
                .setMaxDepth(3)
                .build();
    }


    public static void main(String[] args) {

        Configuration configuration = createConfiguration();

        UTGCFG utgcfg = new UTGCFG.Builder().setConfiguration(configuration).build();

        utgcfg.run();
    }

}
