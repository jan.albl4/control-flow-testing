package example.classes.data;

public enum Ticket {
    STUDENT,
    NORMAL,
    GOLD,
    VIP;
}
