package example.classes;

import example.classes.data.Person;
import example.classes.data.Ticket;

public class FirstExampleClass {

    public FirstExampleClass(){}

    public boolean isOverweight(double weight, double height){
        double bmi = weight / (height * height);
        if (bmi > 25) {
            return true;
        }
        return false;
    }


    public boolean isVip(Person person) {
        return this.hasVIPTicket(person);
    }

    private boolean hasVIPTicket(Person person) {

        if(person != null){
            if(person.getTicket() == Ticket.VIP){
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean canEnter(Person person, int limitedAge, double limitedHeight){
        boolean canEnter = false;

        if(limitedAge > 0 && limitedAge < 150 && limitedHeight > 100 && limitedHeight < 250){
            if(person.getAge() > limitedAge){
                if(person.getHeight() > limitedHeight){
                    canEnter = true;
                }
            }
        }

        return canEnter;
    }
}
