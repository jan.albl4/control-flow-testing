package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class IntegerTest {

    private Analyzer analyzer;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.primitiv.easy")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();
    }

    @Test
    public void testEasy01(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyI", "easy01");

        Clazz clazz = cf.getClasses().get(0);

        assertEquals("TestEasyI", clazz.getClassName());
        assertNotNull(clazz.getMethods().get(0));
        assertNotNull(clazz.getMethods().get(0).getConditions());
        assertEquals(1, clazz.getMethods().get(0).getConditions().size());

        ConditionElement c  = clazz.getMethods().get(0).getConditions().get(0);

        assertTrue(((Condition)c).getConditionalExpressions().get(0).isInputDependent());
    }

    @Test
    public void testEasy02(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyI", "easy02");

        Clazz clazz = cf.getClasses().get(0);

        assertEquals(1, clazz.getMethods().get(0).getConditions().size());

        ConditionElement c  = clazz.getMethods().get(0).getConditions().get(0);

        Condition condition = (Condition)c;

        assertTrue(((Condition)c).getConditionalExpressions().get(0).isInputDependent());
        assertEquals("( ( ( 3.141592653589793 * b )  / a )  cmpg a ) >= 0", ((Condition)c).getConditionalExpressions().get(0).getExpression());

    }

    @Test
    public void testEasy03(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyI", "easy03");

        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz = cf.getClasses().get(0);

        assertEquals(2, clazz.getMethods().get(0).getConditions().size());

        ConditionElement c  = clazz.getMethods().get(0).getConditions().get(0);

        SwitchConditions switchConditions = (SwitchConditions)c;

        assertEquals(3, switchConditions.getBranches().size());
    }


    @Test
    public void testEasy04(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyI", "easy04");

        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz = cf.getClasses().get(0);

        assertEquals(2, clazz.getMethods().get(0).getConditions().size());

        ConditionElement c1  = clazz.getMethods().get(0).getConditions().get(0);
        ConditionElement c2  = clazz.getMethods().get(0).getConditions().get(1);

        Condition condition1 = (Condition) c1;
        Condition condition2 = (Condition) c2;

        assertEquals(0, condition1.getTrueBranch().size());
        assertEquals(0, condition1.getFalseBranch().size());

        assertEquals(0, condition2.getTrueBranch().size());
        assertEquals(0, condition2.getFalseBranch().size());
    }



}
