package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ObjectTest {

    private Analyzer analyzer;

    private DataGenerator dataGenerator;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.object.medium")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();

        dataGenerator = new DataGeneratorImpl();
    }

    @Test
    public void testEasy1(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestObjectMedium", "medium1");

        Clazz clazz = cf.getClasses().get(0);

        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));


        assertEquals("TestObjectMedium", clazz.getClassName());
        assertEquals(1, clazz.getMethods().size());

        Method m = clazz.getMethods().get(0);

        assertEquals(1, m.getMethodParameter().size());

        ConditionElement c1  = clazz.getMethods().get(0).getConditions().get(0);
        ConditionElement c2  = clazz.getMethods().get(0).getConditions().get(1);
        ConditionalExpression ce1 = ((Condition)c1).getConditionalExpressions().get(0);
        ConditionalExpression ce2 = ((Condition)c2).getConditionalExpressions().get(0);

        assertTrue(ce1.isInputDependent());
        assertEquals("person instanceOf input.data.object.data.Child == 0", ce1.getExpression());

        assertTrue(ce2.isInputDependent());
        assertEquals("person == null", ce2.getExpression());

        ObjectParameter p = (ObjectParameter) m.getMethodParameter().get(0);

        assertEquals(3, p.getPossibleObjectTypes().size());
        assertTrue(p.isCheckedNull());
    }


    @Test
    public void testEasy2(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestObjectMedium", "medium2");

        Clazz clazz = cf.getClasses().get(0);

        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));


        assertEquals("TestObjectMedium", clazz.getClassName());
        assertEquals(1, clazz.getMethods().size());

        Method m = clazz.getMethods().get(0);

        assertEquals(1, m.getMethodParameter().size());

        ConditionElement c1  = clazz.getMethods().get(0).getConditions().get(0);
    }
}
