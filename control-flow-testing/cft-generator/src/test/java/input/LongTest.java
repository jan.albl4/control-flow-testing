package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.Clazz;
import cz.zcu.kiv.cft.output.data.Condition;
import cz.zcu.kiv.cft.output.data.ConditionElement;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LongTest {

    private Analyzer analyzer;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.primitiv.easy")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();
    }

    @Test
    public void testEasy01(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyL", "easy1");

        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz = cf.getClasses().get(0);

        assertEquals("TestEasyL", clazz.getClassName());
        assertNotNull(clazz.getMethods().get(0));
        assertNotNull(clazz.getMethods().get(0).getConditions());
        assertEquals(1, clazz.getMethods().get(0).getConditions().size());

        ConditionElement c  = clazz.getMethods().get(0).getConditions().get(0);

        assertTrue(((Condition)c).getConditionalExpressions().get(0).isInputDependent());
    }
}
