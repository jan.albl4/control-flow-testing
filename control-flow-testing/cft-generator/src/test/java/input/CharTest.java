package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CharTest {

    private Analyzer analyzer;

    private DataGenerator dataGenerator;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.primitiv.easy")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();

        dataGenerator = new DataGeneratorImpl();
    }

    @Test
    public void testEasy1(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyC", "easy1");

        Clazz clazz = cf.getClasses().get(0);

        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));


        assertEquals("TestEasyC", clazz.getClassName());
        assertEquals(1, clazz.getMethods().size());

        Method m = clazz.getMethods().get(0);

        assertEquals(1, m.getMethodParameter().size());

        ConditionElement c  = clazz.getMethods().get(0).getConditions().get(0);
        ConditionalExpression ce = ((Condition)c).getConditionalExpressions().get(0);

        assertTrue(ce.isInputDependent());

        PrimitiveParameter p = (PrimitiveParameter) m.getMethodParameter().get(0);

        assertEquals(5, p.getValues().size());


        assertTrue(p.getValues().contains(String.valueOf(Integer.valueOf(Character.MIN_VALUE))));
        assertTrue(p.getValues().contains("65"));
        assertTrue(p.getValues().contains(String.valueOf(Integer.valueOf(Character.MAX_VALUE))));
    }

    @Test
    public void testEasy2(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyC", "easy2");

        Clazz clazz = cf.getClasses().get(0);

        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));


        assertEquals("TestEasyC", clazz.getClassName());
        assertEquals(1, clazz.getMethods().size());

        Method m = clazz.getMethods().get(0);

        assertEquals(2, m.getMethodParameter().size());

        PrimitiveParameter p1 = (PrimitiveParameter) m.getMethodParameter().get(0);
        PrimitiveParameter p2 = (PrimitiveParameter) m.getMethodParameter().get(1);

        assertEquals(12, p1.getValues().size());
        assertEquals(7, p2.getValues().size());

        assertTrue(p1.getValues().contains(String.valueOf(Integer.valueOf(Character.MIN_VALUE))));
        assertTrue(p1.getValues().contains(String.valueOf(Integer.valueOf(Character.MAX_VALUE))));
        assertTrue(p2.getValues().contains(String.valueOf(Integer.valueOf(Character.MIN_VALUE))));
        assertTrue(p2.getValues().contains(String.valueOf(Integer.valueOf(Character.MAX_VALUE))));
    }
}
