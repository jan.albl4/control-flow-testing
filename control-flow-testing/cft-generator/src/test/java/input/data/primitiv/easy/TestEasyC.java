package input.data.primitiv.easy;

public class TestEasyC {

    public char easy1(char a){
        if (a < 'A'){
            return a;
        }else{
            return 'D';
        }
    }

    public char easy2(char a, char b){

        char out = 1;

        switch (a){
            case 'A':  out = a; break;
            case 'B':  out = a; break;
            case 'D':  out = a; break;
            case 'E':  out = a; break;
            case 'F':  out = a; break;
        }

        if (b == a && b != 'Z'){
            out = 0;
        }

        return out;
    }
}
