package input.data.primitiv.easy;

public class TestEasyD {

    public double easy1(double a){
        if (a < 10){
            return a;
        }else{
            return 4.4;
        }
    }

    public double easy2(double a){
        if (a <= 10.3){
            return a;
        }else{
            return 4.4;
        }
    }

    public double easy3(double a){
        if (a > 10.4){
            return a;
        }else{
            return 4.4;
        }
    }

    public double easy4(double a){
        if (a >= 10.555564565554){
            return a;
        }else{
            return 4.4;
        }
    }

    public double easy5(double a){
        if (a == 10.4){
            return a;
        }else{
            return 4.4;
        }
    }

    public double easy8(double a){
        if (a != 10.555564565554){
            return a;
        }else{
            return 4.4;
        }
    }
}
