package input.data.primitiv.easy;

public class TestEasyS {

    public short easy1(short a, short b){

        if (a < 10 && a > b){
            return 5;
        }else{
            return 4;
        }
    }
}
