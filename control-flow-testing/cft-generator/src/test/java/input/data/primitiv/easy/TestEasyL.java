package input.data.primitiv.easy;

public class TestEasyL {

    public long easy1(long a){
        if (a < 10L){
            return 5;
        }else{
            return 4;
        }
    }


    public long easy2(long a){
        if (a <= 10){
            return 5;
        }else{
            return 4;
        }
    }

    public long easy3(long a){
        if (a > 10){
            return 5;
        }else{
            return 4;
        }
    }

    public long easy4(long a){
        if (a >= 10){
            return 5;
        }else{
            return 4;
        }
    }

    public long easy5(long a){
        if (a == 10){
            return 5;
        }else{
            return 4;
        }
    }

    public long easy6(long a){
        if (a != 10){
            return 5;
        }else{
            return 4;
        }
    }
}
