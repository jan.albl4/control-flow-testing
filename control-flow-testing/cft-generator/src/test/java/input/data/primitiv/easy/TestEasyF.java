package input.data.primitiv.easy;

public class TestEasyF {

    public float easy1(float a){
        if (a < 10){
            return a;
        }else{
            return 4.4f;
        }
    }

    public float easy2(float a){
        if (a <= 10.3f){
            return a;
        }else{
            return 4.4f;
        }
    }

    public float easy3(float a){
        if (a > 10.4f){
            return a;
        }else{
            return 4.4f;
        }
    }

    public float easy4(float a){
        if (a >= 10.555564565554f){
            return a;
        }else{
            return 4.4f;
        }
    }

    public float easy5(float a){
        if (a == 10.4f){
            return a;
        }else{
            return 4.4f;
        }
    }

    public float easy6(float a){
        if (a != 10.555564565554f){
            return a;
        }else{
            return 4.4f;
        }
    }
}
