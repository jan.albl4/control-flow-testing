package input.data.primitiv.medium;

public class TestMediumI {


    public int medium11(int a, int b, int c){
        if (a < b){
            for (int i = 5 * a; i < c; i++) {
                if(i == b){
                    break;
                }
            }

            if( c / b == 3) {
                return a / b;
            }

            return 5;
        }else{
            return 4;
        }
    }

    public int medium1(int a, int b){
        if (a < b){
            return 5;
        }else{
            return 4;
        }
    }



    public int medium66(int a, int b){
        if (a * a - a< 3){
           return 0;
        }

        if(b > 4){
            b--;
        }

        return 1;
    }

    public int medium2(int a, int b) {
        if (a < b + 3){
            for(int i = 48; i < a; i++){
                b = a/3;
                if(a/b > 45){
                    break;
                }
            }
            return 5;
        }else{
            if(b * 3 / a == 1.6){
                return b*5;
            }
            return 4;
        }
    }

    public int medium3(int a, int b){
        a = 65 * b;
        a = a /2;
        a = b  + 3 + a;

        if (a < b + 3){
            return a;
        }else{
            return 4;
        }
    }

    public int medium33(int a, double b){
        if (a >= b){
            if(b != a){
                return a;
            }else{
                if(b <= a){
                    a++;
                }
            }
            return a;
        }else{
            return 4;
        }
    }


    public int medium4(int a, int b){
        a = 65 * b;
        if (a < 96){
            if(a > Math.PI){
                return (int)(Math.PI * 56);
            }
            return a;
        }else{
            return 4;
        }
    }

    public int medium05(int a, int b, double c, char o){

        for (int i = -1; i < b; i++) {
            switch (a){
                case -25:
                    a--;
                    if(a > 44){
                        b=-2;
                    }
                    break;
                case 0: c = Math.E; break;
                default:
                    if(c/b < a){
                        b+=2;
                    }else {
                        a *= 25;
                    }
            }
        }


        return a^3;
    }

    public int medium5(int a, int b, double c, char o){

        for (int i = -1; i < b; i++) {
            switch (a){
                case -25:
                    a--;
                    if(a > 44){
                        b=-2;
                    }
                    break;
                case 0: c = Math.E; break;
                default:
                    if(c/b < a){
                        for(int j = a; j > 125; j--){
                            if(b < 0){
                                b+=2;
                            }
                        }
                    }else {
                        a *= 25;
                    }
            }
        }

        if (a < b) {
            a--;
        }

        switch (a){
            case 1:
                a--;
                if(a > 44){
                    b=-2;
                }
                break;
            case 0: c = Math.E; break;
            default:
                if(b < a){
                    for(int j = a; j > 125; j--){
                        if(b < 0){
                            b+=2;
                        }
                    }
                }else {
                    a *= 25;
                }
        }

        return a^3;
    }
}


