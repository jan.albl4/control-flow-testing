package input.data.primitiv.hard;


import input.data.object.data.Person;

public class TestHardI {
    /**
     * Vypocita faktorial cisla
     * @param number cislo >= 0
     * @return faktorial cisla
     */
    public static int factorial(int number) {
        if (number < 0) {
            //TODO - tohle hazi error - Exception in thread "main" java.lang.Error: Error loading java.lang.CharSequence
            //throw new IllegalArgumentException("zaporny argument");
        }
        int result = 1;
        for (int i = number; i > 1; i--) {
            result *= i;
        }
        return result;
    }


    /**
     * Fibonacciho posloupnost rekurzivne
     * @param index poradi cisla (pocitano od 0)
     * @return Fibonacciho cislo na danem poradi
     */
    public static int fibonacciRek(int index){
        if(index == 0) return 0; //zarazka
        else if(index == 1) return 1; //druha zarazka
        else return fibonacciRek(index - 1) + fibonacciRek(index - 2); //rekurzivni volani
    }

    public static void metoda0(int[] b){
        //TODO - funguje jen s java 1.7
        metoda1(b[1]);
        //System.out.print("aa metoda0");
        //System.out.print("bb metoda0");
    }



    public static int metoda1(int a){
        int u = a * 2;
        if(u > 6){
            for(int i = 1; i < 12; i++){
                a = metoda3(u);
            }
        }
        return a;
    }



    public static int metoda2(int b, int a, Person person){
        b *= 4;
        if(b > 125){
            //metoda0();
            int c = metoda1(b);
            //int d = metoda1(2*b);
            //int k = metoda2(d);
        }else {
            b = metoda1(b*13);
        }
        return b;
    }

    public static int metoda3(int b){
        b *= 4;
        if(b > 125){
            b =(int)metoda4(b);
        }else{
            b = metoda1(b);
        }
        return b;
    }

    public static double metoda4(int c){
        for(int i = 10; i < c; i++){
            c--;
        }

        if(c > Math.PI){
            return Math.PI;
        }
        return c;
    }
}
