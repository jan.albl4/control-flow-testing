package input.data.primitiv.easy;

public class TestEasyBy {

    public byte easy1(byte a){
        if (a < 5){
            return a;
        }else{
            return 'D';
        }
    }

    public byte easy2(byte a){
        if (a <= 'A'){
            return a;
        }else{
            return 'D';
        }
    }

    public byte easy3(byte a){
        if (a > 'A'){
            return a;
        }else{
            return 'D';
        }
    }

    public byte easy4(byte a){
        if (a >= 'A'){
            return a;
        }else{
            return 'D';
        }
    }

    public byte easy5(byte a){
        if (a >= 'B'){
            return a;
        }else{
            return 'D';
        }
    }

    public byte easy6(byte a){
        if (a >= 'A'){
            return a;
        }else{
            return 'D';
        }
    }
}
