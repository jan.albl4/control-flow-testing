package input.data.primitiv.easy;

public class TestEasyBo {

    public boolean easy1(boolean a){
        if (a){
            return a;
        }else{
            return false;
        }
    }

    public boolean easy2(boolean a){
        if (!a){
            return a;
        }else{
            return true;
        }
    }
}
