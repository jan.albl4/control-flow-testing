package input.data.primitiv.easy;

public class TestEasyI {

    public TestEasyI() { }

    public int easy01(int a) {
        if (a < 10) {
            --a;
        }

        return a;
    }

    public void easy02(int a, int b) {

        double c =  Math.PI * b / a;

        if (c < a && b > 10) {
            ++a;
        }

    }

    public boolean easy03(int a) {
        switch(a) {
            case 1:
                ++a;
                break;
            case 2:
                --a;
                break;
            case 3:
                a += 2;
                break;
            default:
                a /= 2;
        }

        return a >= 3;
    }

    public boolean easy04(int limit) {
        boolean b = true;
        for(int i = 0; i < limit; i++){
            b = false;
        }

        if(limit > 4){
            b = true;
        }

        return b;

    }


}
