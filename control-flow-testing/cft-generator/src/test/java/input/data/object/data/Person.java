package input.data.object.data;


import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The person.
 *
 * @author Mr.FrAnTA
 */
public class Person {

  /** The helper constant for formatter of person's birth date. */
  protected static final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

  /** The name of person. */
  private String name;

  /** The age of person. */
  public int age;

  /** The person's height. */
  public double height;

  /** The person's weight. */
  private double weight;

  /** The person's gender. */
  private Gender gender;

  /** The person's birth date. */
  private Date birthDate;

  /** The color of person's hair. */
  private Color hairColor;

  public Person() { }

  /**
   * Constructs the person.
   *
   * @param name the name of person.
   */
  public Person(String name) {
    this.name = name;
  }

  /**
   * Returns the name of person.
   *
   * @return the name of person.
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of person.
   *
   * @param name the name to set.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Returns the age of person.
   *
   * @return the age of person.
   */
  public int getAge() {
    return age;
  }

  /**
   * Sets the age of person.
   *
   * @param age the age to set.
   */
  public void setAge(int age) {
    this.age = age;
  }

  /**
   * Returns the person's height.
   *
   * @return the person's height.
   */
  public double getHeight() {
    return height;
  }

  /**
   * Sets the person's height.
   *
   * @param height the height to set.
   */
  public void setHeight(double height) {
    this.height = height;
  }

  /**
   * Returns the person's weight.
   *
   * @return the person's weight.
   */
  public double getWeight() {
    return weight;
  }

  /**
   * Sets the person's weight.
   *
   * @param weight the weight to set.
   */
  public void setWeight(double weight) {
    this.weight = weight;
  }

  /**
   * Returns the person's gender.
   *
   * @return the person's gender.
   */
  public Gender getGender() {
    return gender;
  }

  /**
   * Sets the color of person's hair.
   *
   * @param gender the gender to set.
   */
  public void setGender(Gender gender) {
    this.gender = gender;
  }

  /**
   * Returns the person's birth date.
   *
   * @return the person's birth date.
   */
  public Date getBirthDate() {
    return birthDate;
  }

  /**
   * Sets the person's birth date.
   *
   * @param birthDate the birth date to set.
   */
  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  /**
   * Returns the color of person's hair.
   *
   * @return the color of person's hair.
   */
  public Color getHairColor() {
    return hairColor;
  }

  /**
   * Returns the color of person's hair.
   *
   * @param hairColor the color of hair to set.
   */
  public void setHairColor(Color hairColor) {
    this.hairColor = hairColor;
  }

  /**
   * Returns string representation of person object.
   *
   * @return String representation of person.
   */
  @Override
  public String toString() {
    return getClass().getName() + " [name=" + name + ", age=" + age + ", height=" + height + "cm, weight=" + weight + "kg, gender=" + gender + ", birthDate="
        + (birthDate == null ? birthDate : dateFormatter.format(birthDate)) + ", hairColor=" + hairColor + "]";
  }

}
