package input.data.object.medium;

import input.data.object.data.Child;
import input.data.object.data.Gender;
import input.data.object.data.Parent;
import input.data.object.data.Person;

public class TestObjectMedium {

    private TestObjectMedium(){
        medium1(new Person());
    }

    public TestObjectMedium(int a){
        medium1(new Person());
    }

    public TestObjectMedium(Person p){
        medium1(p);
    }

    public void medium1(Person person){
        if(person instanceof Child){
            ((Person)person).setAge(10);
        }

        if(person != null){
            person.setGender(Gender.FEMALE);
        }
    }

    public boolean medium2(Parent parent){
        if(parent.getSon() instanceof Child){
            return true;
        }

        return false;
    }

    public boolean medium3(Parent parent){
        if(parent.getSon().getHeight() > 34){
            return true;
        }

        return false;
    }

    public boolean canEnter(Parent p, int limitedAge, double limitedHeight){
        boolean canEnter = false;

        if(p.getAge() > limitedAge){
            if(p.getHeight() > limitedHeight){
                canEnter = true;
            }
        }

        return canEnter;
    }

    public boolean canEnterTest(Parent p, int limitedAge, double limitedHeight){
        boolean canEnter = false;

        if(p.getFather().getFather().getAge() > limitedAge){
            canEnter = true;
        }

        return canEnter;
    }



    /*public boolean canEnter(Parent parent, int limitedAge, double limitedHeight){
        boolean canEnter = false;

        if(parent.getSon().age > limitedAge){
            if(parent.getDaughter().height > limitedHeight){
                canEnter = true;
            }
        }

        if(parent.getGender() == Gender.MALE){
            canEnter = false;
        }


        return canEnter;
    }*/

    public static void shift(boolean[] array, int startIndexInclusive, int endIndexExclusive, int offset) {
        if (array != null) {
            if (startIndexInclusive < array.length - 1 && endIndexExclusive > 0) {
                if (startIndexInclusive < 0) {
                    startIndexInclusive = 0;
                }

                if (endIndexExclusive >= array.length) {
                    endIndexExclusive = array.length;
                }

                int n = endIndexExclusive - startIndexInclusive;
                if (n > 1) {
                    offset %= n;
                    if (offset < 0) {
                        offset += n;
                    }

                    while(n > 1 && offset > 0) {
                        int n_offset = n - offset;
                        if (offset > n_offset) {
                            //swap(array, startIndexInclusive, startIndexInclusive + n - n_offset, n_offset);
                            n = offset;
                            offset -= n_offset;
                        } else {
                            if (offset >= n_offset) {
                                //swap(array, startIndexInclusive, startIndexInclusive + n_offset, offset);
                                break;
                            }

                            //swap(array, startIndexInclusive, startIndexInclusive + n_offset, offset);
                            startIndexInclusive += offset;
                            n = n_offset;
                        }
                    }

                }
            }
        }
    }
}
