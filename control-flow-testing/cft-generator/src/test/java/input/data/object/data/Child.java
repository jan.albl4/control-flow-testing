package input.data.object.data;

/**
 * The child.
 *
 * @author Mr.FrAnTA
 */
public class Child extends Person {

  /**
   * Constructs the child.
   *
   * @param name the name of child.
   */
  public Child(String name) {
    super(name);
  }

  /**
   * Constructs the child.
   *
   * @param firstName the first name of child.
   * @param lastName the second name of child.
   */
  public Child( String firstName, String lastName) {
    super(firstName + " " + lastName);
  }

}
