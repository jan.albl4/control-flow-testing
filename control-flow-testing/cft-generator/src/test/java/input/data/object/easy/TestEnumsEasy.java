package input.data.object.easy;

import input.data.object.data.Gender;
import input.data.object.data.Parent;
import input.data.object.data.Person;
import soot.G;

public class TestEnumsEasy {

    public boolean easy01(Gender gender){
        if(Gender.FEMALE == gender){
            return true;
        }
        return false;
    }

    public boolean easy02(Person person){
        if(Gender.FEMALE == person.getGender()){
            return true;
        }
        return false;
    }

    public boolean easy03(Gender gender){
        if(Gender.FEMALE == gender){
            return true;
        }
        return false;
    }

    public boolean easy04(Gender gender){
        if(null == gender){
            return true;
        }
        return false;
    }

    public boolean easy05(Gender gender){
        if(gender == gender){
            return true;
        }
        return false;
    }

    public boolean easy06(Parent parent){
        if(Gender.FEMALE == parent.getSon().getGender()){
            return true;
        }
        return false;
    }
}
