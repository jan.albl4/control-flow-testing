package input.data.object.easy;

import input.data.object.data.Person;

public class TestInteraction {

    // setInteractionStrength = 1
    public boolean interatction01(int a){
        if(a < Math.PI){
            return true;
        }
        return false;
    }

    // setInteractionStrength = 1
    public boolean interatction02(int a, int b){
        boolean output = false;
        if(a < Math.PI){
            output = true;
        }

        if(b > Math.PI){
            output = false;
        }
        return output;
    }

    // setInteractionStrength = 2
    public boolean interatction03(int a, int b){
        boolean output = false;
        if(a < Math.PI){
            return true;
        }

        if(b > Math.PI){
            output = true;
        }
        return output;
    }

    // setInteractionStrength = 2
    public boolean interatction04(int a, int b, int c){
        boolean output = false;
        if(a < b){
            output  = true;
        }

        if(b > c){
            output = true;
        }
        return output;
    }


    // setInteractionStrength = 3
    public boolean interatction05(int a, int b, int c){
        if(a < b){
           if(a > c){
               return true;
           }
        }

        return false;
    }

    // setInteractionStrength = 2
    public boolean interatction06(Person p, int limit){
        boolean output = false;
        if(p.age > limit){
            output = true;
        }

        if(p.height > limit){
            output = true;
        }

        if(p.height * 2 > p.age){
            output = true;
        }

        if(p.getHeight() == p.getWeight()){
            output = true;
        }

        return output;
    }

    // setInteractionStrength = 4
    public boolean interatction07(Person p, int limit){
        boolean output = false;
        if(p.age > limit){
            if(p.height > p.getWeight()){
                output = true;
            }
        }

        if(p.height * 2 > p.age){
            output = true;
        }

        if(p.getHeight() == p.getWeight()){
            output = true;
        }

        return output;
    }

    // setInteractionStrength = 3
    public boolean interatction08(Person p, int limit){
        boolean output = false;
        if(p.age > limit){
            if(p.height > p.getAge()){
                output = false;
            }
        }

        if(p.getHeight() * 2 > p.getWeight()){
            output = true;
        }

        return output;
    }

    // setInteractionStrength = 4
    public boolean interatction09(Person p, int limit){
        boolean output = false;
        if(p.age > limit){
            if(p.height > p.getAge()){
                return false;
            }
        }

        if(p.getHeight() * 2 > p.getWeight()){
            output = true;
        }

        return output;
    }
}
