package input.data.object.data;

/**
 * The gender of person.
 *
 * @author Mr.FrAnTA
 */
public enum Gender {

  /** Constant for males. */
  MALE,

  /** Constant for females. */
  FEMALE

}
