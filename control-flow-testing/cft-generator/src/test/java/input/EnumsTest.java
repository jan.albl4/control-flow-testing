package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnumsTest {

    private Analyzer analyzer;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.object.easy")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();
    }

    @Test
    public void testEasy01(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEnumsEasy", "easy01");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz =  cf.getClasses().get(0);

        assertEquals("TestEnumsEasy", clazz.getClassName());
        assertNotNull(clazz.getMethods().get(0));
        assertNotNull(clazz.getMethods().get(0).getConditions());
        assertEquals(1, clazz.getMethods().get(0).getConditions().size());

        ConditionElement c  =  clazz.getMethods().get(0).getConditions().get(0);

        assertTrue(((Condition)c).getConditionalExpressions().get(0).isInputDependent());
    }

    @Test
    public void testEasy02(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEnumsEasy", "easy02");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz =  cf.getClasses().get(0);

        assertEquals("TestEnumsEasy", clazz.getClassName());
        assertNotNull(clazz.getMethods().get(0));
        assertNotNull(clazz.getMethods().get(0).getConditions());
        assertEquals(1, clazz.getMethods().get(0).getConditions().size());

        Method m = clazz.getMethods().get(0);

        ConditionElement c  = m.getConditions().get(0);

        assertTrue(((Condition)c).getConditionalExpressions().get(0).isInputDependent());

        ObjectParameter op = (ObjectParameter) m.getMethodParameter().get(0);

        assertEquals("person", op.getName());
        assertEquals(1, op.getEnumParameters().size());

        EnumParameter ep = op.getEnumParameters().get(0);
        assertEquals("gender", ep.getName());
    }


    @Test
    public void testEasy03(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEnumsEasy", "easy03");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz =  cf.getClasses().get(0);

        Method method = clazz.getMethods().get(0);
        EnumParameter enumParameter = (EnumParameter)method.getMethodParameter().get(0);

        ConditionElement c  = clazz.getMethods().get(0).getConditions().get(0);

        assertTrue(((Condition)c).getConditionalExpressions().get(0).isInputDependent());

        assertEquals("gender", enumParameter.getName());
        assertEquals(2, enumParameter.getPossibleEnums().size());
        assertTrue(enumParameter.getPossibleEnums().contains("input.data.object.data.Gender.FEMALE"));
        assertTrue(enumParameter.getPossibleEnums().contains("input.data.object.data.Gender.MALE"));
    }

    @Test
    public void testEasy06(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEnumsEasy", "easy06");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz =  cf.getClasses().get(0);

        Method method = clazz.getMethods().get(0);
        ObjectParameter objectParameter = (ObjectParameter) method.getMethodParameter().get(0);

        assertEquals("parent", objectParameter.getName());
        assertTrue( objectParameter.getPossibleObjectTypes().size() > 0);
        assertEquals( 1, objectParameter.getObjects().size());
        assertEquals( 0, objectParameter.getEnumParameters().size());

        objectParameter = objectParameter.getObjects().get(0);

        assertEquals("son", objectParameter.getName());
        assertTrue( objectParameter.getPossibleObjectTypes().size() > 0);
        assertEquals( 1, objectParameter.getEnumParameters().size());
        assertEquals( 0, objectParameter.getObjects().size());

        EnumParameter enumParameter = objectParameter.getEnumParameters().get(0);

        assertEquals("gender", enumParameter.getName());
        assertEquals(2, enumParameter.getPossibleEnums().size());
        assertTrue(enumParameter.getPossibleEnums().contains("input.data.object.data.Gender.FEMALE"));
        assertTrue(enumParameter.getPossibleEnums().contains("input.data.object.data.Gender.MALE"));
    }
}
