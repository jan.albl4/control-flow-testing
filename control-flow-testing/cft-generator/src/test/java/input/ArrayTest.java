package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.Clazz;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.output.data.Method;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ArrayTest {


    private Analyzer analyzer;

    private DataGenerator dataGenerator;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.primitiv.array")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();

        dataGenerator = new DataGeneratorImpl();
    }

    @Test
    public void testEasyArray01(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestPrimitiveArray", "easyArray01");

        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));
        System.out.println(OutputConverter.convertToXml(cf));

        Clazz clazz = cf.getClasses().get(0);

        assertEquals("TestPrimitiveArray", clazz.getClassName());

        assertNotNull(clazz.getMethods().get(0));

        Method m = clazz.getMethods().get(0);
    }
}
