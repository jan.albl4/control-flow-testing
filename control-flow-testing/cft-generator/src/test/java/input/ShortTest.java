package input;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ShortTest {

    private Analyzer analyzer;

    private DataGenerator dataGenerator;

    @Before
    public void beforeAll() {

        String classPath = "";

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }

        analyzer = new AnalyzerImpl.Builder()
                .setProjectPath(classPath)
                .setPackagePath("input.data.primitiv.easy")
                .setMaxDepth(2)
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();

        dataGenerator = new DataGeneratorImpl();
    }

    @Test
    public void testEasy01(){
        ControlFlowAnalysisOutput cf = analyzer.analyzeMethod("TestEasyS", "easy1");

        dataGenerator.generateData(cf);

        System.out.println(OutputConverter.convertToJson(cf));

        Clazz clazz = cf.getClasses().get(0);

        assertEquals("TestEasyS", clazz.getClassName());

        assertNotNull(clazz.getMethods().get(0));

        Method m = clazz.getMethods().get(0);

        PrimitiveParameter p1 = (PrimitiveParameter)m.getMethodParameter().get(0);
        PrimitiveParameter p2 = (PrimitiveParameter)m.getMethodParameter().get(1);


        assertNotNull(clazz.getMethods().get(0).getConditions());
        assertEquals(7, p1.getValues().size());
        assertEquals(5, p2.getValues().size());

        assertTrue(p1.getValues().contains("10"));
        assertTrue(p1.getValues().contains(String.valueOf(Integer.valueOf(Short.MIN_VALUE))));
        assertTrue(p1.getValues().contains(String.valueOf(Integer.valueOf(Short.MAX_VALUE))));
        assertTrue(p2.getValues().contains(String.valueOf(Integer.valueOf(Short.MIN_VALUE))));
        assertTrue(p2.getValues().contains(String.valueOf(Integer.valueOf(Short.MAX_VALUE))));

    }
}
