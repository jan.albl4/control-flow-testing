package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import org.junit.Before;
import org.junit.Test;
import utils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AnalyzerInteractionTest {
    private String classPath = "";

    @Before
    public void initClassPath(){
        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }
    }

    @Test
    public void testAnalyzerInteractionMethod1(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction01");

        Utils.checkControlFlowOutput(cfo);

        Clazz clazz  = cfo.getClasses().get(0);

        Method method =  clazz.getMethods().get(0);

        assertEquals(1, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod2(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction02");

        Utils.checkControlFlowOutput(cfo);
        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(1, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod3(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction03");

        Utils.checkControlFlowOutput(cfo);
        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(2, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod4(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction04");

        Utils.checkControlFlowOutput(cfo);

        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(2, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod5(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction05");

        Utils.checkControlFlowOutput(cfo);

        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(3, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod6(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction06");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cfo);

        Utils.checkControlFlowOutput(cfo);

        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(2, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod7(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction07");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cfo);

        Utils.checkControlFlowOutput(cfo);

        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(4, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod8(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction08");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cfo);

        Utils.checkControlFlowOutput(cfo);

        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(3, method.getInteractionStrength());
    }

    @Test
    public void testAnalyzerInteractionMethod9(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestInteraction", "interatction09");
        DataGenerator dataGenerator = new DataGeneratorImpl();
        dataGenerator.generateData(cfo);

        Utils.checkControlFlowOutput(cfo);

        System.out.println(OutputConverter.convertToJson(cfo));

        Clazz clazz  = cfo.getClasses().get(0);
        Method method =  clazz.getMethods().get(0);

        assertEquals(4, method.getInteractionStrength());
    }
}
