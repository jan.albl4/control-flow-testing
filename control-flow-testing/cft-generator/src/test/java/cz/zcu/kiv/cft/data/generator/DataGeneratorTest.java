package cz.zcu.kiv.cft.data.generator;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.output.data.Method;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.junit.Before;
import org.junit.Test;
import utils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DataGeneratorTest {

    private String classPath = "";

    private DataGenerator dataGenerator;

    @Before
    public void initClassPathAndDataGenerator(){
        dataGenerator = new DataGeneratorImpl();

        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }
    }

    @Test
    public void testGenerateInt1(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.primitiv.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestEasyI", "easy01");

        dataGenerator.generateData(cfo);
        OutputConverter.convertToJson(cfo);

        Utils.checkControlFlowOutput(cfo);

        assertEquals(1, cfo.getClasses().get(0).getMethods().size());

        Method method = cfo.getClasses().get(0).getMethods().get(0);

        //Method
        assertNotNull(method.getMethodParameter());
        assertNotNull(method.getConditions());
        assertEquals(1, method.getMethodParameter().size());


        PrimitiveParameter parameter = (PrimitiveParameter)method.getMethodParameter().get(0);
        //Parameter
        assertEquals(5, parameter.getValues().size());

        assertTrue(parameter.getValues().contains(String.valueOf(Integer.MIN_VALUE)));
        assertTrue(parameter.getValues().contains("10"));
        assertTrue(parameter.getValues().contains(String.valueOf(Integer.MAX_VALUE)));
    }

    @Test
    public void testGenerateInt2(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.primitiv.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestEasyI", "easy02");

        dataGenerator.generateData(cfo);


        Utils.checkControlFlowOutput(cfo);

        assertEquals(1, cfo.getClasses().get(0).getMethods().size());

        Method method = cfo.getClasses().get(0).getMethods().get(0);

        //Method
        assertNotNull(method.getMethodParameter());
        assertNotNull(method.getConditions());
        assertEquals(2, method.getMethodParameter().size());


        PrimitiveParameter parameterA = (PrimitiveParameter)method.getMethodParameter().get(0);
        PrimitiveParameter parameterB = (PrimitiveParameter)method.getMethodParameter().get(1);
        //Parameter
        assertEquals(5, parameterA.getValues().size());
        assertEquals(7, parameterB.getValues().size());

        assertTrue(parameterA.getValues().contains(String.valueOf(Integer.MIN_VALUE)));
        assertTrue(parameterA.getValues().contains(String.valueOf(Integer.MAX_VALUE)));
        assertTrue(parameterB.getValues().contains(String.valueOf(Integer.MIN_VALUE)));
        assertTrue(parameterB.getValues().contains(String.valueOf(Integer.MAX_VALUE)));
    }

    @Test
    public void testGenerateIntSwitch(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.primitiv.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestEasyI", "easy03");

        System.out.println(OutputConverter.convertToJson(cfo));
        dataGenerator.generateData(cfo);
        System.out.println(OutputConverter.convertToJson(cfo));

        Utils.checkControlFlowOutput(cfo);

        assertEquals(1, cfo.getClasses().get(0).getMethods().size());

        Method method = cfo.getClasses().get(0).getMethods().get(0);

        //Method
        assertNotNull(method.getMethodParameter());
        assertNotNull(method.getConditions());
        assertEquals(1, method.getMethodParameter().size());


        PrimitiveParameter parameterA = (PrimitiveParameter)method.getMethodParameter().get(0);
       //Parameter
        assertEquals(9, parameterA.getValues().size());

        assertTrue(parameterA.getValues().contains(String.valueOf(Integer.MIN_VALUE)));
        assertTrue(parameterA.getValues().contains(String.valueOf(Integer.MAX_VALUE)));
    }
}
