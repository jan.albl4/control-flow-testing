package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.analyzer.exception.AnalyzerVariableTableNotSetException;
import cz.zcu.kiv.cft.analyzer.exception.ProjectPathNotSetException;
import cz.zcu.kiv.cft.analyzer.exception.LoadingMethodException;
import cz.zcu.kiv.cft.analyzer.exception.ClassNotLoadException;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import org.junit.Before;
import org.junit.Test;

public class AnalyzerExceptionTest {

    private String classPath = "";

    @Before
    public void initClassPath(){
        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\src\\test\\java\\";
        }
    }


    @Test(expected = AnalyzerVariableTableNotSetException.class)
    public void testAnalyzerVariableTableNotSet(){
        Analyzer analyzer = new AnalyzerImpl.Builder().build();
        analyzer.analyzeMethod(null, null);
    }

    @Test(expected = ProjectPathNotSetException.class)
    public void testClassPathNotSet(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .build();
        analyzer.analyzeMethod(null, null);
    }


    @Test(expected = ClassNotLoadException.class)
    public void testClassNotLoad1(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setProjectPath(classPath).build();

        analyzer.analyzeMethod(null, null);
    }

    @Test(expected = ClassNotLoadException.class)
    public void testClassNotLoad2(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("package")
                .setProjectPath(classPath).build();

        analyzer.analyzeMethod(null, null);
    }

    @Test(expected = ClassNotLoadException.class)
    public void testClassNotLoad3(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("package")
                .setProjectPath(classPath).build();

        analyzer.analyzeMethod("SomeClass", null);
    }
    @Test(expected = ClassNotLoadException.class)
    public void testClassNotLoad4(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.object.easy")
                .setProjectPath(classPath).build();

        analyzer.analyzeMethod("TestInterface", "someMetdhod");
    }

    @Test(expected = LoadingMethodException.class)
    public void testLoadingMethod1(){
        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.primitiv.easy")
                .setProjectPath(classPath).build();

        analyzer.analyzeMethod("TestEasyI", "some method");
    }


}
