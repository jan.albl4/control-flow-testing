package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.tools.OSPlatform;

import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.data.*;
import org.junit.Before;
import org.junit.Test;
import utils.Utils;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AnalyzerTest {

    private String classPath = "";

    @Before
    public void initClassPath(){
        if (OSPlatform.isMac()) {
            classPath = System.getProperty("user.dir") + "/cft-generator/src/test/java/";
        } else if(OSPlatform.isWindows()) {
            classPath = System.getProperty("user.dir") + "\\cft-generator\\src\\test\\java\\";
        }
    }

    @Test
    public void testAnalyzeMethod1(){

        Analyzer analyzer = new AnalyzerImpl.Builder()
                .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables())
                .setPackagePath("input.data.primitiv.easy")
                .setProjectPath(this.classPath).build();

        ControlFlowAnalysisOutput cfo = analyzer.analyzeMethod("TestEasyI", "easy01");

        Utils.checkControlFlowOutput(cfo);

        Clazz clazz  = cfo.getClasses().get(0);

        //class
        assertEquals("TestEasyI",clazz.getClassName());
        assertEquals("input.data.primitiv.easy", clazz.getPackageName());
        assertEquals(classPath, clazz.getClassPath());
        assertEquals(1, clazz.getMethods().size());

        Method method =  clazz.getMethods().get(0);

        //method
        assertEquals("easy01", method.getName());
        assertEquals("int",  method.getReturnType());
        assertTrue(!method.isStatic());
        assertNotNull(method.getConditions());
        assertNotNull(method.getMethodParameter());
        assertEquals(1, method.getMethodParameter().size());
        assertEquals(1, method.getConditions().size());

        ConditionElement conditionElement = method.getConditions().get(0);

        //Condition
        assertTrue(conditionElement instanceof Condition);

        List<ConditionalExpression> cel = ((Condition)conditionElement).getConditionalExpressions();

        assertEquals("a >= 10", cel.get(0).getExpression());
        assertTrue(cel.get(0).isInputDependent());
        assertEquals(1, cel.get(0).getArgTypeMap().size());
        assertTrue(cel.get(0).getArgTypeMap().containsKey("a"));
        assertEquals(1, cel.get(0).getArgTypeMap().get("a").size());
        assertEquals("int", cel.get(0).getArgTypeMap().get("a").get(0).toString());


        Parameter parameter = method.getMethodParameter().get(0);
        //Parameter
        assertTrue(parameter instanceof PrimitiveParameter);
        assertEquals("a", ((PrimitiveParameter)parameter).getName());
        assertEquals("int", ((PrimitiveParameter)parameter).getDataType());
        assertTrue(parameter.isPrimitive());
    }
}
