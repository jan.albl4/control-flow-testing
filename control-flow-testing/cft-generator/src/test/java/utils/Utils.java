package utils;

import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Utils {

    public static void checkControlFlowOutput(ControlFlowAnalysisOutput controlFlowAnalysisOutput){
        assertNotNull(controlFlowAnalysisOutput);
        assertNotNull(controlFlowAnalysisOutput.getClasses().get(0));
        assertNotNull(controlFlowAnalysisOutput.getClasses().get(0).getMethods());
        assertNotNull(controlFlowAnalysisOutput.getClasses().get(0).getClassPath());
        assertNotNull(controlFlowAnalysisOutput.getClasses().get(0).getPackageName());
        assertNotNull(controlFlowAnalysisOutput.getClasses().get(0).getClassName());
    }
}
