package cz.zcu.kiv.cft.analyzer.branches;


import cz.zcu.kiv.cft.analyzer.symbols.SymbolItem;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolTableStack;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolTable;
import soot.Unit;

import java.util.List;

/**
 *
 * Class holding a branch node
 *
 * @author Jan Albl
 */
public class StackItem {

    /** Branch node */
    private Unit unit;

    /** End branch node. Branching node postdominator */
    private Unit endNode;

    /** Count of unprocessed branches */
    private int unprocessedBranchesCount;

    /** All symbol tables for a given branch node */
    private List<SymbolTableStack> allVariableTables;

    /** Last symbol table */
    private SymbolTableStack symbolTableStack;

    /**
     * Returns the last symbol table
     * @return returns the last symbol table
     * */
    public SymbolTable getVariableTableLast() {

        SymbolTable vt = new SymbolTable();

        for (int i = 0; i < symbolTableStack.pop().getSymbols().size(); i++) {
            SymbolItem copyItem = symbolTableStack.pop().getSymbols().get(i);

            SymbolItem symbolItem = new SymbolItem();
            symbolItem.setType(copyItem.getType());
            symbolItem.setSootValue(copyItem.getSootValue());
            symbolItem.setParam(copyItem.getParam());
            symbolItem.setParameterRef(copyItem.getParameterRef());
            symbolItem.setName(copyItem.getName());
            symbolItem.setRightOp(copyItem.getRightOp());
            symbolItem.setHistory(copyItem.getHistory());

            vt.addSymbolItem(symbolItem);
        }

        return vt;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public int getUnprocessedBranchesCount() {
        return unprocessedBranchesCount;
    }

    public void setUnprocessedBranchesCount(int unprocessedBranchesCount) {
        this.unprocessedBranchesCount = unprocessedBranchesCount;
    }

    public Unit getEndNode() {
        return endNode;
    }

    public void setEndNode(Unit endNode) {
        this.endNode = endNode;
    }

    public List<SymbolTableStack> getAllVariableTables() {
        return allVariableTables;
    }

    public void setAllVariableTables(List<SymbolTableStack> allVariableTables) {
        this.allVariableTables = allVariableTables;
    }

    public SymbolTableStack getSymbolTableStack() {
        return symbolTableStack;
    }

    public void setSymbolTableStack(SymbolTableStack symbolTableStack) {

        this.symbolTableStack = new SymbolTableStack();

        for(SymbolTable symbolTable : symbolTableStack.getSymbolTableStack()){

            SymbolTable symbolTableCopy = new SymbolTable();

            for(int i = 0; i < symbolTable.getSymbols().size(); i++){
                SymbolItem copyItem = symbolTable.getSymbols().get(i);

                SymbolItem symbolItem = new SymbolItem();
                symbolItem.setType(copyItem.getType());
                symbolItem.setSootValue(copyItem.getSootValue());
                symbolItem.setParam(copyItem.getParam());
                symbolItem.setParameterRef(copyItem.getParameterRef());
                symbolItem.setName(copyItem.getName());
                symbolItem.setRightOp(copyItem.getRightOp());
                symbolItem.setHistory(copyItem.getHistory());
                symbolTableCopy.addSymbolItem(symbolItem);
            }

            this.symbolTableStack.push(symbolTableCopy);
        }
    }

}
