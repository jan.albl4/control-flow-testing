package cz.zcu.kiv.cft.data.generator.data;

import soot.Type;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class EnumVariable extends BaseVariable {

    /**  */
    private boolean isNullable;

    /**  */
    private List<Type> parrentType;

    /**  */
    private List<Type> dataType;

    /**  */
    private List<String> enums;

    public EnumVariable(){
        parrentType = new ArrayList<>();
        dataType = new ArrayList<>();
        enums = new ArrayList<>();
        isNullable = false;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public void setNullable(boolean nullable) {
        isNullable = nullable;
    }

    public List<Type> getParrentType() {
        return parrentType;
    }

    public void setParrentType(List<Type> parrentType) {
        this.parrentType = parrentType;
    }

    public List<Type> getDataType() {
        return dataType;
    }

    public void setDataType(List<Type> dataType) {
        this.dataType = dataType;
    }

    public List<String> getEnums() {
        return enums;
    }

    public void setEnums(List<String> enums) {
        this.enums = enums;
    }
}
