package cz.zcu.kiv.cft.analyzer.symbols;

import soot.SootMethod;
import soot.Type;
import soot.Value;
import soot.jimple.ParameterRef;

/**
 *
 * Data class representing an item in the symbol table
 *
 * @author Jan Albl
 */
public class SymbolItem {

    /** Item data type */
    private Type type;

    /** History symbol */
    private HistorySymbolItem history;

    /** Item value */
    private Value sootValue;

    /** Right side of the symbol */
    private Value rightOp;

    /** Name of the symbol */
    private String name;

    /** Is a parameter-dependent symbol */
    private Boolean param = false;

    /** Parental method */
    private SootMethod parentMethod = null;

    /** Reference to a parameter if it is dependent on it */
    private ParameterRef parameterRef = null;

    public Boolean getParam() {
        return param;
    }

    public void setParam(Boolean param) {
        this.param = param;
    }

    public ParameterRef getParameterRef() {
        return parameterRef;
    }

    public void setParameterRef(ParameterRef parameterRef) {
        this.parameterRef = parameterRef;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Value getSootValue() {
        return sootValue;
    }

    public void setSootValue(Value sootValue) {
        this.sootValue = sootValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Value getRightOp() {
        return rightOp;
    }

    public void setRightOp(Value rightOp) {
        this.rightOp = rightOp;
    }

    public HistorySymbolItem getHistory() {
        return history;
    }

    public SootMethod getParentMethod() {
        return parentMethod;
    }

    public void setParentMethod(SootMethod parentMethod) {
        this.parentMethod = parentMethod;
    }

    public void setHistory(HistorySymbolItem history) {
        this.history = history;
    }
}
