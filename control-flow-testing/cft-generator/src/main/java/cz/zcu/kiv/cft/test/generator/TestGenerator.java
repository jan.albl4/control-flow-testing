package cz.zcu.kiv.cft.test.generator;

import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;

/**
 *
 * Interface that generates unit tests from the generated data stored in the {@link ControlFlowAnalysisOutput} class
 *
 * @author Jan Albl
 */
public interface TestGenerator {

    /**
     * Generates test classes from data to the specified path
     * @param cfo {@link ControlFlowAnalysisOutput}
     * @param path path where unit tests are to be generated
     * */
    void generateClassFor(ControlFlowAnalysisOutput cfo, String path);

    /**
     * Generates test classes from data to the specified path
     * @param cfo {@link ControlFlowAnalysisOutput}
     * @param path path where unit tests are to be generated
     * @param maximumLines the approximate maximum number of rows of the generated test class
     * */
    void generateClassFor(ControlFlowAnalysisOutput cfo, String path, int maximumLines);
}
