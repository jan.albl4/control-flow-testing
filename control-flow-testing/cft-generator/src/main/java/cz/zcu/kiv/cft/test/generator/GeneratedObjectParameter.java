package cz.zcu.kiv.cft.test.generator;

import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.tools.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class generating bodies object parameters
 *
 * @author Jan Albl
 */
public class GeneratedObjectParameter extends GeneratedParameter {

    public GeneratedObjectParameter(ObjectParameter objectParameter){
        this.setName(objectParameter.getName());

        createBlocks(objectParameter);
    }

    private List<String> createBlock(ObjectParameter p, String name, List<String> useName){
        List<String> out = new ArrayList<>();

        List<List<String>> blocks = new ArrayList<>();

        String localName = getOriginalName(useName, p.getName());

        for(String value : p.getPossibleObjectTypes()){
            String block = "";

            block +=  p.getDataType() + " ";
            block +=  localName;
            block += " = new " + value + "();\n";

            out.add(block);
        }

        blocks.add(out);

        for(PrimitiveParameter par : p.getPrimitiveFields()){
            List<String> bodyP = createBlockForPrimitive(par, localName);
            blocks.add(bodyP);
        }

        for(EnumParameter value: p.getEnumParameters()){
            List<String> bodyP = createBodyForEnum(value, localName);
            blocks.add(bodyP);
        }

        for(ObjectParameter op : p.getObjects()) {
            blocks.add(createBlock(op, localName, useName));
        }

        if(name != null){
            out = new ArrayList<>();

            String blockForObject = name + ".set"+ p.getName().substring(0, 1).toUpperCase() + p.getName().substring(1);
            blockForObject += "(" + localName + ");\n";

            out.add(blockForObject);
            blocks.add(out);
        }

        out = Utils.combineLists(blocks);

        if(p.isCheckedNull()){

            String blockForObject = "";

            blockForObject +=  p.getDataType() + " ";
            blockForObject +=  localName;
            blockForObject += " = null;\n";

            out.add(blockForObject);
        }

        return out;
    }

    private void createBlocks(ObjectParameter p){
        this.setBody(createBlock(p, null, new ArrayList<>()));
    }


    private List<String> createBlockForPrimitive(PrimitiveParameter p, String parentGeter){
        List<String> out = new ArrayList<>();

        for(String value : p.getValues()){
            String blockForPrimitive = "";

            blockForPrimitive += parentGeter + ".set"+ p.getName().substring(0, 1).toUpperCase() + p.getName().substring(1);
            blockForPrimitive += "(" + value + ");\n";

            out.add(blockForPrimitive);
        }

        return out;
    }


    private List<String> createBodyForEnum(EnumParameter p, String parentGetter){
        List<String> out = new ArrayList<>();

        for(String value : p.getPossibleEnums()){
            String bodyForPrimitive = "";

            bodyForPrimitive += parentGetter + ".set"+ p.getName().substring(0, 1).toUpperCase() + p.getName().substring(1);

            bodyForPrimitive += "(" + value + ");\n";

            out.add(bodyForPrimitive);
        }

        return out;
    }


    private String getOriginalName(List<String> useName, String name){

        String outName = name;

        if(useName.contains(outName)) {
            outName += useName.size();
        }

        useName.add(outName);

       return outName;
    }

}
