package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.Range;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public interface ShortEvaluator extends IntervalCalculator {
    void addShortValuesToParameter(PrimitiveParameter primitiveParameter, Range range);
}
