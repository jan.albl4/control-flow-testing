package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.Range;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class FloatEvaluatorImpl implements FloatEvaluator {

    private final String suffix = "f";

    @Override
    public void addFloatValuesToParameter(PrimitiveParameter primitiveParameter, Range range){
        if(primitiveParameter != null && range != null) {

            List<String> values = getValueFromRange(range);

            for(String v: values){
                primitiveParameter.getValues().add(v);
            }
        }
    }

    @Override
    public List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter){
        List<Range> ranges = new ArrayList<>();

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new FloatRange(Float.MIN_VALUE, primitiveParameter.getBorderValue().get(0)));
        }

        for (int i = 0; i < primitiveParameter.getBorderValue().size()-1; i++) {
            ranges.add(new FloatRange(primitiveParameter.getBorderValue().get(i), primitiveParameter.getBorderValue().get(1+i)));
        }

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new FloatRange(primitiveParameter.getBorderValue().get(primitiveParameter.getBorderValue().size()-1), Float.MAX_VALUE));
        }

        return ranges;
    }

    private List<String> getValueFromRange(Range range){
        List<String> values = new ArrayList<>();

        values.add(String.valueOf(range.getMinimumFloat())+suffix);

        float min = 1; //aby se nestalo ze dostanu cislo k
        float max = range.getMaximumFloat() - range.getMinimumFloat() - 1;

        if (range.getMaximumFloat() - range.getMinimumFloat() < 0){
            //preteklo cislo je vetsi nez Integer.Maxint
            max = Float.MAX_VALUE - 1.0f;
        }

        if (range.getMaximumFloat() - range.getMinimumFloat() != 0 && max > min) {
            Random random = new Random();
            float between = min + (max - min) * random.nextFloat();
            values.add(String.valueOf(between + range.getMinimumFloat())+suffix);
        }

        values.add(String.valueOf(range.getMaximumFloat())+suffix);

        return values;
    }

    @Override
    public String getRandomValue(){
        double minRandomValue = -100000;
        double maxRandomValue = 100000;
        Random r = new Random();
        double randomValue = minRandomValue + (maxRandomValue - minRandomValue) * r.nextDouble();
        return String.valueOf(randomValue)+suffix;
    }
}
