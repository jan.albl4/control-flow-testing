package cz.zcu.kiv.cft.tools;

import cz.zcu.kiv.cft.tools.log.Logger;
import soot.SootClass;
import soot.SootField;
import soot.Unit;
import soot.jimple.VirtualInvokeExpr;
import soot.toolkits.graph.UnitGraph;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Class that contains useful methods used throughout the project
 *
 * @author Jan Albl
 * */
public class Utils {

    /** Logger {@link Logger} */
    private static final Logger logger = Logger.getLogger();

    /**
     * Gets a list of all nodes that are accessible from the start node. The method uses the DFS algorithm
     * @param graph graph
     * @param startNode start node
     * @return list of all nodes that are accessible from the start node
     * */
    public static List<Unit> DFS(UnitGraph graph, Unit startNode) {

        List<Unit> result = new ArrayList<Unit>();
        Set<Unit> closed = new HashSet<Unit>();
        Stack<Unit> open = new Stack<Unit>();

        // init
        open.push(startNode);

        while (!open.isEmpty()) {

            Unit active = open.pop();

            if (!closed.contains(active)) {

                result.add(active);
                //visited
                closed.add(active);

                open.addAll(graph.getSuccsOf(active));
            }
        }

        return result;
    }

    /**
     * Detects whether the method returns a class field
     * @param vie virtual invoke expression
     * @return returns true if the method returns a class field
     * */
    public static boolean isGetField(VirtualInvokeExpr vie){
        String methodName = vie.getMethod().getName();
        boolean isField = false;

        if (methodName.indexOf("get") == 0) {
            SootClass sootClass = vie.getMethod().getDeclaringClass();
            String possibleFields  = methodName.replace("get", "").toLowerCase();
            for (SootField f : sootClass.getFields()) {
                if(f.getName().toLowerCase().equals(possibleFields)){
                    isField = true;
                    break;
                }
            }
        }

        return isField;
    }

    /**
     * Returns field of class by getter method
     * @param vie virtual invoke expression
     * @return returns field of class
     * **/
    public static SootField getField(VirtualInvokeExpr vie){
        String methodName = vie.getMethod().getName();

        if (methodName.indexOf("get") == 0) {
            SootClass sootClass = vie.getMethod().getDeclaringClass();
            String possibleFields  = methodName.replace("get", "").toLowerCase();
            for (SootField f : sootClass.getFields()) {
                if(f.getName().toLowerCase().equals(possibleFields)){
                    return f;
                }
            }
        }

        return null;
    }

    /**
     * Gets all .java and .class files from the specified path. Taking the input path as a project path,
     * it takes all subfolders as package names.
     * @param projectFolderPath  path to the project folder to be analyzed. Other subfolders are treated as individual packages
     * @return  the map where the key is the package and the value is the list of classes in the package
     */
    public static Map<String, List<String>> getClassAndPackagesFromFolder(String projectFolderPath){
        Map<String, List<String>>  classAndPackageMap = new HashMap<>();

        try (Stream<Path> filePathStream= Files.walk(Paths.get(projectFolderPath))) {
            filePathStream.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {

                    String packageFile = filePath.toString().substring(projectFolderPath.length());

                    packageFile = packageFile.replace(filePath.getFileName().toString(), "");
                    packageFile = packageFile.replace(OSPlatform.getPathSeparator(), ".");

                    if (packageFile.length() > 0) {
                        packageFile = packageFile.substring(0, packageFile.length()-1);
                    }

                    String[] fileExtension = filePath.getFileName().toString().split("\\.");

                    if(fileExtension.length == 2 &&  (fileExtension[1].equals("class") || fileExtension[1].equals("java"))){

                        if (!classAndPackageMap.containsKey(packageFile)) {
                            classAndPackageMap.put(packageFile, new ArrayList<>());
                        }

                        classAndPackageMap.get(packageFile).add(fileExtension[0]);
                    }
                }
            });
        }catch (Exception e){
            logger.warn(e.toString());
        }

        return classAndPackageMap;
    }

    //kombinace vsech moznych vystupu
    public static List<String> combineLists(List<List<String>> arr) {

        List<String> output = new ArrayList<>();

        // number of arrays
        int n = arr.size();

        // to keep track of next element in each of
        // the n arrays
        int[] indices = new int[n];

        // initialize with first element's index
        for (int i = 0; i < n; i++){
            indices[i] = 0;
        }

        while (true) {

            String body = "";

            // print current combination
            for (int i = 0; i < n; i++) {
                body += arr.get(i).get(indices[i]);
            }

            body += "\n";


            //System.out.println(body);
            output.add(body);


            // find the rightmost array that has more
            // elements left after the current element
            // in that array
            int next = n - 1;
            while (next >= 0 && (indices[next] + 1 >= arr.get(next).size()))
                next--;

            // no such array is found so no more
            // combinations left
            if (next < 0){
                break;
            }

            // if found move to next element in that
            // array
            indices[next]++;

            // for all arrays to the right of this
            // array current index again points to
            // first element
            for (int i = next + 1; i < n; i++)
                indices[i] = 0;
        }

        return output;
    }
}