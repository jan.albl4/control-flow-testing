package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.Range;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class DoubleEvaluatorImpl implements DoubleEvaluator {

    @Override
    public void addDoubleValuesToParameter(PrimitiveParameter primitiveParameter, Range range){
        if(primitiveParameter != null && range != null) {

            List<String> values = getValueFromRange(range);

            for(String v: values){
                primitiveParameter.getValues().add(v);
            }
        }
    }

    @Override
    public List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter){
        List<Range> ranges = new ArrayList<>();

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new DoubleRange(Double.MIN_VALUE, primitiveParameter.getBorderValue().get(0).doubleValue()));
        }

        for (int i = 0; i < primitiveParameter.getBorderValue().size()-1; i++) {
            ranges.add(new DoubleRange(primitiveParameter.getBorderValue().get(i), primitiveParameter.getBorderValue().get(1+i)));
        }

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new DoubleRange(primitiveParameter.getBorderValue().get(primitiveParameter.getBorderValue().size()-1).doubleValue(), Double.MAX_VALUE));
        }

        return ranges;
    }

    private List<String> getValueFromRange(Range range){
        List<String> values = new ArrayList<>();

        values.add(String.valueOf(range.getMinimumDouble()));

        double min = 1; //aby se nestalo ze dostanu cislo k
        double max = range.getMaximumDouble() - range.getMinimumDouble() - 1;

        if (range.getMaximumDouble() - range.getMinimumDouble() < 0){
            //preteklo cislo je vetsi nez Integer.Maxint
            max = Double.MAX_VALUE - 1.0;
        }

        if (range.getMaximumDouble() - range.getMinimumDouble() != 0 && max > min) {
            Random random = new Random();
            double between = min + (max - min) * random.nextDouble();
            values.add(String.valueOf(between + range.getMinimumDouble()));
        }

        values.add(String.valueOf(range.getMaximumDouble()));

        return values;
    }

    @Override
    public String getRandomValue(){
        double minRandomValue = -100000;
        double maxRandomValue = 100000;
        Random r = new Random();
        double randomValue = minRandomValue + (maxRandomValue - minRandomValue) * r.nextDouble();
        return String.valueOf(randomValue);
    }
}
