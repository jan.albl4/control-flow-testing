package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import org.apache.commons.lang.math.Range;

import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public interface IntervalCalculator {

    List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter);

    String getRandomValue();
}
