package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.tools.log.Logger;
import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.expression.AST;
import org.matheclipse.core.expression.AST2;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.parser.client.eval.DoubleEvaluator;
import soot.Type;

import java.util.*;

/**
 *
 * Class that serves to evaluate the values of primitive equations.
 * In this project, we use the Symja to evaluate equations
 *
 * @author Jan Albl
 */
public class SymbolicEvaluatorImpl implements SymbolicEvaluator {

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    @Override
    public void evaluate(ConditionalExpression condition, Map<String, PrimitiveVariable> primitiveVariable) {

        Map<String, String> symbolMapper = this.createSymbolMaper(condition);

        if (condition.getArgTypeMap().size() == 1) {
            this.oneUnknownEquation(condition, primitiveVariable, symbolMapper, new ExprEvaluator());
        } else if(condition.getArgTypeMap().size() > 1 ){
            this.moreUnknownSymbolsEquation(condition, primitiveVariable, symbolMapper, new ExprEvaluator());
        }
    }

    /**
    * Evaluates the expression for one unknown in the equation
     * @param condition condition
     * @param primitiveVariable generated data will be stored here
     * @param symbolMapper symbol name mapper
     * @param exprEvaluator expression evaluator
    * */
    private void oneUnknownEquation(ConditionalExpression condition, Map<String, PrimitiveVariable> primitiveVariable, Map<String, String> symbolMapper, ExprEvaluator exprEvaluator){
        try {
            String equation = replaceComparativeOperators(condition.getExpression());
            equation = replaceSymbols(equation,symbolMapper);

            String argOriginname = condition.getArgTypeMap().entrySet().iterator().next().getKey();
            String arg = symbolMapper.get(argOriginname);

            logger.debug("Calculate: Roots("+equation +", "+arg+")");

            IExpr r = exprEvaluator.eval("Roots("+equation +", "+arg+")");
            List<Double> limits = getDoubleValue(r, arg);

            List<String> pom = new ArrayList<>();

            for(Double d: limits){
                pom.add(d.toString());
            }

            condition.getArgLimitValue().put(argOriginname, pom);

            crateNewPrimitiveParameterIfNotExist(primitiveVariable, argOriginname, condition);

            primitiveVariable.get(argOriginname).getBorderValue().addAll(limits);
        } catch (Exception e){
            logger.warn(e.toString());
            logger.debug("the condition \"" + condition.getExpression()+ "\" cannot be evaluated");
        }
    }

    /**
     * Evaluates the expression for more unknowns in the equation
     * @param condition condition
     * @param primitiveVariable generated data will be stored here
     * @param symbolMapper symbol name mapper
     * @param exprEvaluator expression evaluator
     * */
    private void moreUnknownSymbolsEquation(ConditionalExpression condition, Map<String, PrimitiveVariable> primitiveVariable, Map<String, String> symbolMapper, ExprEvaluator exprEvaluator){
        try {
            //random values for n-1 symbols and n-th evaluation
            String equation = replaceComparativeOperators(condition.getExpression());
            equation = replaceSymbols(equation,symbolMapper);

            List<String> arg = new ArrayList<>();

            Map<String, List<Double>> limits = new HashMap<>();

            int i = 1;
            for (Map.Entry<String, List<Type>> entry : condition.getArgTypeMap().entrySet()) {

                if(i < condition.getArgTypeMap().size()){
                    //char is positive
                    double value = getRandomValue(1, 100);
                    List<Double> pom = new ArrayList<>();
                    pom.add(value);
                    limits.put(entry.getKey(), pom);
                    exprEvaluator.defineVariable(symbolMapper.get(entry.getKey()), value);
                }
                arg.add(entry.getKey());
                i++;
            }

            logger.debug("Calculate: Roots("+equation +", "+symbolMapper.get(arg.get(arg.size()-1))+")");

            IExpr r = exprEvaluator.eval("Roots("+equation +", "+symbolMapper.get(arg.get(arg.size()-1))+")");
            limits.put(arg.get(arg.size()-1), getDoubleValue(r, symbolMapper.get(arg.get(arg.size()-1))));


            for (Map.Entry<String, List<Double>> entry : limits.entrySet()) {

                List<String> pom = new ArrayList<>();

                for(Double d: entry.getValue()){
                    pom.add(d.toString());
                }

                condition.getArgLimitValue().put(entry.getKey(), pom);

                crateNewPrimitiveParameterIfNotExist(primitiveVariable, entry.getKey(), condition);

                primitiveVariable.get(entry.getKey()).getBorderValue().addAll(entry.getValue());
            }
        } catch (Exception e){
            logger.warn(e.toString());
            logger.debug("the condition \"" + condition.getExpression()+ "\" cannot be evaluated");
        }
    }

    /**
     * if the data has not yet been generated for this symbol, it creates a new {@link PrimitiveVariable}
     * instance to store the generated data
     * @param primitiveVariable map of primitive variables
     * @param argOriginame the name of the symbol
     * @param condition condition
     * */
    private void crateNewPrimitiveParameterIfNotExist(Map<String, PrimitiveVariable> primitiveVariable, String argOriginame, ConditionalExpression condition){
        if (!primitiveVariable.containsKey(argOriginame)) {
            primitiveVariable.put(argOriginame, new PrimitiveVariable());
            primitiveVariable.get(argOriginame).setName(argOriginame);

            String type = condition.getArgTypeMap().get(argOriginame).get(condition.getArgTypeMap().get(argOriginame).size()-1).toString();
            primitiveVariable.get(argOriginame).setType(type);

            List<Type> types = condition.getArgTypeMap().get(argOriginame);
            primitiveVariable.get(argOriginame).setType(types.get(types.size()-1).toString());

            for(Type t: types){
                primitiveVariable.get(argOriginame).getParrentType().add(t.toString());
            }
        }
    }

    /**
     * Returns a random value between min and max
     * @param min min
     * @param max max
     * @return random value
     * */
    private double getRandomValue(double min, double max){
        return (Math.random() * ((max - min) + 1)) + min;
    }

    /**
     * According to the mapper it replaces the symbols in the equation
     * @param equation equation
     * @param symbolMapper symbol mapper
     * @return the resulting expression
     * */
    private String replaceSymbols(String equation, Map<String, String> symbolMapper){
        String output = " " + equation + " ";
        for (Map.Entry<String, String> entry : symbolMapper.entrySet()) {
            output = output.replace(" " + entry.getKey() + " ", entry.getValue());
        }
        return output;
    }

    /**
     * Obtains values for an input argument from an expression
     * @param r expression
     * @param arg argument
     * @return values
     * */
    private List<Double> getDoubleValue(IExpr r, String arg){

        List<Double> limitValues = new ArrayList<>();
        if(r != null){

            if(r.isAST0()){
                //this is not yet evaluated
            }
            if(r.isAST1()){
                //this is not yet evaluated
            }
            if(r.isAST2() && r instanceof AST2){
                limitValues.addAll(evalAST2(r, arg));
            }
            if(r.isAST3()){
                //this is not yet evaluated
            }

            if(r.isAST() && r instanceof AST){

                AST ast = (AST)r;

                Iterator iterator = ast.iterator();
                IExpr expr = (IExpr)iterator.next();

                //the equation has more solutions
                while (expr != null){

                    limitValues.addAll(evalAST2(expr, arg));

                    if(iterator.hasNext()){
                        expr = (IExpr) iterator.next();
                    }else {
                        expr = null;
                    }
                }
            }

            return limitValues;
        }

        return null;
    }

    /**
     * Obtains values for an input argument from an expression
     * @param expr expression
     * @param arg argument
     * @return values
     * */
    private List<Double> evalAST2(IExpr expr, String arg){

        List<Double> limitValues = new ArrayList<>();

        if(expr.isAST2() && expr instanceof AST2){

            org.matheclipse.parser.client.eval.DoubleEvaluator engine = new DoubleEvaluator(true);

            AST2 ast2 = (AST2)expr;

            if (!ast2.arg1().toString().equals(arg)) {
                double d = engine.evaluate(ast2.arg1().toString());
                limitValues.add(d);
            } else if(!ast2.arg2().toString().equals(arg)) {
                double d = engine.evaluate(ast2.arg2().toString());
                limitValues.add(d);
            }
        }

        return limitValues;
    }

    /**
     * Converts an inequality to an equation
     * @param inequality inequality
     * @return equation
     * */
    private  String replaceComparativeOperators(String inequality){
        String equation = null;
        if (inequality != null) {
            equation = inequality.replace("<=", "==");
            equation = equation.replace(">=", "==");
            equation = equation.replace("<", "==");
            equation = equation.replace(">", "==");
            equation = equation.replace("!=", "==");
        }
        return equation;
    }

    /***
     * Maper symbols. Symja cannot reverse a symbol that contains dots and other characters
     * @param condition condition containing symbols
     * @return the map where the key is a symbol from the condition and the value is its alias
     * */
    private Map<String, String> createSymbolMaper(ConditionalExpression condition){
        Map<String, String> symbolMapper = new HashMap<>();

        char startSymbol = 'a';
        int symbol = startSymbol;

        for (Map.Entry<String, List<Type>> entry : condition.getArgTypeMap().entrySet()) {
            symbolMapper.put(entry.getKey(),String.valueOf((char)symbol++)) ;
        }

        return symbolMapper;
    }

}
