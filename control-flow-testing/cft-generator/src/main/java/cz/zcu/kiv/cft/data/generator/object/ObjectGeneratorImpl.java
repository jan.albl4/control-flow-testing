package cz.zcu.kiv.cft.data.generator.object;

import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.output.data.Method;
import cz.zcu.kiv.cft.data.generator.data.ObjectVariable;
import cz.zcu.kiv.cft.output.data.ObjectParameter;
import cz.zcu.kiv.cft.output.data.Parameter;
import cz.zcu.kiv.cft.tools.log.Logger;
import soot.*;

import java.util.List;
import java.util.Map;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class ObjectGeneratorImpl implements ObjectGenerator{

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    public void evalCondition(ConditionalExpression c, Map<String, ObjectVariable> objectDataType){

        if (!DataUtil.isEnumConditon(c) && !DataUtil.isPrimitiveCondition(c)) {
            Map<String, List<Type>> atm = c.getArgTypeMap();

            if (atm.size() == 1){

                evalInstanceOf(c, objectDataType);

                if (c.getExpression().contains(" == null") || c.getExpression().contains(" != null")) {

                    String symbol = c.getExpression().replace(" == null", "");
                    symbol = symbol.replace(" != null", "").trim();

                    ObjectVariable ov;

                    if(!objectDataType.containsKey(symbol)){
                        ov = new ObjectVariable();
                        ov.setName(symbol);
                        ov.getParrentType().addAll(atm.get(symbol));
                        objectDataType.put(symbol, ov);
                    }

                    ov = objectDataType.get(symbol);
                    ov.setNullable(true);
                }
            }
        }
    }


    public void randomValuesToEmptySymbols(Method method){
        if (method!= null) {
            for(Parameter p : method.getMethodParameter()) {
                if(p instanceof ObjectParameter){
                    if(((ObjectParameter) p).getPossibleObjectTypes().size() == 0 && p.getSootType() != null && isObjetType(p.getSootType())) {

                        SootClass sootClass = null;

                        if(p.getSootType() instanceof RefType){
                            RefType refType = (RefType) p.getSootType();
                            sootClass = refType.getSootClass();
                        }else if (p.getSootType() instanceof ArrayType){
                            ArrayType arrayType = (ArrayType) p.getSootType();

                            if(arrayType.getElementType() instanceof RefType){
                                RefType refType = (RefType) arrayType.getElementType();
                                sootClass = refType.getSootClass();
                            }
                        }

                        if(sootClass != null && (sootClass.isInterface() || sootClass.isAbstract())){

                            List<SootClass> sootClasses = DataUtil.getSubClass(p.getSootType());

                            for(SootClass sc : sootClasses){
                                if(!sootClass.isInterface() && !sootClass.isAbstract()){
                                    ((ObjectParameter) p).getPossibleObjectTypes().add(sc.getType().toString());
                                    break;
                                }
                            }

                        } else if(sootClass != null) {
                            ((ObjectParameter) p).getPossibleObjectTypes().add(sootClass.getType().toString());
                        }
                    }
                }
            }
        }

    }

    public void fillObjectParameter(Method method, Map<String, ObjectVariable> objectDataType){
        for (Map.Entry<String, ObjectVariable> entry : objectDataType.entrySet()) {
            for(Parameter p : method.getMethodParameter()){

                String[] objectAndField = entry.getKey().split("\\.");
                String actualEntry = "";

                if(objectAndField.length > 0 && p.getName().equals(objectAndField[0])){
                    if(p instanceof ObjectParameter) {

                        ObjectParameter actual = (ObjectParameter) p;

                        for(int i = 0; i<objectAndField.length; i++){

                            actualEntry += objectAndField[i];

                            //neni primitivni
                            boolean canAdd = true;

                            //je to hned parametr
                            if(actual.getName().equals(objectAndField[i])){
                                canAdd = false;
                            }

                            for(ObjectParameter op : actual.getObjects()){
                                if(op.getName().equals(objectAndField[i])){
                                    actual = op;
                                    canAdd = false;
                                    break;
                                }
                            }

                            if(canAdd){
                                Type type = entry.getValue().getParrentType().get(i);

                                ObjectParameter op = new ObjectParameter();
                                op.setName(objectAndField[i]);
                                op.setSootType(type);
                                op.setPrimitive(false);

                                //je tam treba person.son a pak person.son.son
                                if(objectDataType.containsKey(actualEntry)){
                                    for(Type t: objectDataType.get(actualEntry).getDataType()){
                                        op.getPossibleObjectTypes().add(t.toString());
                                    }
                                }
                                // op.setPossibleObjectTypes();

                                actual.getObjects().add(op);
                                actual = op;
                            } else {
                                if(actual.getName().equals(objectAndField[i])){

                                    if(objectDataType.containsKey(actualEntry)){
                                        for(Type t: objectDataType.get(actualEntry).getDataType()){
                                            actual.getPossibleObjectTypes().add(t.toString());
                                        }

                                        if(objectDataType.get(actualEntry).isNullable()){
                                            actual.setCheckedNull(true);
                                        }
                                    }
                                }
                            }

                            actualEntry += ".";
                        }
                    }
                }
            }
        }
    }


    private boolean isObjetType(Type type){

        SootClass sootClass = null;

        if(type instanceof RefType &&  ((RefType)type).getSootClass() != null){
            sootClass = ((RefType)type).getSootClass();
        }else if(type instanceof ArrayType && ((ArrayType)type).getElementType() instanceof RefType){
            //sootClass = ((RefType)((ArrayType)type).getElementType()).getSootClass();
            return false;
        }

        if(sootClass != null){
            try{
                sootClass = ((RefType)type).getSootClass();
                while(sootClass != null){
                    if(sootClass.getName().equals("java.lang.Enum")){
                        return false;
                    }else if(sootClass.getName().equals("java.lang.Object")){
                        return true;
                    }

                    sootClass = sootClass.getSuperclass();
                }
            }catch (Exception e){
                //TODO log
            }
        }

        return false;
    }

    private void evalInstanceOf(ConditionalExpression c, Map<String, ObjectVariable> objectDataType){
        if(c.getExpression().contains(" instanceOf ")){
            String[] arrays = c.getExpression().split(" instanceOf ");

            Map<String, List<Type>> atm = c.getArgTypeMap();

            if (arrays.length > 1) {

                String symbol = arrays[0].trim();

                if (atm.containsKey(symbol)) {

                    String dataType = arrays[1].replace("== 0", "");
                    dataType = dataType.replace("!= 0", "");
                    dataType = dataType.trim();

                    ObjectVariable ov;

                    if(!objectDataType.containsKey(symbol)){
                        ov = new ObjectVariable();
                        ov.setName(symbol);
                        ov.getParrentType().addAll(atm.get(symbol));
                        objectDataType.put(symbol, ov);
                    }

                    ov = objectDataType.get(symbol);

                    setPosibleDataTypeFor(dataType, ov);
                    for(Type t : atm.get(symbol)){
                        setPosibleDataTypeFor(t.toString(), ov);
                    }
                }
            }
        }
    }

    private void setPosibleDataTypeFor(String dataTypeFor, ObjectVariable ov){

        try{
            SootClass sootClass = DataUtil.getSootClassFor(dataTypeFor);

            if(!sootClass.isAbstract()){

                if(!sootClass.isInterface()){
                    ov.getDataType().add(sootClass.getType());
                }

                List<SootClass> sc = DataUtil.getSubClass(sootClass.getType());

                for(SootClass s : sc){
                    if(!s.isPhantom() && !s.isAbstract() && !s.isInterface()){
                        ov.getDataType().add(s.getType());
                    }
                }
            }

        } catch (Exception e){
            logger.error(e.toString());
        }
    }
}
