package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.IntRange;
import org.apache.commons.lang.math.Range;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class BooleanEvaluatorImpl implements BooleanEvaluator {

    @Override
    public void addBooleanValuesToParameter(PrimitiveParameter primitiveParameter){
        primitiveParameter.getValues().add(String.valueOf("true"));
        primitiveParameter.getValues().add(String.valueOf("false"));
    }

    @Override
    public List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter) {

        List<Range> ranges = new ArrayList<>();

        ranges.add(new IntRange(0, 1));

        return ranges;
    }

    @Override
    public String getRandomValue(){
        return DataUtil.getRandomNumberInRange(0, 1) % 2 == 0 ? "true": "false";
    }
}
