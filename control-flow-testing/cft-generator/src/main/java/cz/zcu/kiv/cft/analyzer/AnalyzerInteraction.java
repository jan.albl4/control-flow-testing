package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;

/**
 * Interface class for analyzing the interaction strength of input parameters of method in {@link ControlFlowAnalysisOutput}
 *
 * @author Jan Albl
 */
public interface AnalyzerInteraction {

    /**
     * Analyzing the interaction strength of input parameters of method in {@link ControlFlowAnalysisOutput}
     *
     * @param controlFlowAnalysisOutput analyzed structure
     * */
    void analyzeInteraction(ControlFlowAnalysisOutput controlFlowAnalysisOutput);
}
