package cz.zcu.kiv.cft.data.generator.enums;

import cz.zcu.kiv.cft.data.generator.data.EnumVariable;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.output.data.Method;

import java.util.Map;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public interface EnumValueGenerator {

    void evalInstanceOf(ConditionalExpression c, Map<String, EnumVariable> objectDataType);

    void randomValuesToEmptySymbols(Method method);

    void fillEnumParameter(Method method, Map<String, EnumVariable> objectDataType);
}
