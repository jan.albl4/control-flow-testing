package cz.zcu.kiv.cft.analyzer.exception;

/**
 *
 * Exception that is thrown when the class loader is not set
 *
 * @author Jan Albl
 */
public class ClassLoaderNotSetException extends RuntimeException {

    /**
     * Constructor to create an instance of this class.
     * *
     * @param message message exceptions
     * */
    public ClassLoaderNotSetException(String message){
        super(message);
    }
}