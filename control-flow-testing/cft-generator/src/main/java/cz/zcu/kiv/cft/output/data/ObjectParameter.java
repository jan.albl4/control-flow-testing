package cz.zcu.kiv.cft.output.data;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class representing an object parameter
 * @author Jan Albl
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectParameter extends Parameter {

    /** Flag of the interface class */
    private boolean isInterface;

    /** Flag of the abstract class */
    private boolean isAbstract;

    /** Flag whether the parameter should be null-tested */
    private boolean checkedNull;

    /** List of nested objects */
    @XmlElementWrapper(name = "objects")
    @XmlElement(name = "object")
    private List<ObjectParameter> objects = new ArrayList();

    /** List of nested primitive fields */
    @XmlElementWrapper(name = "primitiveFields")
    @XmlElement(name = "primitiveField")
    private List<PrimitiveParameter> primitiveFields = new ArrayList();

    /** List of nested enum fields */
    @XmlElementWrapper(name = "enumFields")
    @XmlElement(name = "enumField")
    private List<EnumParameter> enumParameters = new ArrayList();

    /** Possible object data types */
    @XmlElementWrapper(name = "possibleObjectTypes")
    @XmlElement(name = "possibleObjectType")
    private Set<String> possibleObjectTypes = new HashSet<>();

    public boolean isCheckedNull() {
        return checkedNull;
    }

    public void setCheckedNull(boolean checkedNull) {
        this.checkedNull = checkedNull;
    }

    public List<ObjectParameter> getObjects() {
        return objects;
    }

    public void setObjects(List<ObjectParameter> objects) {
        this.objects = objects;
    }

    public List<PrimitiveParameter> getPrimitiveFields() {
        return primitiveFields;
    }

    public void setPrimitiveFields(List<PrimitiveParameter> primitiveFields) {
        this.primitiveFields = primitiveFields;
    }

    public Set<String> getPossibleObjectTypes() {
        return possibleObjectTypes;
    }

    public void setPossibleObjectTypes(Set<String> possibleObjectTypes) {
        this.possibleObjectTypes = possibleObjectTypes;
    }

    public boolean isInterface() {
        return isInterface;
    }

    public void setInterface(boolean anInterface) {
        isInterface = anInterface;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setAbstract(boolean anAbstract) {
        isAbstract = anAbstract;
    }

    public List<EnumParameter> getEnumParameters() {
        return enumParameters;
    }

    public void setEnumParameters(List<EnumParameter> enumParameters) {
        this.enumParameters = enumParameters;
    }

}
