package cz.zcu.kiv.cft.output;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zcu.kiv.cft.output.data.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

/**
 *
 * Class for converting output objects to XML or JSON
 *
 * @author Jan Albl
 */
public class OutputConverter {

    public static String convertToXml(ControlFlowAnalysisOutput controlFlow) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ControlFlowAnalysisOutput.class, SwitchConditions.class, Condition.class,  Parameter.class,  PrimitiveParameter.class, ObjectParameter.class, EnumParameter.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(controlFlow, sw);

            //jaxbMarshaller.marshal(controlFlow, System.out);

            return sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void convertToXmlFile(ControlFlowAnalysisOutput controlFlow, String filePath) {
        try {
            File file = new File(filePath);
            JAXBContext jaxbContext = JAXBContext.newInstance(ControlFlowAnalysisOutput.class, SwitchConditions.class, Condition.class,  Parameter.class,  PrimitiveParameter.class, ObjectParameter.class, EnumParameter.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(controlFlow, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static String convertToJson(ControlFlowAnalysisOutput controlFlow){
        ObjectMapper mapper = new ObjectMapper();

        try {
            return  mapper.writerWithDefaultPrettyPrinter().writeValueAsString(controlFlow);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void convertToJsonFile(ControlFlowAnalysisOutput controlFlow, String filePath){
        ObjectMapper mapper = new ObjectMapper();

        try {
            // Convert object to JSON string and save into a file directly
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File( filePath ), controlFlow);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
