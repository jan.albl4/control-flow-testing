package cz.zcu.kiv.cft.output.data;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class contains basic information about analyzed and generated data
 *
 * @author Jan Albl
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ControlFlowAnalysisOutput {

    /** List of all analyzed classes */
    @XmlElementWrapper(name = "classes")
    @XmlElement(name = "class")
    private List<Clazz> classes = new ArrayList<>();

    public List<Clazz> getClasses() {
        return classes;
    }

    public void setClasses(List<Clazz> clazz) {
        this.classes = clazz;
    }
}
