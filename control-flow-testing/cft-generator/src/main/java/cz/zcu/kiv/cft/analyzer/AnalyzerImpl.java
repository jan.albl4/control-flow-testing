package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.analyzer.exception.AnalyzerVariableTableNotSetException;
import cz.zcu.kiv.cft.analyzer.exception.ClassLoaderNotSetException;
import cz.zcu.kiv.cft.analyzer.exception.ProjectPathNotSetException;
import cz.zcu.kiv.cft.analyzer.branches.BranchStackItem;
import cz.zcu.kiv.cft.analyzer.branches.BranchStack;
import cz.zcu.kiv.cft.analyzer.branches.SwitchStackItem;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoader;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoaderImpl;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolTableAnalyzer;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.*;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.output.data.Method;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolTableStack;
import cz.zcu.kiv.cft.tools.Utils;
import cz.zcu.kiv.cft.tools.log.Logger;
import cz.zcu.kiv.cft.tools.statistics.Statistics;
import soot.*;
import soot.jimple.*;
import soot.toolkits.graph.UnitGraph;

import java.util.*;

/**
 *
 * Class for analyzing java source files. The output of this interface is an  {@link ControlFlowAnalysisOutput}
 * data structure that contains the information obtained from the source code. It can also obtain a control flow graph from the source code.
 *
 * @author Jan Albl
 */
public class AnalyzerImpl implements Analyzer {

    /** Maximum depth of search */
    private int maxDepth;

    /** Path to the analyzed project */
    private String projectPath;

    /** Package of analyzed class */
    private String packagePath;

    /** {@link SootClassLoader} */
    private SootClassLoader sootClassLoader;

    /** {@link OutputInitializer} */
    private OutputInitializer outputInitializer;

    /** {@link SymbolTableAnalyzer} */
    private SymbolTableAnalyzer symbolTableAnalyzer;

    /** {@link AnalyzerInteraction} */
    private AnalyzerInteraction analyzerInteraction;

    /**
     * Symbol tables of all branch conditions in the flow control graph.
     * Methods can call themselves. Therefore, the node's unique identifier
     * is its Unit and UnitGraph  in which the unit is included
     * */
    private Map<Unit, Map<UnitGraph, List<SymbolTableStack>>> variableTables;

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    /**
     * Constructor to create an instance of this class. This can only be created through an internal builder
     * @param projectPath {@link #projectPath}
     * @param packagePath {@link #packagePath}
     * @param maxDepth {@link #maxDepth}
     * @param symbolTableAnalyzer {@link #symbolTableAnalyzer}
     * @param loadNecessaryClasses loads all classes on the project and other referenced ones
     * **/
    private AnalyzerImpl(String projectPath, String packagePath, int maxDepth, SymbolTableAnalyzer symbolTableAnalyzer, boolean loadNecessaryClasses){
        this.projectPath = projectPath;
        this.maxDepth = maxDepth;
        this.packagePath = packagePath;
        this.symbolTableAnalyzer = symbolTableAnalyzer;
        this.setAnalyzerVariableTableImpl(symbolTableAnalyzer);
        this.analyzerInteraction = new AnalyzerInteractionImpl();
        this.setSootClassLoaderImpl(this.projectPath, loadNecessaryClasses);
        this.outputInitializer = new OutputInitializerImpl(this.sootClassLoader);
    }

    @Override
    public ControlFlowAnalysisOutput analyzeMethod(String className, String methodName) {
        return this.analyzeMethod(className, methodName, null, null);
    }

    @Override
    public ControlFlowAnalysisOutput analyzeMethod(String className, String methodName, List<String> parameters){
        return this.analyzeMethod(className, methodName, parameters, null);
    }

    @Override
    public ControlFlowAnalysisOutput analyzeMethod(String className, String methodName, List<String> parameters, String returnType){

        this.checkSootClassLoader();

        logger.info("analyze the "+ methodName +" method of "+ className +" class");

        String fullClassName = getFullClassName(className);
        SootMethod sootMethod = sootClassLoader.getSootMethod(fullClassName, methodName, parameters, returnType);
        SootClass sc = sootMethod.getDeclaringClass();

        if(!DataUtil.isEnumType(sc.getType()) && !sc.isAbstract() && !sc.isPhantom() && !sc.isInterface()) {
            ControlFlowAnalysisOutput cfo = outputInitializer.initFromSootMethod(sootMethod);
            cfo.getClasses().get(0).setClassPath(this.projectPath);

            ControlFlowGraph cfg = new ControlFlowGraph(sootMethod, this.maxDepth);

            analyze(cfg,cfo.getClasses().get(0).getMethods().get(0));
            analyzerInteraction.analyzeInteraction(cfo);

            logger.info("end analyze the "+ methodName +" method of "+ className + " class");

            return cfo;
        } else {
            logger.warn("the "+methodName+" method of " + className +" cannot be parsed. The structure is complex, or you cannot load specific implementations");
            return null;
        }
    }

    @Override
    public ControlFlowAnalysisOutput analyzeClass(String className) {

        this.checkSootClassLoader();

        logger.info("analyzing class: "+ className);

        String fullClassName = getFullClassName(className);
        SootClass sootClass = sootClassLoader.getSootClass(fullClassName);

        ControlFlowAnalysisOutput cfo = outputInitializer.initFromSootClass(sootClass);
        cfo.getClasses().get(0).setClassPath(this.projectPath);

        Statistics.addAnalyzedClass();

        if(!DataUtil.isEnumType(sootClass.getType()) && !sootClass.isAbstract() && !sootClass.isPhantom() && !sootClass.isInterface()){
            for(SootMethod sootMethod : sootClass.getMethods()){
                ControlFlowGraph cfg = new ControlFlowGraph(sootMethod, this.maxDepth);

                Method method = outputInitializer.initFromSootMethod(sootMethod, cfo.getClasses().get(0));
                analyze(cfg, method);

                Statistics.addAnalyzedMethod();
            }
        } else {
            logger.warn("\""+ className + "\" class cannot be analyzed. The structure is complex, or you cannot load specific implementations");
        }

        analyzerInteraction.analyzeInteraction(cfo);

        logger.info("end analysis class: "+ className);

        return cfo;

    }

    @Override
    public ControlFlowAnalysisOutput analyzeFolder(String projectFolderPath) {

        ControlFlowAnalysisOutput cfo = new ControlFlowAnalysisOutput();

        Map<String, List<String>> classAndPackageMap = Utils.getClassAndPackagesFromFolder(projectFolderPath);
        int j = 0; // only for logger

        for (Map.Entry<String,List<String>> entry : classAndPackageMap.entrySet()) {

            logger.info("Process package " + (j++) + "/" + classAndPackageMap.size() + " e: " + entry.getKey());
            int i = 0; // only for logger

            for(String clazz : entry.getValue()){

                logger.info("Process " + (i++) + "/" + entry.getValue().size() + " " + clazz);

                try{
                    this.packagePath = entry.getKey();
                    this.projectPath = projectFolderPath;
                    this.setSootClassLoaderImpl(this.projectPath, false);

                    cfo.getClasses().addAll(this.analyzeClass(clazz).getClasses());
                }catch (Exception e){
                    logger.warn(e.toString());
                }
            }
        }

        return cfo;
    }

    @Override
    public ControlFlowGraph getControlFlowGraph(String className, String methodName, List<String> parameters, String returnType) {

        checkSootClassLoader();

        logger.info("analyze the "+ methodName +" method of "+ className +" class");

        String fullClassName = getFullClassName(className);
        SootMethod sootMethod = sootClassLoader.getSootMethod(fullClassName, methodName, parameters, returnType);
        SootClass sc = sootMethod.getDeclaringClass();

        if(!DataUtil.isEnumType(sc.getType()) && !sc.isAbstract() && !sc.isPhantom() && !sc.isInterface()) {
            ControlFlowAnalysisOutput cfo = outputInitializer.initFromSootMethod(sootMethod);
            cfo.getClasses().get(0).setClassPath(this.projectPath);

            return new ControlFlowGraph(sootMethod, this.maxDepth);

        } else {
            logger.warn("the "+methodName+" method of " + className +" cannot be parsed. The structure is complex, or you cannot load specific implementations");
            return null;
        }
    }

    /**
     * Analyze a given control flow graph and stores the result in the {@link Method} data structure
     *
     * @param controlFlowGraph control flow graph
     * @param method structure to save the result
     * */
    private void analyze(ControlFlowGraph controlFlowGraph, Method method){

        variableTables  = symbolTableAnalyzer.symbolTableAnalysis(controlFlowGraph);

        this.analyzeUnitGraph(controlFlowGraph, controlFlowGraph.getUnitGraph(), method);
    }

    /**
     * Analyzes the UnitGraph of the control flow graph and stores the result in {@link Method} structure
     *
     * @param cfg control flow graph
     * @param unitGraph analyzed UnitGraph
     * @param method structure to save the result
     * */
    private void analyzeUnitGraph(ControlFlowGraph cfg, UnitGraph unitGraph, Method method){

        Iterator i = unitGraph.iterator();
        Unit u = (Unit)i.next();

        while (u != null) {
            logger.debug("analyzing unit: " + u.toString());

            Unit end = cfg.endConditionNodes(u, unitGraph);

            this.goThroughBranching(u, end, unitGraph, cfg, method);

            if (end != null) {
                u = end;
            } else {
                u = null;
            }
        }
    }

    /**
     * Analyzes all branches of a control flow graph to the end node.
     *
     * @param u unit from unitGraph
     * @param unitGraph unit graph. Part of the flow control graph
     * @param branchStack stack unprocessed branching
     * @param end end node
     * @param cfg control flow graph
     * */
    private void goTrueBranches(Unit u, UnitGraph unitGraph, BranchStack branchStack, Unit end, ControlFlowGraph cfg){
        if(end == null || !end.equals(u)){
            while (u != null) {
                if (u instanceof DefinitionStmt){
                    definitionStmt(u, branchStack, unitGraph, cfg);
                } else if (u instanceof IfStmt){
                    ifStmt(u, branchStack, unitGraph, cfg);
                }else if (u instanceof TableSwitchStmt || u instanceof LookupSwitchStmt) {
                    switchStmt(u, branchStack, unitGraph, cfg);
                }else if(u instanceof InvokeStmt){
                    invokeStmt(u, branchStack, unitGraph, cfg);
                }

                if(unitGraph.getSuccsOf(u) != null && unitGraph.getSuccsOf(u).size() > 0){

                    Unit next = unitGraph.getSuccsOf(u).get(0);

                    if (end != null && end.equals(next) || cfg.getCycleEdge().containsKey(u) && cfg.getCycleEdge().get(u).equals(next)){
                        u = null;
                    } else {
                        u = next;
                    }

                }else{
                    u = null;
                }
            }
        }

        branchStack.markProcessedNode();
    }


    /**
     *  If the input node contains a method call, it scans it to the maximum depth
     *
     * @param u unit from unitGraph
     * @param branchStack stack unprocessed branching
     * @param unitGraph unit graph. Part of the flow control graph
     * @param cfg control flow graph
     * */
    private void definitionStmt(Unit u, BranchStack branchStack, UnitGraph unitGraph, ControlFlowGraph cfg) {

        if (u instanceof DefinitionStmt) {
            DefinitionStmt assign = (DefinitionStmt) u;
            Value rightOp = assign.getRightOp();

            if (rightOp instanceof VirtualInvokeExpr || rightOp instanceof StaticInvokeExpr || rightOp instanceof SpecialInvokeExpr) {
                methodCall(u, branchStack, unitGraph, cfg);
            }
        }
    }

    /**
     *  If the input node is a method call (InvokeStmt), it scans it to the maximum depth
     *
     * @param u unit from unitGraph
     * @param branchStack stack unprocessed branching
     * @param unitGraph unit graph. Part of the flow control graph
     * @param cfg control flow graph
     * */
    private void invokeStmt(Unit u, BranchStack branchStack, UnitGraph unitGraph, ControlFlowGraph cfg){
        if (u instanceof InvokeStmt) {
            methodCall(u, branchStack, unitGraph, cfg);
        }
    }

    /**
     * If the input node is a method call, it scans it to the maximum depth
     *
     * @param u unit from unitGraph
     * @param branchStack stack unprocessed branching
     * @param unitGraph unit graph. Part of the flow control graph
     * @param cfg control flow graph
     * */
    private void methodCall(Unit u, BranchStack branchStack, UnitGraph unitGraph, ControlFlowGraph cfg){
        if (maxDepth > 0) {
            maxDepth--;

            if(cfg.getUnitGraphMap().containsKey(u)
                    && cfg.getUnitGraphMap().get(u).containsKey(unitGraph)){

                analyzeUnitGraph(cfg, cfg.getUnitGraphMap().get(u).get(unitGraph), branchStack.getMethod());

            }
            maxDepth++;
        }
    }

    /**
     * If the branch Unit is of the IfStmt type, it evaluates whether the condition is dependent on the input
     * parameters and inserts the If into the stack that will be further processed.
     *
     * @param u unit from unitGraph
     * @param branchStack stack unprocessed branching
     * @param unitGraph unit graph. Part of the flow control graph
     * @param cfg control flow graph
     * */
    private void ifStmt(Unit u, BranchStack branchStack, UnitGraph unitGraph, ControlFlowGraph cfg){
        if(u instanceof IfStmt && unitGraph.getSuccsOf(u) != null && unitGraph.getSuccsOf(u).size() == 2 && !cfg.getCycleEdge().containsKey(u)){
            BranchStackItem branchStackItem = new BranchStackItem(u, cfg.endConditionNodes(u, unitGraph));
            branchStackItem.setAllVariableTables(variableTables.get(u).get(unitGraph));
            branchStackItem.setSymbolTableStack(variableTables.get(u).get(unitGraph).get(0));
            branchStack.push(branchStackItem, true);
            logger.debug("found branching condition - if");
        }
    }

    /**
     * If the branch Unit is of the SwitchStmt type, it evaluates whether the condition is dependent on the input
     * parameters and inserts the Switch into the stack that will be further processed.
     *
     * @param u unit from unitGraph
     * @param branchStack stack unprocessed branching
     * @param unitGraph unit graph. Part of the flow control graph
     * @param cfg control flow graph
     * */
    private void switchStmt(Unit u, BranchStack branchStack, UnitGraph unitGraph, ControlFlowGraph cfg) {
        if(u instanceof TableSwitchStmt || u instanceof LookupSwitchStmt) {
            SwitchStackItem stackItem = new SwitchStackItem(u, cfg.endConditionNodes(u, unitGraph));

            stackItem.setAllVariableTables(variableTables.get(u).get(unitGraph));
            stackItem.setSymbolTableStack(variableTables.get(u).get(unitGraph).get(0));
            branchStack.push(stackItem, true);
            logger.debug("found branching condition - switch");
        }
    }

    /**
     * Searches the graph from the input node to the end node and analyzes the branching within the graph
     *
     * @param u entry node
     * @param end end node
     * @param unitGraph searched UnitGraph
     * @param cfg control flow graph
     * @param method structure to save the result
     * **/
    private void goThroughBranching(Unit u, Unit end, UnitGraph unitGraph, ControlFlowGraph cfg, Method method){

        BranchStack branchStack = new BranchStack();
        branchStack.setMethod(method);

        this.goTrueBranches(u, unitGraph, branchStack, end, cfg);

        while (branchStack.size() != 0){

            u = branchStack.getNextUnprocessedNode(unitGraph);

            goTrueBranches(u, unitGraph, branchStack, end, cfg);

            logger.debug("count of unprocessed branches: " + branchStack.size());
        }
    }


    @Override
    public List<String> getDots(String className) {

        checkSootClassLoader();

        List<String> dots = new ArrayList<>();

        String fullClassName = getFullClassName(className);
        SootClass sootClass = sootClassLoader.getSootClass(fullClassName);

        if(!DataUtil.isEnumType(sootClass.getType()) && !sootClass.isAbstract() && !sootClass.isPhantom() && !sootClass.isInterface()){
            for(SootMethod sootMethod : sootClass.getMethods()){
                ControlFlowGraph cfg = new ControlFlowGraph(sootMethod, this.maxDepth);
                dots.add(cfg.getDot());
            }
        } else {
            logger.error("\"" + className + "\" class cannot be analyzed");
        }

        return dots;
    }

    @Override
    public String getDots(String className, String methodName, List<String> parameters, String returnType) {

        checkSootClassLoader();

        String fullClassName = getFullClassName(className);
        SootMethod sootMethod = sootClassLoader.getSootMethod(fullClassName, methodName, parameters, returnType);

        return (new ControlFlowGraph(sootMethod, this.maxDepth)).getDot();
    }

    @Override
    public Map<String, List<String>> getDost(String projectFolderPath) {

        checkSootClassLoader();

        Map<String, List<String>> dotsMap = new HashMap<>();
        Map<String, List<String>> classAndPackageMap = Utils.getClassAndPackagesFromFolder(projectFolderPath);
        int j = 0; // only for logger

        for (Map.Entry<String,List<String>> entry : classAndPackageMap.entrySet()) {

            logger.info("Process package " + (j++) + "/" + classAndPackageMap.size() + " e: " + entry.getKey());
            int i = 0; // only for logger

            for(String clazz : entry.getValue()){

                logger.info("Process " + (i++) + "/" + entry.getValue().size() + " " + clazz);

                try{
                    this.packagePath = entry.getKey();
                    this.projectPath = projectFolderPath;
                    this.setSootClassLoaderImpl(this.projectPath, false);

                    dotsMap.put(clazz, this.getDots(clazz));

                }catch (Exception e){
                    logger.warn(e.toString());
                }
            }
        }

        return dotsMap;
    }

    /**
     * Checks whether the appropriate class loader is set, if not set, throws an exception
     * */
    private void checkSootClassLoader() {
        if (this.sootClassLoader == null) {
            throw new ClassLoaderNotSetException("Class loader not set.");
        }
    }

    /**
     * Sets the symbol table analyzer to be used for analysis
     *
     * @param symbolTableAnalyzer {@link SymbolTableAnalyzer}
     * */
    private void setAnalyzerVariableTableImpl(SymbolTableAnalyzer symbolTableAnalyzer){
        if(symbolTableAnalyzer != null){
            symbolTableAnalyzer.setMaxDepth(maxDepth);
        } else {
            throw new AnalyzerVariableTableNotSetException("Analyzer variable table not set.");
        }
    }


    /**
     * Sets the loader according to the project path and the relative class loading type
     *
     * @param projectPath  {@link #projectPath}
     * @param loadNecessaryClasses loads all classes on the project and other referenced ones
     * */
    private void setSootClassLoaderImpl(String projectPath, boolean loadNecessaryClasses){
        if (projectPath != null) {
            this.sootClassLoader = SootClassLoaderImpl.getInstance(projectPath, loadNecessaryClasses);
        } else {
            throw new ProjectPathNotSetException("Class path not set");
        }
    }

    /**
     * Returns the full class name with the package it is in
     *
     * @param className name of test class
     * @return returns the full class name with the package it is in
     * */
    private String getFullClassName(String className){
        String fullClassName = className;

        if (this.packagePath != null && this.packagePath.length() > 0) {
            fullClassName = this.packagePath + "." + fullClassName;
        }

        return fullClassName;
    }

    /**
     * Builder that creates new instance of {@link AnalyzerImpl}
     *
     * @author Jan Albl
     */
    public static class Builder{

        /** Path to the analyzed project */
        private String projectPath;

        /** Package of analyzed class */
        private String packagePath;

        /** analyzer of symbol tables */
        private SymbolTableAnalyzer symbolTableAnalyzer;

        /** Maximum depth of search */
        private int maxDepth;

         /** loads all classes on the project and other referenced ones */
        private boolean loadNecessaryClasses = false;

        public Builder setLoadNecessaryClasses(boolean loadNecessaryClasses){
            this.loadNecessaryClasses=loadNecessaryClasses;
            return this;
        }

        public Builder setProjectPath(String projectPath){
            this.projectPath = projectPath;
            return this;
        }

        public Builder setPackagePath(String packagePath){
            this.packagePath = packagePath;
            return this;
        }

        public Builder setMaxDepth(int maxDepth){
            if (maxDepth >= 0) {
                this.maxDepth = maxDepth;
            }
            return this;
        }

        public Builder setSymbolTableAnalyzer(SymbolTableAnalyzer symbolTableAnalyzer){
            this.symbolTableAnalyzer = symbolTableAnalyzer;
            return this;
        }

        public AnalyzerImpl build(){
            return new AnalyzerImpl(projectPath, packagePath, maxDepth, symbolTableAnalyzer, loadNecessaryClasses);
        }
    }
}