package cz.zcu.kiv.cft.analyzer.evaluator;

import cz.zcu.kiv.cft.tools.Utils;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoader;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.analyzer.symbols.*;
import soot.RefType;
import soot.SootClass;
import soot.Type;
import soot.Value;
import soot.jimple.*;
import soot.jimple.internal.JCastExpr;
import soot.jimple.internal.JInstanceFieldRef;
import soot.jimple.internal.JSubExpr;
import soot.jimple.internal.JimpleLocal;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class that unfolds the symbols in the branch condition to the base form according to the symbol table
 *
 * @author Jan Albl
 */
public class ConditionDeveloper {

    /** {@link SootClassLoader} for loading classes in a tested project */
    private static SootClassLoader sootClassLoader;

    /**
     * Constructor to create an instance of this class.
     *
     * @param sootClassLoader {@link SootClassLoader} for loading classes in a tested project
     * **/
    public ConditionDeveloper(SootClassLoader sootClassLoader){
        this.sootClassLoader = sootClassLoader;
    }

    /**
     * Unfolds the symbols in the branch condition to the base form according to the symbol table
     * @param hsi history symbol
     * @param condition structure where branching information is stored
     * @return condition in basic form
     * */
    public String parameterExpression(HistorySymbolItem hsi, ConditionalExpression condition){

        String out = "";

        if (hsi.getSootValue() instanceof IntConstant
                || hsi.getSootValue() instanceof DoubleConstant
                || hsi.getSootValue() instanceof FloatConstant
                || hsi.getSootValue() instanceof LongConstant
                || hsi.getSootValue() instanceof NullConstant) {

            return opToString(hsi.getSootValue());
        }

        if (hsi instanceof HistoryClassField && hsi.getSootValue() == null) {

            if(((HistoryClassField)hsi).getParentHSI().getSootValue() instanceof ParameterRef){
                out = ((HistoryClassField)hsi).getSymbolItem().getName() + "." + ((HistoryClassField)hsi).getName();

                if(!condition.getArgTypeMap().containsKey(out)){
                    condition.getArgTypeMap().put(out, new ArrayList<>());
                }

                condition.getArgTypeMap().get(out).add(((HistoryClassField)hsi).getSymbolItem().getType());
                condition.getArgTypeMap().get(out).add(((HistoryClassField)hsi).getFieldRef().getType());
            }
        }

        if (hsi.getSootValue() instanceof BinopExpr) {
            BinopExpr binopExpr = (BinopExpr)hsi.getSootValue();
            out = " ( ";
            if(hsi.getLeftOp() != null && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)){
                out += parameterExpression(hsi.getLeftOp(), condition);
            }else {
                out += opToString(binopExpr.getOp1());
                fillArgTypeMap(binopExpr.getOp1(), condition);
            }
            out += " " + binopExpr.getSymbol() + " ";

            if(hsi.getRightOp() != null  && !(hsi.getRightOp().getSootValue() instanceof ParameterRef)){
                out += parameterExpression(hsi.getRightOp(), condition);
            }else {
                out += opToString(binopExpr.getOp2());
                fillArgTypeMap(binopExpr.getOp2(), condition);
            }
            out += " ) ";
        } else if(hsi.getSootValue() instanceof JCastExpr) {
            JCastExpr jCastExpr = (JCastExpr)hsi.getSootValue();

            if(hsi.getLeftOp() != null && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)){
                out += parameterExpression(hsi.getLeftOp(), condition);
            }else {
                out += opToString(jCastExpr.getOp());
                fillArgTypeMap(jCastExpr.getOp(), condition);
            }
        } else if(hsi.getSootValue() instanceof JimpleLocal) {

            if(hsi.getLeftOp() != null && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)){
                out += parameterExpression(hsi.getLeftOp(), condition);
            }else {
                out += opToString(hsi.getSootValue());
                fillArgTypeMap(hsi.getSootValue(), condition);
            }

        } else if(hsi.getSootValue() instanceof JInstanceFieldRef) {
            JInstanceFieldRef jInstanceFieldRef = (JInstanceFieldRef) hsi.getSootValue();
            if (hsi.getLeftOp() != null && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)) {
                out += parameterExpression(hsi.getLeftOp(), condition);
            } else {
                if(hsi.getLeftOp().getSootValue() instanceof ParameterRef){
                    out += jInstanceFieldRef.getBase() + "." + jInstanceFieldRef.getField().getName();
                }else{
                    out += opToString(hsi.getSootValue());
                }

            }

        } else if(hsi.getSootValue() instanceof JSubExpr) {
            JSubExpr jubExpr = (JSubExpr)hsi.getSootValue();

            if (hsi.getLeftOp() != null  && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)) {
                out = parameterExpression(hsi.getLeftOp(), condition);
            } else {
                out = opToString(jubExpr.getOp1());
            }
            out += " " + jubExpr.getSymbol() + " ";

            if (hsi.getRightOp() != null  && !(hsi.getRightOp().getSootValue() instanceof ParameterRef)) {
                out += parameterExpression(hsi.getRightOp(), condition);
            } else {
                out += opToString(jubExpr.getOp2());
            }

        } else if (hsi.getSootValue() instanceof VirtualInvokeExpr) {
            VirtualInvokeExpr virtualInvokeExpr = (VirtualInvokeExpr)hsi.getSootValue();

            if(hsi.getLeftOp() != null  && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)){
                out = parameterExpression(hsi.getLeftOp(), condition);
            } else{

                if (Utils.isGetField((virtualInvokeExpr))){
                    out = virtualInvokeExpr.getBase().toString() + "."+virtualInvokeExpr.getMethod().getName().substring(3);
                } else if(virtualInvokeExpr.getMethod().getDeclaringClass().toString().equals("java.lang.String")
                        && virtualInvokeExpr.getMethod().getName().equals("equals")) {

                    out = parameterExpression(hsi.getCallObject(), condition);

                    out += " equals ";

                    if(hsi.getParameters().size() == 0) {
                        out += (virtualInvokeExpr.getArg(0));
                    } else {
                        out += parameterExpression(hsi.getParameters().get(0), condition);
                    }

                }
            }

        }else if(hsi.getSootValue() instanceof LengthExpr){
            LengthExpr lengthExpr = (LengthExpr)hsi.getSootValue();

            if(hsi.getLeftOp() != null  && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)){
                out = parameterExpression(hsi.getLeftOp(), condition);
            }else {
                out = " lengthof " + lengthExpr.getOp().toString();
                fillArgTypeMap(lengthExpr.getOp(), condition);
            }

        }else if(hsi.getSootValue() instanceof InstanceOfExpr){
            InstanceOfExpr instanceOfExpr = (InstanceOfExpr)hsi.getSootValue();

            if (hsi.getLeftOp() != null  && !(hsi.getLeftOp().getSootValue() instanceof ParameterRef)) {
                out = parameterExpression(hsi.getLeftOp(), condition);
            } else {
                out = opToString(instanceOfExpr.getOp());
                fillArgTypeMap(instanceOfExpr.getOp(), condition);
            }

            evalInstanceOf(out, instanceOfExpr, condition);
            out += " instanceOf " +instanceOfExpr.getCheckType();

        }else if(hsi instanceof HistoryClassField){

            if(hsi.getLeftOp() == null && hsi.getRightOp() == null && hsi.getSootValue() == null){

                if(!(((HistoryClassField) hsi).getParentHSI().getSootValue() instanceof ParameterRef)){
                    out = parameterExpression(((HistoryClassField) hsi).getParentHSI(), condition);

                    List<Type> parrentType = condition.getArgTypeMap().get(out);
                    condition.getArgTypeMap().remove(out);

                    out += "."+((HistoryClassField) hsi).getName();

                    condition.getArgTypeMap().put(out, new ArrayList<>());
                    condition.getArgTypeMap().get(out).addAll(parrentType);
                    condition.getArgTypeMap().get(out).add(((HistoryClassField) hsi).getFieldRef().getType());
                }
            }
        } else if(hsi.getSootValue() instanceof StaticFieldRef){
            out += ((StaticFieldRef)(hsi.getSootValue())).getField().getType();
            out += "."+((StaticFieldRef)(hsi.getSootValue())).getField().getName();

        } else if(hsi.getSootValue() != null){
            out += hsi.getSootValue().toString();
        }

        return out;
    }

    /**
     * Evaluates instances of in condition. Saves possible object data types to the {@link ConditionalExpression} data structure
     * @param arg under which symbol information is stored
     * @param instanceOfExpr instance of expression
     * @param condition information is stored here
     * */
    private void evalInstanceOf(String arg, InstanceOfExpr instanceOfExpr, ConditionalExpression condition){
        if (arg != null && instanceOfExpr != null && condition != null) {

            if (instanceOfExpr.getOp().getType() instanceof RefType) {
                RefType refType = (RefType)instanceOfExpr.getOp().getType();
                SootClass sootClass = refType.getSootClass();

                List<SootClass> posibleImp;

                if(sootClass.isInterface()){
                    posibleImp = sootClassLoader.getSubClassForInterface(sootClass);
                }else {
                    posibleImp = sootClassLoader.getSubClass(sootClass);
                }

                if(!condition.getArgTypeMap().containsKey(arg)){
                    condition.getArgTypeMap().put(arg, new ArrayList<>());
                }

                for(SootClass sc : posibleImp){
                    condition.getArgTypeMap().get(arg).add(sc.getType());
                }
            }
        }
    }

    /**
     * Vy argument, it stores its data type in a data structure {@link ConditionalExpression}
     * @param arg argument
     * @param condition condition where the argument data types will be stored
     * */
    private static void fillArgTypeMap(Value arg, ConditionalExpression condition){

        if(arg instanceof JimpleLocal){
            if(!condition.getArgTypeMap().containsKey(((JimpleLocal) arg).getName())){
                condition.getArgTypeMap().put(((JimpleLocal) arg).getName(), new ArrayList<>());
            }

            if(!condition.getArgTypeMap().get(((JimpleLocal) arg).getName()).contains(arg.getType())){
                condition.getArgTypeMap().get(((JimpleLocal) arg).getName()).add(arg.getType());
            }
        }
    }

    /**
     * it replaces the float and long numbers with a classical number entry
     * @param value hodnota, která má být nahrazena
     * @return classic notation of numbers
     * */
    private String opToString(Value value){
        String regex = "(-?\\d+L)|(-?\\d+.\\d+F)";

        //is long or float number
        if(value.toString().matches(regex)){
            return value.toString().replace("L", "").replace("F", "");
        }

        return value.toString();
    }


}
