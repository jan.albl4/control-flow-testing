package cz.zcu.kiv.cft.output.data;


import soot.Unit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class SwitchConditions extends ConditionElement {

    private List<Condition> branches = new ArrayList();

    @XmlElement
    public List<Condition> getBranches() {
        return branches;
    }

    public void setBranches(List<Condition> branches) {
        this.branches = branches;
    }

    @XmlTransient
    public Unit getUnit() {
        return this.getUnit();
    }

    public void setUnit(Unit unit) {
        this.setUnit(unit);
    }
}
