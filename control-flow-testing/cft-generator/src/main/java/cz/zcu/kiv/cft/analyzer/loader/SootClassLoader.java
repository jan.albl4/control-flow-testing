package cz.zcu.kiv.cft.analyzer.loader;

import soot.SootClass;
import soot.SootMethod;
import soot.Type;

import java.util.List;

/**
 * Interface that is used to load Soot classes and methods
 *
 * @author Jan Albl
 */
public interface SootClassLoader {

    /**
     * By full class name returns SootClass
     * @param name class name
     * @return soot class representation
     * */
    SootClass getSootClass(String name);

    /**
     * Returns sootMethod according to input parameters. If the method could not be loaded it throws an exception
     * @param clazz class name
     * @param method method name
     * @return soot method representation
     * */
    SootMethod getSootMethod(String clazz, String method);

    /**
     * Returns sootMethod according to input parameters. If the method could not be loaded it throws an exception
     * @param clazz class name
     * @param method method name
     * @param parameterType data types of input parameters that specify the method to be load
     * @return soot method representation
     * */
    SootMethod getSootMethod(String clazz, String method, List<String> parameterType);

    /**
     * Returns sootMethod according to input parameters. If the method could not be loaded it throws an exception
     * @param clazz class name
     * @param method method name
     * @param returnType  return type of the analyzed method used to determine the method to be analyzed
     * @return soot method representation
     * */
    SootMethod getSootMethod(String clazz, String method, String returnType);

    /**
     * Returns sootMethod according to input parameters. If the method could not be loaded it throws an exception
     * @param clazz class name
     * @param method method name
     * @param parameterType data types of input parameters that specify the method to be load
     * @param returnType  return type of the analyzed method used to determine the method to be analyzed
     * @return soot method representation
     * */
    SootMethod getSootMethod(String clazz, String method, List<String> parameterType, String returnType);

    /**
     * Returns possible subclass of the object by input class
     * @param sootClass parent
     * @return possible subclass
     * */
    List<SootClass> getSubClass(SootClass sootClass);

    /**
     * Returns possible subclass of the object by input type
     * @param type data type
     * @return possible subclass
     * */
    List<SootClass> getSubClass(Type type);

    /**
     * Returns possible subclass of the object by input interface
     * @param sootClass interface
     * @return possible subclass
     * */
    List<SootClass> getSubClassForInterface(SootClass sootClass);

    /**
     * Returns possible subclass of the object by input type
     * @param type parent or interface
     * @return possible subclass
     * */
    List<SootClass> getSubClassForInterface(Type type);
}
