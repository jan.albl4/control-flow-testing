package cz.zcu.kiv.cft.test.generator;

import java.util.List;

/**
 *
 * Class carrying information about the generated parameter
 * @author Jan Albl
 */
public abstract class GeneratedParameter {

    /** name */
    private String name;

    private List<String> body;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getBody() {
        return body;
    }

    public void setBody(List<String> body) {
        this.body = body;
    }
}
