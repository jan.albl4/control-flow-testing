package cz.zcu.kiv.cft.output;

import cz.zcu.kiv.cft.analyzer.loader.SootClassLoader;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.*;
import soot.*;

import java.util.*;

/**
 *
 * Class to initialize output after analysis.
 *
 * @author Jan Albl
 */
public class OutputInitializerImpl implements OutputInitializer{

    /** {@link SootClassLoader} */
    private SootClassLoader sootClassLoader;

    /**
     * Constructor to create an instance of this class.
     * @param sootClassLoader class loader
     * **/
    public OutputInitializerImpl(SootClassLoader sootClassLoader){
        this.sootClassLoader = sootClassLoader;
    }

    @Override
    public ControlFlowAnalysisOutput initFromSootMethod(SootMethod sootMethod){
        if(sootMethod != null && !sootMethod.isPhantom() && !sootMethod.isNative() && sootMethod.retrieveActiveBody() != null) {
            ControlFlowAnalysisOutput cfo = new ControlFlowAnalysisOutput();

            if(sootMethod.getDeclaringClass() != null){

                Clazz clazz = initClassFrom(sootMethod.getDeclaringClass());

                Method method = initMethodFrom(sootMethod);

                clazz.getMethods().add(method);

                cfo.getClasses().add(clazz);
            }

            return cfo;
        } else {
            return null;
        }
    }

    private void initEnumData(EnumParameter enumParameter, Type type){
        if (type instanceof RefType) {
            RefType refType = (RefType)type;

            if (refType.getSootClass() != null) {
                SootClass sootClass = refType.getSootClass();

                for(SootField sf : sootClass.getFields()){
                    if(!sf.getName().equals("$VALUES")){
                        String name = sootClass.getName()+"."+sf.getName();
                        enumParameter.getPossibleEnums().add(name);
                        break;
                    }
                }
            }
        }
    }

    private void initObjectData(ObjectParameter objectParameter, Type type){
        if (type instanceof RefType) {
            RefType refType = (RefType)type;

            if (refType.getSootClass() != null) {

                if(refType.getSootClass().isAbstract()){
                    objectParameter.setAbstract(true);
                }

                if(refType.getSootClass().isInterface()){
                    objectParameter.setInterface(true);
                    initDataTypeForInterface(objectParameter, type);
                }
            }
        }
    }

    private void initDataTypeForInterface(ObjectParameter objectParameter, Type type){
        if (type instanceof RefType) {

            List<SootClass> subClass  = sootClassLoader.getSubClassForInterface(type);

            for(SootClass s : subClass){
                if(!s.isInterface() && !s.isAbstract()){
                    objectParameter.getPossibleObjectTypes().add(s.getName());
                }
            }
        }
    }

    private Clazz initClassFrom(SootClass sootClass){
        if(sootClass != null){
            Clazz clazz = new Clazz();

            clazz.setMethods(new ArrayList<Method>());
            clazz.setClassName(sootClass.getShortName());
            clazz.setPackageName(sootClass.getPackageName());

            boolean hasPublicConstucor = hasPublicConstucor(sootClass);

            clazz.setHasPublicConstructor(hasPublicConstucor);

            return clazz;
        }

        return null;
    }

    private boolean hasPublicConstucor(SootClass sootClass){
        if(sootClass != null){
            boolean hasConstructor = false;
            boolean hasPublicConstucor = true;

            for(SootMethod sootMethod : sootClass.getMethods()){

                if(sootMethod.isConstructor()){
                    hasConstructor = true;

                    if(sootMethod.isPrivate()){
                        hasPublicConstucor = false;
                    }else{
                        hasPublicConstucor = true;
                        break;
                    }
                }
            }

            if (hasConstructor && !hasPublicConstucor) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }


    private Method initMethodFrom(SootMethod sootMethod){
        if(sootMethod != null){

            Method method = new Method();

            if(!sootMethod.isConstructor()){
                method.setName(sootMethod.getName());
                method.setConstructor(false);
            }else {
                method.setName(sootMethod.getDeclaringClass().getShortName());
                method.setConstructor(true);
            }

            method.setParameterCount(sootMethod.getParameterCount());
            method.setStatic(sootMethod.isStatic());
            method.setReturnType(sootMethod.getReturnType().toString());
            method.setPublic(sootMethod.isPublic());

            addParametrForMethod(sootMethod, method);

            return method;

        }
        return null;
    }

    public ControlFlowAnalysisOutput initFromSootClass(SootClass sootClass) {
        if(sootClass != null && !sootClass.isPhantom()) {
            ControlFlowAnalysisOutput cfo = new ControlFlowAnalysisOutput();

            Clazz clazz = initClassFrom(sootClass);

            cfo.getClasses().add(clazz);

            return cfo;
        } else {
            return null;
        }
    }

    public Method initFromSootMethod(SootMethod sootMethod, Clazz clazz){
        if(sootMethod != null && !sootMethod.isPhantom() && !sootMethod.isNative() && sootMethod.retrieveActiveBody() != null && sootMethod.getDeclaringClass() != null) {

            Method m = initMethodFrom(sootMethod);

            clazz.getMethods().add(m);

            return m;
        } else {
            return null;
        }
    }

    private void addParametrForMethod(SootMethod sootMethod, Method method){
        Body b = sootMethod.retrieveActiveBody();

        for(int i = 0; i < sootMethod.getParameterCount(); i++){
            Local local = b.getParameterLocal(i);

            Parameter p;

            String type = local.getType().toString();

            if ( type.equals("char")
                    || type.equals("byte")
                    || type.equals("short")
                    || type.equals("int")
                    || type.equals("float")
                    || type.equals("long")
                    || type.equals("double")
                    || type.equals("boolean")
            ){
                p = new PrimitiveParameter();
                p.setPrimitive(true);
            } else if(DataUtil.isEnumType(local.getType())) {
                p = new EnumParameter();
                p.setPrimitive(false);
                initEnumData((EnumParameter) p, local.getType());
                p.setSootType(local.getType());
            } else{
                p = new ObjectParameter();
                p.setPrimitive(false);
                initObjectData((ObjectParameter) p, local.getType());
                p.setSootType(local.getType());
            }

            p.setName(local.getName());
            p.setSootType(local.getType());
            p.setIndex(i);

            method.getMethodParameter().add(p);
        }
    }
}
