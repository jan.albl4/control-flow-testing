package cz.zcu.kiv.cft.output.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import soot.Unit;

import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Class representing the basic structure for branching in a program
 *
 * @author Jan Albl
 */
public class ConditionElement {

    /** Soot branching expression */
    @JsonIgnore
    private Unit unit;

    /** Parent branch */
    @JsonIgnore
    private ConditionElement parentCondition = null;

    @XmlTransient
    public ConditionElement getParentCondition() {
        return parentCondition;
    }

    public void setParentCondition(ConditionElement parentCondition) {
        this.parentCondition = parentCondition;
    }

    @XmlTransient
    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit u) {
        this.unit = u;
    }
}
