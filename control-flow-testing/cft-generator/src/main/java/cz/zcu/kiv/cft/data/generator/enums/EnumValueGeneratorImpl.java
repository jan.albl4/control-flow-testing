package cz.zcu.kiv.cft.data.generator.enums;

import cz.zcu.kiv.cft.data.generator.data.EnumVariable;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.tools.log.Logger;
import soot.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class EnumValueGeneratorImpl implements EnumValueGenerator {

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    public void evalInstanceOf(ConditionalExpression c, Map<String, EnumVariable> objectDataType){

        if(DataUtil.isEnumConditon(c)){
            Map<String, List<Type>> atm = c.getArgTypeMap();

            if(atm.size() == 1){

                if(c.getExpression().contains(" == ") || c.getExpression().contains(" != ") ){

                    String[] eq = c.getExpression().split(" == ");
                    String[] notEq = c.getExpression().split(" != ");

                    if (eq.length == 2 || notEq.length == 2) {

                        String[] arr;

                        if(eq.length == 2){
                            arr = eq;
                        } else {
                            arr = notEq;
                        }

                        String field = "";
                        String enums = "";

                        for (String arg: arr) {
                            if(c.getArgTypeMap().containsKey(arg)){
                                field = arg.trim();

                                String[] type = arg.trim().split("\\.");

                                int i = 0;
                                for(Type t : c.getArgTypeMap().get(arg)){
                                    if (!objectDataType.containsKey(field)) {
                                        objectDataType.put(field, new EnumVariable());
                                        objectDataType.get(field).setName(type[i]);

                                        List<Type> types = c.getArgTypeMap().get(field);
                                        objectDataType.get(field).setType(types.get(types.size()-1).toString());
                                        objectDataType.get(field).setParrentType(types);
                                    }
                                    i++;
                                }
                            }else {
                                enums = arg.trim();
                            }
                        }

                        String type = enums.trim();
                        String actualField = "";
                        int lastDot = type.lastIndexOf(".");

                        if (lastDot > 0) {
                            actualField = type.substring(lastDot+1);
                            type = type.substring(0, lastDot);
                        }

                        if (!c.getPossibleDataType().containsKey(field)) {
                            c.getPossibleDataType().put(field, new ArrayList<>());
                        }

                        c.getPossibleDataType().get(field).add(enums);

                        SootClass sootClass = DataUtil.getSootClassFor(type);

                        for (SootField f: sootClass.getFields()) {
                            if(!f.getName().equals(actualField)){
                                c.getPossibleDataType().get(field).add(type + "." + f.getName());
                                break;
                            }
                        }

                        if (!objectDataType.containsKey(field)) {
                            objectDataType.put(field, new EnumVariable());
                            objectDataType.get(field).setName(field);

                            List<Type> types = c.getArgTypeMap().get(field);
                            objectDataType.get(field).setType(types.get(types.size()-1).toString());
                            objectDataType.get(field).setParrentType(types);
                        }

                        objectDataType.get(field).getEnums().addAll(c.getPossibleDataType().get(field));
                    }

                }
            }
        }
    }

    public void randomValuesToEmptySymbols(Method method){

        if (method!= null) {
            for(Parameter p : method.getMethodParameter()) {
                if(p instanceof ObjectParameter){
                    if(((ObjectParameter) p).getPossibleObjectTypes().size() == 0
                            && p.getSootType() != null
                            && DataUtil.isEnumType(p.getSootType())) {

                        SootClass sootClass = null;

                        if(p.getSootType() instanceof RefType){
                            RefType refType = (RefType) p.getSootType();
                            sootClass = refType.getSootClass();
                        }else if (p.getSootType() instanceof ArrayType){
                            ArrayType arrayType = (ArrayType) p.getSootType();

                            if(arrayType.getElementType() instanceof RefType){
                                RefType refType = (RefType) arrayType.getElementType();
                                sootClass = refType.getSootClass();
                            }
                        }

                        if(sootClass != null && (sootClass.isInterface() || sootClass.isAbstract())){
                            List<SootClass> sootClasses = DataUtil.getSubClass(p.getSootType());

                            for(SootClass sc : sootClasses){
                                if(!sootClass.isInterface() && !sootClass.isAbstract()){
                                    ((ObjectParameter) p).getPossibleObjectTypes().add(sc.getType().toString());
                                    break;
                                }
                            }

                        } else if(sootClass != null) {
                            ((ObjectParameter) p).getPossibleObjectTypes().add(sootClass.getType().toString());
                        }
                    }
                }
            }
        }
    }

    public void fillEnumParameter(Method method, Map<String, EnumVariable> objectDataType){
        for (Map.Entry<String, EnumVariable> entry : objectDataType.entrySet()) {

            for(Parameter p : method.getMethodParameter()){

                String[] objectAndField = entry.getKey().split("\\.");

                if (p instanceof EnumParameter) {
                    EnumParameter ep = (EnumParameter) p;
                    ep.getPossibleEnums().addAll(entry.getValue().getEnums());
                } else if(p instanceof ObjectParameter) {

                    ObjectParameter actual = (ObjectParameter) p;

                    for(int i = 1; i<objectAndField.length; i++){

                        if(DataUtil.isEnumType(entry.getValue().getParrentType().get(i))){

                            boolean canAdd = true;

                            for(PrimitiveParameter pp : actual.getPrimitiveFields()){
                                if(pp.getName().equals(objectAndField[i])){
                                    canAdd = false;
                                }
                            }

                            if(canAdd){

                                //fillPrimitiveParameter(pp, entry.getValue().getRanges());
                                Type type = entry.getValue().getParrentType().get(i);

                                EnumParameter ep = new EnumParameter();
                                ep.setName(objectAndField[i]);
                                ep.setSootType(type);
                                ep.setPrimitive(false);

                                actual.getEnumParameters().add(ep);

                                ep.getPossibleEnums().addAll(entry.getValue().getEnums());

                                break;
                            }

                        }else {

                            //neni primitivni
                            boolean canAdd = true;

                            for(ObjectParameter op : actual.getObjects()){
                                if(op.getName().equals(objectAndField[i])){
                                    actual = op;
                                    canAdd = false;
                                    break;
                                }
                            }

                            if(canAdd){
                                String type = entry.getValue().getParrentType().get(i).toString();

                                ObjectParameter op = new ObjectParameter();
                                op.setName(objectAndField[i]);
                                op.getPossibleObjectTypes().add(type);
                                op.setPrimitive(false);

                                actual.getObjects().add(op);
                                actual = op;
                            }
                        }
                    }
                }
            }
        }
    }
}

