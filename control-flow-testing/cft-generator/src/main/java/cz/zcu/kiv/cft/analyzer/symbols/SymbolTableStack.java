package cz.zcu.kiv.cft.analyzer.symbols;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Stack of symbol tables. Each table in the stack corresponds to one method
 *
 * @author Jan Albl
 */
public class SymbolTableStack {

    /** Stack of symbol tables. */
    private List<SymbolTable> symbolTableStack = new ArrayList<SymbolTable>();

    /**
     * Removes the first element from the stack
     * @return removed element
     * */
    public SymbolTable popRm(){
        if(this.symbolTableStack.size() > 0){
            SymbolTable symbolTableItem = this.symbolTableStack.get(this.symbolTableStack.size()-1);
            this.symbolTableStack.remove(this.symbolTableStack.size()-1);
            return symbolTableItem;
        }
        return null;
    }

    /**
     * Returns an element that is on top of the stack
     * @return  element that is on top of the stack
     * */
    public SymbolTable pop(){
        return this.symbolTableStack.get(this.symbolTableStack.size()-1);
    }

    /**
     * Inserts an element on the stack top.
     * @param stackItem element to be inserted into the stack
     * */
    public void push(SymbolTable stackItem){
        this.symbolTableStack.add(stackItem);
    }


    /**
     * Copies the current symbol table stack
     * @retrun copy the current symbol table stack
     * */
    public SymbolTableStack clone(){

        SymbolTableStack symbolTableStack = new SymbolTableStack();

        for(SymbolTable v : this.symbolTableStack){
            SymbolTable vt = new SymbolTable();

            for(SymbolItem vti : v.getSymbols()){
                SymbolItem clon = new SymbolItem();

                clon.setType(vti.getType());
                clon.setRightOp(vti.getRightOp());
                clon.setName(vti.getName());
                clon.setParam(vti.getParam());
                clon.setSootValue(vti.getSootValue());
                clon.setParentMethod(vti.getParentMethod());
                clon.setParameterRef(vti.getParameterRef());
                clon.setHistory(vti.getHistory());

                vt.addSymbolItem(clon);
            }

            symbolTableStack.push(vt);
        }

        return symbolTableStack;
    }

    /**
     * Replaces the element at the top of the stack with the input element
     * @param symbolTable input element
     * */
    public void replaceTop(SymbolTable symbolTable){
        this.popRm();
        this.push(symbolTable);
    }


    public List<SymbolTable> getSymbolTableStack(){
        return symbolTableStack;
    }
}
