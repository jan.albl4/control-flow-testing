package cz.zcu.kiv.cft.test.generator;

/**
 * Class carrying information about the generated test class annotation
 *
 * @author Jan Albl
 */
public class Annotation {

    /** Value */
    private String annotation;

    public Annotation(String annotation){
        this.annotation = annotation;
    }

    @Override
    public String toString() {
        return annotation + "\n";
    }
}
