package cz.zcu.kiv.cft.output;

import cz.zcu.kiv.cft.output.data.Clazz;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.output.data.Method;
import soot.SootClass;
import soot.SootMethod;

/**
 *
 * Interface for initialize output after analysis.
 *
 * @author Jan Albl
 */
public interface OutputInitializer {

    /**
     * According to the input method, it creates a basic output structure in which there is information about this method
     * @param sootMethod method
     * @return {@link ControlFlowAnalysisOutput}
     * */
    ControlFlowAnalysisOutput initFromSootMethod(SootMethod sootMethod);

    /**
     * According to the input method, it creates a basic output structure in which there is information about this method
     * @param sootMethod method
     * @param clazz method
     * @return {@link Method}
     * */
    Method initFromSootMethod(SootMethod sootMethod, Clazz clazz);

    ControlFlowAnalysisOutput initFromSootClass(SootClass sootClass);
}
