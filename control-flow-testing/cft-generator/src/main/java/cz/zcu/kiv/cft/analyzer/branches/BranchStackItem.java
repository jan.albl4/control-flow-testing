package cz.zcu.kiv.cft.analyzer.branches;

import soot.Unit;

/**
 *
 * Class holding a branch node
 *
 * @author Jan Albl
 */
public class BranchStackItem extends StackItem {

    /**
     * Constructor to create an instance of this class.
     *
     * @param u the branch entry unit contains a condition
     * @param endNode end branch node
     * **/
    public BranchStackItem(Unit u, Unit endNode){
        this.setUnit(u);
        this.setUnprocessedBranchesCount(1);
        this.setEndNode(endNode);
    }
}
