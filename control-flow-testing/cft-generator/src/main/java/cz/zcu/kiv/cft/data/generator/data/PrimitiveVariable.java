package cz.zcu.kiv.cft.data.generator.data;

import org.apache.commons.lang.math.Range;
import soot.Type;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class PrimitiveVariable extends BaseVariable{

    /**  */
    private List<Double> borderValue;

    /**  */
    private List<Range> ranges;

    /**  */
    private List<String> parrentType;

    public PrimitiveVariable(){
        this.borderValue = new ArrayList<>();
        this.ranges = new ArrayList<>();
        this.parrentType = new ArrayList<>();
    }

    public List<Double> getBorderValue() {
        return borderValue;
    }

    public void setBorderValue(List<Double> borderValue) {
        this.borderValue = borderValue;
    }

    public List<Range> getRanges() {
        return ranges;
    }

    public void setRanges(List<Range> ranges) {
        this.ranges = ranges;
    }

    public List<String> getParrentType() {
        return parrentType;
    }

    public void setParrentType(List<String> parrentType) {
        this.parrentType = parrentType;
    }
}
