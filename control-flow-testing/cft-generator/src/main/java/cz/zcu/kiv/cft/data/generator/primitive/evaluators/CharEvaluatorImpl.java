package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.IntRange;
import org.apache.commons.lang.math.Range;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class CharEvaluatorImpl implements CharEvaluator {

    @Override
    public void addCharValuesToParameter(PrimitiveParameter primitiveParameter, Range range){
        if(primitiveParameter != null && range != null) {

            List<String> values = getValueFromRange(range);

            for(String v: values){
                primitiveParameter.getValues().add(v);
            }
        }
    }

    @Override
    public List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter){
        List<Range> ranges = new ArrayList<>();

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new IntRange((int)Character.MIN_VALUE, primitiveParameter.getBorderValue().get(0)));
        }

        for (int i = 0; i < primitiveParameter.getBorderValue().size()-1; i++) {
            ranges.add(new IntRange(primitiveParameter.getBorderValue().get(i), primitiveParameter.getBorderValue().get(1+i)));
        }

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new IntRange(primitiveParameter.getBorderValue().get(primitiveParameter.getBorderValue().size()-1), (int)Character.MAX_VALUE));
        }

        return ranges;
    }

    private List<String> getValueFromRange(Range range){
        List<String> values = new ArrayList<>();

        if(range.getMinimumInteger() >=  Character.MIN_VALUE){
            values.add(String.valueOf(range.getMinimumInteger()));
        }

        if(range.getMaximumInteger() <= Character.MAX_VALUE){
            values.add(String.valueOf(range.getMaximumInteger()));
        }

        if(range.getMaximumInteger() <= Character.MAX_VALUE
                && range.getMinimumInteger() >=  Character.MIN_VALUE
                && range.getMinimumInteger()+1 < range.getMaximumInteger()){

            int intermediateValue = 0;

            if(range.getMinimumInteger()+1 == range.getMaximumInteger()-1){
                intermediateValue = range.getMinimumInteger()+1;
            } else {
                intermediateValue  = DataUtil.getRandomNumberInRange(range.getMinimumInteger()+1, range.getMaximumInteger()-1);
            }

            values.add(String.valueOf(intermediateValue));
        }

        return values;
    }

    @Override
    public String getRandomValue(){
        return String.valueOf(DataUtil.getRandomNumberInRange(Character.MIN_VALUE, Character.MAX_VALUE));
    }
}
