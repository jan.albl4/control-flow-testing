package cz.zcu.kiv.cft.output.data;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data class representing branching in a program.
 *
 * @author Jan Albl
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Condition extends ConditionElement {

    /** List of truth branches that are reached if the condition is true */
    @XmlElementWrapper(name = "trueBranches")
    @XmlElement(name = "trueBranch")
    private List<ConditionElement> trueBranch = new ArrayList();

    /** List of false branches that are reached if the condition is true */
    @XmlElementWrapper(name = "falseBranches")
    @XmlElement(name = "falseBranch")
    private List<ConditionElement> falseBranch = new ArrayList();

    /** List of possible expressions conditions */
    private List<ConditionalExpression> conditionalExpressions = new ArrayList<>();

    public List<ConditionElement> getTrueBranch() {
        return trueBranch;
    }

    public void setTrueBranch(List<ConditionElement> trueBranch) {
        this.trueBranch = trueBranch;
    }

    public List<ConditionElement> getFalseBranch() {
        return falseBranch;
    }

    public void setFalseBranch(List<ConditionElement> falseBranch) {
        this.falseBranch = falseBranch;
    }

    @Override
    public String toString() {
        String out = "";
        boolean first = true;

        for(ConditionalExpression c: this.conditionalExpressions){
            if(!first) {
                out += " || " + c.getExpression();
            } else {
                out += c.getExpression();
                first = false;
            }
        }

        return out;
    }

    public List<ConditionalExpression> getConditionalExpressions() {
        return conditionalExpressions;
    }

    public void setConditionalExpressions(List<ConditionalExpression> conditionalExpressions) {
        this.conditionalExpressions = conditionalExpressions;
    }

    public void addConditionalExpression(ConditionalExpression expression){
        boolean canAdd = true;
        for(ConditionalExpression ce : conditionalExpressions){
            if(ce.getExpression().equals(expression.getExpression())){
                canAdd = false;
            }
        }

        if(canAdd){
            conditionalExpressions.add(expression);
        }
    }
}
