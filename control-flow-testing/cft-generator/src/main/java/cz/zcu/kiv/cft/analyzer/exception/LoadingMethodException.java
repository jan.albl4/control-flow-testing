package cz.zcu.kiv.cft.analyzer.exception;

/**
 *
 * Exception that is thrown when a problem occurs when loading a test method
 *
 * @author Jan Albl
 */
public class LoadingMethodException extends RuntimeException {

    /**
     * Constructor to create an instance of this class.
     *
     * @param message message exceptions
     * */
    private LoadingMethodException(String message){
        super(message);
    }

    /**
     * Constructor to create an instance of this class.
     *
     * @param className name of analyzed class
     * @param methodName name of analyzed method
     * */
    public LoadingMethodException(String className, String methodName){
        this("Method "+methodName+" of class "+className+" not found or unable to load. If this method is overloaded in the class try to specify parameters and return type.");
    }
}
