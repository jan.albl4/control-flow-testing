package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.tools.log.Logger;
import soot.Unit;
import soot.toolkits.graph.MHGPostDominatorsFinder;
import soot.toolkits.graph.UnitGraph;

/**
 *
 * This class analyzes the immediate postdominators in control flow graph
 *
 * @author Jan Albl
 */
public class AnalyzerImmediatePostDominator {

    /** Logger {@link Logger} */
    private static final Logger logger = Logger.getLogger();

    /**
     * Analyzes the immediate postdominators in control flow graph
     *
     * @param graph UnitGraph
     * @param u the node for which to find the immediate postdominator
     * @return the immediate postdominator of  input node
     */
    public static Unit getImmediatePostDominatorFor(UnitGraph graph, Unit u){

        MHGPostDominatorsFinder postDominatorsFinder = new MHGPostDominatorsFinder(graph);
        try {
            return (Unit)postDominatorsFinder.getImmediateDominator(u);
        } catch (AssertionError e){
            logger.warn("Can not find immediate post dominator for this node " + u.toString());
            //https://github.com/Sable/soot/blob/4a04145d1a4d2713ef2733e33abd5ba77b2fd848/src/main/java/soot/toolkits/graph/MHGDominatorsFinder.java
        }

        return null;
    }
}
