package cz.zcu.kiv.cft.analyzer.branches;

import cz.zcu.kiv.cft.analyzer.evaluator.ConditionEvaluatorImpl;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoaderImpl;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolTableStack;
import soot.Unit;
import soot.toolkits.graph.UnitGraph;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for a branch stack that holds the currently found and unprocessed branches
 *
 * @author Jan Albl
 */
public class BranchStack {

    /** All stack items */
    private List<StackItem> stackItems = new ArrayList();

    /** Current parental condition */
    private ConditionElement currentCondition;

    /** The method where the output from the current branch analysis is saved */
    private Method method;

    /** Stack of symbol tables */
    private SymbolTableStack symbolTableStack;

    /**
     * Adds a newly found branching condition to the parent branch
     * @param conditionElement branching condition
     * */
    private void addConditionElement(ConditionElement conditionElement) {
        if (currentCondition instanceof Condition) {
            Condition act = (Condition) currentCondition;
            if(pop() instanceof BranchStackItem && pop().getUnprocessedBranchesCount() == 1){
                act.getFalseBranch().add(conditionElement);
            }else if(pop() instanceof BranchStackItem){
                act.getTrueBranch().add(conditionElement);
            }else if(pop() instanceof SwitchStackItem){
                act.getTrueBranch().add(conditionElement);
            }
        }
    }

    /**
     * Removes the first element from the stack
     * @return removed element
     * */
    private StackItem popRm() {
        if (this.stackItems.size() > 0) {

            if (currentCondition != null && currentCondition.getParentCondition() != null) {
                currentCondition = currentCondition.getParentCondition();
            } else {
                currentCondition = null;
            }

            StackItem branchStackItem = this.stackItems.get(this.stackItems.size()-1);
            this.stackItems.remove(this.stackItems.size()-1);
            return branchStackItem;
        }
        return null;
    }

    /**
     * Returns an element that is on top of the stack
     * @return  element that is on top of the stack
     * */
    public StackItem pop() {
        return this.stackItems.get(this.stackItems.size()-1);
    }

    /**
     * Returns a node that is in the first order for further processing
     * @param unitGraph control flow graph
     * @return node that is in the first order for further processing
     * */
    public Unit getNextUnprocessedNode(UnitGraph unitGraph){

        Unit u = this.pop().getUnit();
        Unit node = null;

        if(this.pop() instanceof BranchStackItem){
            if(this.pop().getUnprocessedBranchesCount() == 1){
                node = unitGraph.getSuccsOf(u).get(0);
            }else{
                node = unitGraph.getSuccsOf(u).get(1);
            }
        } else if (this.pop() instanceof SwitchStackItem){
            List<Unit> neighbors = unitGraph.getSuccsOf(u);
            node = ((SwitchStackItem)this.pop()).getAnotherUnprocessedNode(unitGraph);
        }

        return node;
    }

    /**
     * Marks one branch branch as processed
     * */
    public void markProcessedNode() {
        if(this.size() != 0){

            while(this.size() != 0){
                int unprocessedBranchesCount = this.stackItems.get(this.stackItems.size()-1).getUnprocessedBranchesCount();
                if(unprocessedBranchesCount == 0){
                    this.popRm();
                }else{
                    this.stackItems.get(this.stackItems.size()-1).setUnprocessedBranchesCount(unprocessedBranchesCount-1);
                    break;
                }
            }
        }
    }

    /**
     * Inserts an element on the stack top. If the flag is set, it evaluates its branching condition
     *
     * @param stackItem element to be inserted into the stack
     * @param evaluate condition should be evaluated
     * */
    public void push(StackItem stackItem, boolean evaluate) {

        if (evaluate) {
            ConditionEvaluatorImpl.setSootClassLoader(SootClassLoaderImpl.getInstance());

            if (stackItem instanceof BranchStackItem) {
                Condition pom = new Condition();
                ConditionEvaluatorImpl.evaluate(stackItem, pom);

                if (currentCondition != null) {
                    pom.setParentCondition(currentCondition);

                    addConditionElement(pom);

                    currentCondition = pom;

                } else {
                    currentCondition = pom;
                    this.method.getConditions().add(currentCondition);
                }
            } else if(stackItem instanceof SwitchStackItem) {
                SwitchConditions pom = new SwitchConditions();

                ConditionEvaluatorImpl.evaluate(stackItem, pom);

                if (currentCondition != null) {
                    pom.setParentCondition(currentCondition);

                    addConditionElement(pom);

                    currentCondition = pom.getBranches().get(0);

                } else {
                    currentCondition = pom.getBranches().get(0);
                    this.method.getConditions().add(pom);
                }
            }
        }

        this.stackItems.add(stackItem);
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public int size(){
        return stackItems.size();
    }

    public void setSymbolTableStack(SymbolTableStack symbolTableStack) {
        this.symbolTableStack = symbolTableStack;
    }

}
