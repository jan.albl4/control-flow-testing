package cz.zcu.kiv.cft.data.generator.object;

import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.output.data.Method;
import cz.zcu.kiv.cft.data.generator.data.ObjectVariable;

import java.util.Map;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public interface ObjectGenerator {

    void evalCondition(ConditionalExpression c, Map<String, ObjectVariable> objectDataType);

    void randomValuesToEmptySymbols(Method method);

    void fillObjectParameter(Method method, Map<String, ObjectVariable> objectDataType);

}
