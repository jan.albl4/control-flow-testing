package cz.zcu.kiv.cft.output.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class representing the primitive parameter of the method
 *
 * @author Jan Albl
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PrimitiveParameter extends Parameter {

    /** Possible values */
    @XmlElementWrapper(name = "values")
    @XmlElement(name = "value")
    private Set<String> values = new HashSet<>();

    public Set<String> getValues() {
        return values;
    }

    public void setValues(Set<String> values) {
        this.values = values;
    }
}
