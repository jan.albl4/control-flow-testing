package cz.zcu.kiv.cft.analyzer.symbols;

import cz.zcu.kiv.cft.tools.Utils;
import soot.*;
import soot.jimple.*;
import soot.jimple.internal.*;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Represents the symbol table created from the control flow graph
 *
 * @author Jan Albl
 */
public class SymbolTable {

    /** Reference to the input values of the parent method */
    private List<Value> lastParam = new ArrayList<Value>();

    /** Items in the symbol table */
    private List<SymbolItem> symbolItems = new ArrayList<SymbolItem>();

    /**
     * Inserts an item into a symbol table if there is an added value
     * @param symbolItem symbol item
     * @param symbolTableStack sstack of symbol tables
     * */
    public void addSymbolItem(SymbolItem symbolItem, SymbolTableStack symbolTableStack){
        boolean canAdd = true;

        Value classValue = null;

        if (symbolItem.getSootValue() instanceof JInstanceFieldRef) {
            classValue = ((JInstanceFieldRef) symbolItem.getSootValue()).getBase();
        }

        if (symbolItem.getSootValue() instanceof VirtualInvokeExpr) {
            classValue = ((VirtualInvokeExpr) symbolItem.getSootValue()).getBase();
        }

        for (int i = 0; i < symbolItems.size(); i++) {
            if(symbolItems.get(i).getSootValue().equals(symbolItem.getSootValue())) {
                symbolItems.get(i).setRightOp(symbolItem.getRightOp());
                setSymbolHistory(symbolItems.get(i), symbolTableStack);
                canAdd = false;
                break;
            }

            if (classValue != null && symbolItems.get(i).getSootValue().equals(classValue)) {
                setSymbolHistory(symbolItem, symbolTableStack);
                canAdd = false;
                break;
            }
        }

        if(canAdd){
            symbolItems.add(symbolItem);
            setSymbolHistory(symbolItem, symbolTableStack);
        }

    }

    /**
     * Inserts an item into a symbol table
     * @param symbolItem symbol item
     * @param symbolTableStack sstack of symbol tables
     * */
    private void setSymbolHistory(SymbolItem symbolItem, SymbolTableStack symbolTableStack){

        HistorySymbolItem valueObject = new HistorySymbolItem();
        valueObject.setSootValue(symbolItem.getRightOp());

        HistorySymbolItem actual = symbolItem.getHistory();

        if(actual == null){
            symbolItem.setHistory(valueObject);
        }else {
            valueObject.setNext(symbolItem.getHistory());
            symbolItem.getHistory().setPrevious(valueObject);
            symbolItem.setHistory(valueObject);
        }

        evalueSymbolHistory(symbolItem, symbolTableStack);
    }

    /**
     * Inserts a symbol into a table
     * @param symbolItem inserted item
     * */
    public void addSymbolItem(SymbolItem symbolItem){
        symbolItems.add(symbolItem);
    }

    /**
     * Connects the symbol item operators to other symbol histories of the table
     * @param symbolItem symbol item
     * @param symbolTableStack sstack of symbol tables
     * */
    private void evalueSymbolHistory(SymbolItem symbolItem, SymbolTableStack symbolTableStack){
        HistorySymbolItem valueObject = symbolItem.getHistory();

        Value op1 = null;
        Value op2 = null;

        if(valueObject.getSootValue() instanceof JAddExpr){
            JAddExpr jAddExpr = (JAddExpr)valueObject.getSootValue();
            op1 = jAddExpr.getOp1();
            op2 = jAddExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof JCmpgExpr){
            JCmpgExpr jCmpgExpr = (JCmpgExpr)valueObject.getSootValue();
            op1 = jCmpgExpr.getOp1();
            op2 = jCmpgExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof JCmplExpr){
            JCmplExpr jCmplExpr = (JCmplExpr)valueObject.getSootValue();
            op1 = jCmplExpr.getOp1();
            op2 = jCmplExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof JCmpExpr){
            JCmpExpr jCmplExpr = (JCmpExpr)valueObject.getSootValue();
            op1 = jCmplExpr.getOp1();
            op2 = jCmplExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof JDivExpr){
            JDivExpr jDivExpr = (JDivExpr)valueObject.getSootValue();
            op1 = jDivExpr.getOp1();
            op2 = jDivExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof JMulExpr){
            JMulExpr jMulExpr = (JMulExpr)valueObject.getSootValue();
            op1 = jMulExpr.getOp1();
            op2 = jMulExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof JCastExpr){
            JCastExpr jCastExpr = (JCastExpr)valueObject.getSootValue();
            op1 = jCastExpr.getOp();
        }else if(valueObject.getSootValue() instanceof JimpleLocal){
            JimpleLocal jimpleLocal = (JimpleLocal)valueObject.getSootValue();
            op1 = jimpleLocal;
        }else if(valueObject.getSootValue() instanceof JSubExpr){
            JSubExpr jubExpr = (JSubExpr)valueObject.getSootValue();
            op1 = jubExpr.getOp1();
            op2 = jubExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof ParameterRef){
            ParameterRef parameterRef = (ParameterRef)valueObject.getSootValue();
            op1 = parameterRef;
        }else if(valueObject.getSootValue() instanceof InstanceFieldRef){
            InstanceFieldRef instanceFieldRef = (InstanceFieldRef)valueObject.getSootValue();
            op1 = instanceFieldRef;
        }else if(valueObject.getSootValue() instanceof VirtualInvokeExpr){
            VirtualInvokeExpr instanceFieldRef = (VirtualInvokeExpr)valueObject.getSootValue();
            op1 = instanceFieldRef;
            addVirtualInvokeExpr(symbolItem, instanceFieldRef);
        }else if(valueObject.getSootValue() instanceof LengthExpr){
            LengthExpr lengthExpr = (LengthExpr)valueObject.getSootValue();
            op1 = lengthExpr.getOp();
        }else if(valueObject.getSootValue() instanceof InstanceOfExpr){
            InstanceOfExpr instanceOfExpr = (InstanceOfExpr)valueObject.getSootValue();
            op1 = instanceOfExpr.getOp();
        }else if(valueObject.getSootValue() instanceof BinopExpr){ // binary operation
            BinopExpr binopExpr = (BinopExpr)valueObject.getSootValue();
            op1 = binopExpr.getOp1();
            op2 = binopExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof ShlExpr){ // a >> b
            ShlExpr shlExpr = (ShlExpr)valueObject.getSootValue();
            op1 = shlExpr.getOp1();
            op2 = shlExpr.getOp2();
        }else if(valueObject.getSootValue() instanceof UshrExpr){ // a << b
            UshrExpr ushrExpr = (UshrExpr)valueObject.getSootValue();
            op1 = ushrExpr.getOp1();
            op2 = ushrExpr.getOp2();
        } else if(valueObject.getSootValue() instanceof AndExpr){ // a & b
            AndExpr andExpr = (AndExpr)valueObject.getSootValue();
            op1 = andExpr.getOp1();
            op2 = andExpr.getOp2();
        }
        this.addEvalValue(symbolItem, symbolTableStack, op1, op2);
    }

    /**
     * For symbol evaluates method call
     * @param symbolItem symbol item
     * @param virtualInvokeExpr invoke expressino
     * */
    private void addVirtualInvokeExpr(SymbolItem symbolItem, VirtualInvokeExpr virtualInvokeExpr){
        Value base = virtualInvokeExpr.getBase();
        HistorySymbolItem valueObject = symbolItem.getHistory();

        List<Value> params = virtualInvokeExpr.getArgs();

        for(int i = 0; i < symbolItems.size(); i++) {
            if (symbolItems.get(i).getSootValue().equals(base)) {
                valueObject.setCallObject(symbolItems.get(i).getHistory());
            }
        }

        for(Value par : params){
            for(int i = 0; i < symbolItems.size(); i++) {
                if (symbolItems.get(i).getSootValue().equals(par)) {
                    valueObject.getParameters().add(symbolItems.get(i).getHistory());
                }
            }
        }
    }
    /**
     * Connects the symbol item operators to other symbol histories of the table
     * @param op1 left operator
     * @param op2 right operator
     * @param symbolItem symbol item
     * @param symbolTableStack sstack of symbol tables
     * */
    private void addEvalValue(SymbolItem symbolItem, SymbolTableStack symbolTableStack, Value op1, Value op2){

        HistorySymbolItem valueObject = symbolItem.getHistory();
        int stackSize = symbolTableStack.getSymbolTableStack().size();

        List<SymbolItem> symbolItemsParrent = new ArrayList<SymbolItem>();

        if(stackSize > 1){
            symbolItemsParrent = symbolTableStack.getSymbolTableStack().get(stackSize-2).getSymbols();
        }

        //left operator
        if(op1 != null && !(op1 instanceof IntConstant || op1 instanceof DoubleConstant || op1 instanceof FloatConstant || op1 instanceof LongConstant)){
            boolean add = false;

            //field?
            if (op1 instanceof VirtualInvokeExpr && Utils.isGetField((VirtualInvokeExpr)op1)) {
                Value clazz = ((VirtualInvokeExpr) op1).getBase();

                for(int i = 0; i < symbolItems.size(); i++){

                    if(symbolItems.get(i).getSootValue().equals(clazz)) {

                        boolean isInClass = false;

                        for(HistoryClassField f: symbolItems.get(i).getHistory().getFields()){
                            if(f.getFieldRef().equals(Utils.getField((VirtualInvokeExpr)op1))) {
                                isInClass = true;
                                valueObject.setLeftOp(f);
                                break;
                            }
                        }

                        if(!isInClass){

                            HistoryClassField vt = new HistoryClassField();
                            vt.setName((Utils.getField((VirtualInvokeExpr)op1)).getName());
                            vt.setFieldRef((Utils.getField((VirtualInvokeExpr)op1)));
                            vt.setType((Utils.getField((VirtualInvokeExpr)op1)).getType());
                            vt.setSymbolItem((symbolItems.get(i)));

                            //r = r.getFather
                            if(symbolItems.get(i).getSootValue().equals(symbolItem.getSootValue())){
                                vt.setParentHSI(symbolItems.get(i).getHistory().getNext());
                            }else{
                                vt.setParentHSI(symbolItems.get(i).getHistory());
                            }


                            valueObject.setLeftOp(vt);
                            symbolItems.get(i).getHistory().getFields().add(vt);
                        }
                    }
                }


            } else if (op1 instanceof InstanceFieldRef) {
                //field
                Value clazz = ((InstanceFieldRef) op1).getBase();

                for(int i = 0; i < symbolItems.size(); i++){

                    if(symbolItems.get(i).getSootValue().equals(clazz)) {

                        boolean isInClass = false;

                        for(HistoryClassField f: symbolItems.get(i).getHistory().getFields()){
                            if(f.getFieldRef().equals(((InstanceFieldRef) op1).getField())) {
                                isInClass = true;
                                valueObject.setLeftOp(f);
                                break;
                            }
                        }

                        if(!isInClass){
                            HistoryClassField vt = new HistoryClassField();
                            vt.setName(((InstanceFieldRef) op1).getField().getName());
                            vt.setFieldRef(((InstanceFieldRef) op1).getField());
                            vt.setType(((InstanceFieldRef) op1).getField().getType());
                            vt.setParentHSI((symbolItems.get(i).getHistory()));
                            vt.setSymbolItem((symbolItems.get(i)));

                            valueObject.setLeftOp(vt);
                            symbolItems.get(i).getHistory().getFields().add(vt);
                        }
                    }
                }

            } else {
                for(int i = 0; i < symbolItems.size(); i++){
                    if(symbolItems.get(i).getSootValue().equals(op1)) {
                        if(op1.equals(symbolItem.getSootValue())){
                            valueObject.setLeftOp(symbolItems.get(i).getHistory().getNext());
                        }else{
                            valueObject.setLeftOp(symbolItems.get(i).getHistory());
                        }
                        add = true;
                    }
                }
                if(!add){
                    for(int i = 0; i < symbolItemsParrent.size(); i++){
                        if(symbolItemsParrent.get(i).getSootValue().equals(op1)) {
                            if(op1.equals(symbolItem.getSootValue())){
                                valueObject.setLeftOp(symbolTableStack.getSymbolTableStack().get(stackSize-2).getSymbols().get(i).getHistory().getNext());
                            }else{
                                valueObject.setLeftOp(symbolTableStack.getSymbolTableStack().get(stackSize-2).getSymbols().get(i).getHistory());
                            }
                        }
                    }
                }
            }
        }

        //right operator
        if(op2 != null && !(op2 instanceof IntConstant || op2 instanceof DoubleConstant || op2 instanceof FloatConstant || op2 instanceof LongConstant)){
            boolean add = false;
            for(int i = 0; i < symbolItems.size(); i++){
                if(symbolItems.get(i).getSootValue().equals(op2)) {
                    if(op2.equals(symbolItem.getSootValue())){
                        valueObject.setRightOp(symbolItems.get(i).getHistory().getNext());
                        add = true;
                    }else{
                        valueObject.setRightOp(symbolItems.get(i).getHistory());
                        add = true;
                    }
                }
            }

            if(!add){
                for(int i = 0; i < symbolItemsParrent.size(); i++){
                    if(symbolItemsParrent.get(i).getSootValue().equals(op1)) {
                        if(op1.equals(symbolItem.getSootValue())){
                            valueObject.setRightOp(symbolTableStack.getSymbolTableStack().get(stackSize-2).getSymbols().get(i).getHistory().getNext());
                        }else{
                            valueObject.setRightOp(symbolTableStack.getSymbolTableStack().get(stackSize-2).getSymbols().get(i).getHistory());
                        }
                    }
                }
            }
        }
    }

    /**
     * Copies the current symbol table
     * @retrun copy the current symbol table
     * */
    public SymbolTable clone(){
        SymbolTable vt = new SymbolTable();

        for(SymbolItem vti : symbolItems){
            SymbolItem clon = new SymbolItem();

            clon.setType(vti.getType());
            clon.setRightOp(vti.getRightOp());
            clon.setName(vti.getName());
            clon.setParam(vti.getParam());
            clon.setSootValue(vti.getSootValue());
            clon.setParentMethod(vti.getParentMethod());
            clon.setParameterRef(vti.getParameterRef());
            clon.setHistory(vti.getHistory());

            vt.addSymbolItem(clon);
        }

        return vt;
    }

    public List<Value> getLastParam() {
        return lastParam;
    }

    public void setLastParam(List<Value> lastParam) {
        this.lastParam = lastParam;
    }

    public List<SymbolItem> getSymbols(){
        return this.symbolItems;
    }

    public List<SymbolItem> getCurrentSymbolTable(){
        List<SymbolItem> actualVariable = new ArrayList<SymbolItem>();

        for(int i = 0; i < symbolItems.size(); i++){
            actualVariable.add(symbolItems.get(i));
        }

        return actualVariable;
    }
}
