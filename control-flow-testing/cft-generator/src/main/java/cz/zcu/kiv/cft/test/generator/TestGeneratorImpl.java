package cz.zcu.kiv.cft.test.generator;

import com.google.googlejavaformat.java.Formatter;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.tools.log.Logger;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class that generates unit tests from the generated data stored in the {@link ControlFlowAnalysisOutput} class
 *
 * @author Jan Albl
 */
public class TestGeneratorImpl implements TestGenerator {

    /** The approximate maximum number of rows of the generated test class */
    private int maxLine;

    /** Flag whether unit tests will be generated for private methods */
    private boolean isAnalyzePrivateMethod;

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    /**
     * Constructor to create an instance of this class.
     * @param analyzePrivateMethod flag whether unit tests will be generated for private methods
     * **/
    public TestGeneratorImpl(boolean analyzePrivateMethod){
        this.isAnalyzePrivateMethod = analyzePrivateMethod;
        this.maxLine = 10000;
    }

    @Override
    public void generateClassFor(ControlFlowAnalysisOutput cfo, String path) {

        logger.info("generating test classes");

        List<GeneratedClass> gc = this.generateClassFor(cfo);

        if(gc != null && gc.size() > 0){
            createPath(path);

            for(GeneratedClass generatedClass : gc){

                logger.debug("generating \""+generatedClass.getName()+"\" test class");

                String wc = generatedClass.toString();

                try{
                    String formattedSource = new Formatter().formatSource(wc);

                    String s = OSPlatform.getPathSeparator();
                    String classPath = path + s + generatedClass.getClassPackage().replace(".", s);

                    createPath(classPath);

                    PrintWriter out = new PrintWriter(classPath + s + generatedClass.getName()+"Test.java");
                    out.print(formattedSource);
                    out.close();

                }catch (Exception e){
                    logger.error(e.toString());
                }
            }
        }

        logger.info("end of test generation");
    }

    @Override
    public void generateClassFor(ControlFlowAnalysisOutput cfo, String path, int maximumLines) {
        this.maxLine = maximumLines;
        this.generateClassFor(cfo, path);
    }

    /**
     * Generates test classes from data to the list
     * @param cfo {@link ControlFlowAnalysisOutput}
     * @return list of test classes
     * */
    private List<GeneratedClass> generateClassFor(ControlFlowAnalysisOutput cfo) {

        List<GeneratedClass> gc = new ArrayList<>();

        if(cfo != null){

            int clazzCounter = 1;

            for(Clazz clazz : cfo.getClasses()){

                logger.debug("generate a test set for class \"" + clazz.getClassName() + "\"");

                GeneratedClass generatedClass = new GeneratedClass();

                generatedClass.setClassPackage(clazz.getPackageName());
                generatedClass.setName(clazz.getClassName()+clazzCounter);
                generatedClass.getImports().add(clazz.getPackageName()+"."+clazz.getClassName());
                generatedClass.getImports().add("org.junit.Test");

                for (Method method : clazz.getMethods()) {

                    if (testCanBeGenerated(method)) {

                        List<GeneratedParameter> generatedParameters = new ArrayList<>();
                        for(Parameter p: method.getMethodParameter()){

                            if (p instanceof ObjectParameter ){
                                generatedParameters.add(new GeneratedObjectParameter((ObjectParameter)p));
                            }else if (p instanceof EnumParameter ){
                                generatedParameters.add(new GenerateEnumParameter((EnumParameter) p));
                            }else if(p instanceof PrimitiveParameter){
                                generatedParameters.add(new GeneratedPrimitiveParameter((PrimitiveParameter)p));
                            }
                        }

                        List<String> body = createCombinationOfParameters(clazz, method, generatedParameters);

                        int numberLineBody = numberLinesInBody(body);
                        int sumNumberLine = numberLineOnlyClass(generatedClass);
                        int methodCount = 0;

                        for(String b : body){

                            sumNumberLine += numberLineBody;

                            if(sumNumberLine > maxLine){
                                clazzCounter++;

                                GeneratedClass newGeneratedClass = new GeneratedClass();

                                newGeneratedClass.setClassPackage(generatedClass.getClassPackage());
                                newGeneratedClass.setName(clazz.getClassName()+clazzCounter);
                                newGeneratedClass.getImports().addAll(generatedClass.getImports());

                                gc.add(generatedClass);

                                generatedClass = newGeneratedClass;
                                sumNumberLine = numberLineOnlyClass(generatedClass);
                            }

                            generatedClass.getMethods().add(new GeneratedMethod(method.getName(), b, methodCount++));
                        }
                    }
                }

                if(generatedClass.getMethods().size() > 0){
                    gc.add(generatedClass);
                }

                clazzCounter = 1;
            }
        }

        return gc;
    }

    /**
     * Tests whether a unit test can be created for the method
     * @param method method
     * @return true if it can
     * */
    private boolean testCanBeGenerated(Method method){
        if (method != null
                && method.getMethodParameter() != null
                && method.getMethodParameter().size() > 0
                && (method.isPublic() || isAnalyzePrivateMethod)
                && !method.isConstructor()) {
            boolean can = true;

            for(Parameter p : method.getMethodParameter()){
                 if(p.isPrimitive()){
                     if(((PrimitiveParameter)p).getValues() == null || ((PrimitiveParameter)p).getValues().size() == 0) {
                         can = false;
                     }
                 }else if(p instanceof ObjectParameter && (((ObjectParameter)p).getPossibleObjectTypes() == null || ((ObjectParameter)p).getPossibleObjectTypes().size() == 0) ){
                     can = false;
                 }else if(p instanceof EnumParameter && (((EnumParameter)p).getPossibleEnums() == null || ((EnumParameter)p).getPossibleEnums().size() == 0)){
                     can = false;
                 }
            }

            return can;
        } else {
            return false;
        }
    }


    /**
    * Creates a path if it does not exist
    * @param path path
    * */
    private void createPath(String path){

        File dir = new File(path);

        if(!dir.isFile()){
            if(!dir.exists()){
                logger.info("creating directory: " + dir.getName());
                logger.info(dir.getPath());

                boolean result = false;

                try {
                    result = dir.mkdirs();
                } catch(SecurityException se){
                    logger.error("directory not created: SecurityException " + se.toString());
                }
                if(result) {
                    logger.info("directory created");
                }
            }
        }
    }

    /**
     * Calculates the approximate number of lines in the method body
     * @param bodies bodies of methods
     * @return average number of lines per method
     * */
    private int numberLinesInBody(List<String> bodies){

        double ave = 0;

        if (bodies.size() > 0) {
            for(String st : bodies){
                ave += st.split("\r\n|\r|\n").length + 5;
            }
            return (int)(ave / bodies.size());
        }

        return 0;
    }

    /**
     * Calculates the number of lines in the class without methods
     * @param gc {@link GeneratedClass}
     * @return number of lines per class without methods
     * */
    private int numberLineOnlyClass(GeneratedClass gc){
        return  gc.getImports().size() + 4;
    }

    /**
     * For each input parameter combination, it creates one unit test,
     * which it saves in the list. The list then returns
     * @param clazz class
     * @param method method for testing
     * @param arr list of generated parameters
     * */
    private static List<String> createCombinationOfParameters(Clazz clazz, Method method, List<GeneratedParameter> arr) {

        List<String> output = new ArrayList<>();
        int n = arr.size();
        int[] indices = new int[n];

        for (int i = 0; i < n; i++){
            indices[i] = 0;
        }

        while (true) {

            String body = "";

            for (int i = 0; i < n; i++) {
                body += arr.get(i).getBody().get(indices[i]);
            }

            body += "\n";

            if (method.isStatic()) {

                String[] paramcall = new String[method.getParameterCount()];

                for(Parameter p: method.getMethodParameter()){
                    paramcall[p.getIndex()] = p.getName();
                }

                body += clazz.getClassName()+"." + method.getName() + "(";

                for(int i = 0; i< paramcall.length; i++){
                    body += paramcall[i];
                    if (i != paramcall.length-1) {
                        body += ",";
                    }
                }

                body +=  ");\n";
            } else {
                body += clazz.getClassName() + " testClass = new "+clazz.getClassName()+"();\n";

                String[] paramcall = new String[method.getParameterCount()];

                for(Parameter p: method.getMethodParameter()){
                    paramcall[p.getIndex()] = p.getName();
                }

                body += "testClass." + method.getName() + "(";

                for(int i = 0; i< paramcall.length; i++){
                    body += paramcall[i];
                    if (i != paramcall.length-1) {
                        body += ",";
                    }
                }

                body +=  ");\n";
            }

            output.add(body);

            int next = n - 1;
            while (next >= 0 && (indices[next] + 1 >= arr.get(next).getBody().size())){
                next--;
            }

            if (next < 0){
                break;
            }

            indices[next]++;

            for (int i = next + 1; i < n; i++){
                indices[i] = 0;
            }
        }

        return output;
    }

}
