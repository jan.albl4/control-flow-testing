package cz.zcu.kiv.cft.data.generator.primitive;

import cz.zcu.kiv.cft.data.generator.primitive.evaluators.*;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import org.apache.commons.lang.math.*;
import soot.Type;

import java.util.*;

/**
 *
 * Class that is used to generate values for primitive data symbols
 *
 * @author Jan Albl
 */
public class PrimitiveValueGeneratorImpl implements PrimitiveValueGenerator {

    /** Evaluator for integer data types */
    private IntEvaluator intEvaluator;

    /** Evaluator for long data types */
    private LongEvaluator longEvaluator;

    /** Evaluator short integer data types */
    private ShortEvaluator shortEvaluator;

    /** Evaluator for byte data types */
    private ByteEvaluator byteEvaluator;

    /** Evaluator for boolean data types */
    private BooleanEvaluator booleanEvaluator;

    /** Evaluator for char data types */
    private CharEvaluator charEvaluator;

    /** Evaluator for flaot data types */
    private FloatEvaluator floatEvaluator;

    /** Evaluator for double data types */
    private cz.zcu.kiv.cft.data.generator.primitive.evaluators.DoubleEvaluator doubleEvaluator;

    /** Symbol evaluator {@link SymbolicEvaluator} */
    private SymbolicEvaluator symbolicEvaluator;

    /**
     * Constructor to create an instance of this class.
     * **/
    public PrimitiveValueGeneratorImpl(){
        intEvaluator = new IntEvaluatorImpl();
        longEvaluator  = new LongEvaluatorImpl();
        shortEvaluator = new ShortEvaluatorImpl();
        byteEvaluator = new ByteEvaluatorImpl();

        booleanEvaluator = new BooleanEvaluatorImpl();
        charEvaluator = new CharEvaluatorImpl();
        floatEvaluator = new FloatEvaluatorImpl();
        doubleEvaluator = new DoubleEvaluatorImpl();
        symbolicEvaluator = new SymbolicEvaluatorImpl();
    }

    @Override
    public void calculateInterval(ConditionalExpression condition, Map<String, PrimitiveVariable> primitiveVariable){
        if (DataUtil.isPrimitiveCondition(condition)) {
            symbolicEvaluator.evaluate(condition, primitiveVariable);
        }
    }

    @Override
    public void randomValuesToEmptySymbols(Method method){

        if (method!= null) {
            for(Parameter p : method.getMethodParameter()) {
                if(p instanceof PrimitiveParameter){
                    if(((PrimitiveParameter) p).getValues().size() == 0){

                        String value = null;
                        String type = p.getDataType();

                        if (type.equals("int")) {
                            value = intEvaluator.getRandomValue();
                        } else if(type.equals("char")) {
                            value =  charEvaluator.getRandomValue();
                        } else if(type.equals("long")) {
                            value =  longEvaluator.getRandomValue();
                        } else if(type.equals("double")) {
                            value =  doubleEvaluator.getRandomValue();
                        } else if(type.equals("float")) {
                            value =  floatEvaluator.getRandomValue();
                        } else if(type.equals("short")) {
                            value =  shortEvaluator.getRandomValue();
                        }else if(type.equals("byte")) {
                            value = byteEvaluator.getRandomValue();
                        } else if(type.equals("boolean")) {
                            booleanEvaluator.getRandomValue();
                        }

                        if(value != null){
                            ((PrimitiveParameter) p).getValues().add(value);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void fillPrimitiveParameterVariable(Method method, Map<String, PrimitiveVariable> primitiveVariable){

        this.evalPrimitiveInterval(primitiveVariable);

        for (Map.Entry<String, PrimitiveVariable> entry : primitiveVariable.entrySet()) {
            for(Parameter p : method.getMethodParameter()){

                String[] objectAndField = entry.getValue().getName().split("\\.");

                if(objectAndField.length > 0 && p.getName().equals(objectAndField[0])){

                    if(p instanceof PrimitiveParameter){
                        fillPrimitiveParameter((PrimitiveParameter) p, entry.getValue().getRanges());
                    }else if(p instanceof ObjectParameter) {

                        ObjectParameter actual = (ObjectParameter) p;

                        for(int i = 1; i<objectAndField.length; i++){

                            if(DataUtil.isPrimitive(entry.getValue().getParrentType().get(i))){

                                boolean canAdd = true;

                                for(PrimitiveParameter pp : actual.getPrimitiveFields()){
                                    if(pp.getName().equals(objectAndField[i])){
                                        canAdd = false;
                                    }
                                }

                                if(canAdd){
                                    String type = entry.getValue().getParrentType().get(i);

                                    PrimitiveParameter pp = new PrimitiveParameter();
                                    pp.setName(objectAndField[i]);
                                    pp.setSootType(new Type() {
                                        @Override
                                        public String toString() {
                                            return type;
                                        }
                                    });
                                    pp.setPrimitive(true);

                                    actual.getPrimitiveFields().add(pp);

                                    if(actual.getPossibleObjectTypes().size() == 0){
                                        actual.getPossibleObjectTypes().add(entry.getValue().getParrentType().get(i-1));
                                    }

                                    fillPrimitiveParameter(pp, entry.getValue().getRanges());

                                    break;
                                }

                            }else {
                                //it is not primitive
                                boolean canAdd = true;

                                for(ObjectParameter op : actual.getObjects()){
                                    if(op.getName().equals(objectAndField[i])){
                                        actual = op;
                                        canAdd = false;
                                        break;
                                    }
                                }

                                if(canAdd){
                                    String type = entry.getValue().getParrentType().get(i);

                                    ObjectParameter op = new ObjectParameter();
                                    op.setName(objectAndField[i]);
                                    op.getPossibleObjectTypes().add(type);
                                    op.setPrimitive(false);

                                    actual.getObjects().add(op);
                                    actual = op;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Adds values from the input intervals to the primitive parameter
     * @param p primitive parameter
     * @param ranges value intervals
     * */
    private void fillPrimitiveParameter(PrimitiveParameter p, List<Range> ranges){
        for(Range r : ranges){
            String type = p.getDataType();
            if (type.equals("int")) {
                intEvaluator.addIntValuesToParameter(p,r);
            } else if(type.equals("char")) {
                charEvaluator.addCharValuesToParameter(p, r);
            } else if(type.equals("boolean")) {
                booleanEvaluator.addBooleanValuesToParameter(p);
            } else if(type.equals("long")) {
                longEvaluator.addLongValuesToParameter(p, r);
            } else if(type.equals("double")) {
                doubleEvaluator.addDoubleValuesToParameter(p, r);
            } else if(type.equals("float")) {
                floatEvaluator.addFloatValuesToParameter(p, r);
            } else if(type.equals("short")) {
                shortEvaluator.addShortValuesToParameter(p, r);
            }else if(type.equals("byte")) {
                byteEvaluator.addByteValuesToParameter(p, r);
            }
        }
    }

    /**
     * From the limit values for primitive parameters they create
     * intervals, which they then store in the appropriate PrimitiveVariable classes
     * */
    private void evalPrimitiveInterval(Map<String, PrimitiveVariable> primitiveVariable){

        for (Map.Entry<String, PrimitiveVariable> entry : primitiveVariable.entrySet()) {

            Collections.sort(entry.getValue().getBorderValue());

            if(entry.getValue().getBorderValue() != null && entry.getValue().getBorderValue().size() > 0){

                String type = entry.getValue().getType();

                List<Range> ranges = new ArrayList<>();
                PrimitiveVariable p = entry.getValue();

                if (type.equals("int")) {
                    ranges = intEvaluator.calculateIntervalsFromLimitValues(p);
                } else if(type.equals("char")) {
                    ranges =  charEvaluator.calculateIntervalsFromLimitValues(p);
                } else if(type.equals("long")) {
                    ranges =  longEvaluator.calculateIntervalsFromLimitValues(p);
                } else if(type.equals("double")) {
                    ranges =  doubleEvaluator.calculateIntervalsFromLimitValues(p);
                } else if(type.equals("float")) {
                    ranges =  floatEvaluator.calculateIntervalsFromLimitValues(p);
                } else if(type.equals("short")) {
                    ranges =  shortEvaluator.calculateIntervalsFromLimitValues(p);
                } else if(type.equals("byte")) {
                    ranges = byteEvaluator.calculateIntervalsFromLimitValues(p);
                }

                entry.getValue().getRanges().addAll(ranges);
            }
        }
    }
}
