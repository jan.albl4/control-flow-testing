package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import java.util.List;
import java.util.Map;

/**
 *
 * Interface class for analyzing java source files. The output of this interface is an  {@link ControlFlowAnalysisOutput}
 * data structure that contains the information obtained from the source code. It can also obtain a control flow graph from the source code.
 *
 * @author Jan Albl
 */
public interface Analyzer {

    /**
     * It analyzes all methods from the input class
     *
     * @param className name of analyzed class
     * @return {@link ControlFlowAnalysisOutput}
     * */
    ControlFlowAnalysisOutput analyzeClass(String className);

    /**
     * It analyzes the method determined by input parameters
     *
     * @param className name of analyzed class
     * @param methodName name of analyzed method
     * @return {@link ControlFlowAnalysisOutput}
     * */
    ControlFlowAnalysisOutput analyzeMethod(String className, String methodName);

    /**
     * It analyzes the method determined by input parameters
     *
     * @param className name of analyzed class
     * @param methodName name of analyzed method
     * @param parameters data types of input parameters that specify the method to be analyzed
     * @return {@link ControlFlowAnalysisOutput}
     * */
    ControlFlowAnalysisOutput analyzeMethod(String className, String methodName, List<String> parameters);

    /**
     * It analyzes the method determined by input parameters
     *
     * @param className name of analyzed class
     * @param methodName name of analyzed method
     * @param parameters data types of input parameters that specify the method to be analyzed
     * @param returnType the return type of the analyzed method used to determine the method to be analyzed
     * @return {@link ControlFlowAnalysisOutput}
     * */
    ControlFlowAnalysisOutput analyzeMethod(String className, String methodName, List<String> parameters, String returnType);

    /**
     * Analyzes the methods of all classes that are contained in the specified path
     *
     * @param projectFolderPath path to the project folder to be analyzed. Other subfolders are treated as individual packages
     * @return {@link ControlFlowAnalysisOutput}
     * */
    ControlFlowAnalysisOutput analyzeFolder(String projectFolderPath);


    /**
     * From the input method, it obtains a control flow graph that returns if possible
     *
     * @param className name of analyzed class
     * @param methodName name of analyzed method
     * @param parameters data types of input parameters that specify the method to be analyzed
     * @param returnType the return type of the analyzed method used to determine the method to be analyzed
     * @return {@link ControlFlowGraph}
     * */
    ControlFlowGraph getControlFlowGraph(String className, String methodName, List<String> parameters, String returnType);

    /**
     * It gains control flow charts from the input class. These will then return in the DOT format list.
     *
     * @param className name of analyzed class
     * @return control flow graph list in dot format - {@link ControlFlowGraph}
     * */
    List<String> getDots(String className);

    /**
     * It gains control flow chart from the input method. These will then return in the DOT format.
     *
     * @param className name of analyzed class
     * @param methodName name of analyzed method
     * @param parameters data types of input parameters that specify the method to be analyzed
     * @param returnType the return type of the analyzed method used to determine the method to be analyzed
     * @return control flow graph in dot format - {@link ControlFlowGraph}
     * */
    String getDots(String className, String methodName, List<String> parameters, String returnType);

    /**
     * Obtain all possible control flow graph of the classes in the analyzed folder
     *
     * @param projectFolderPath  path to the project folder to be analyzed. Other subfolders are treated as individual packages
     * @return map control flow graph, where the key is the name of the class analyzed and the value is a list of the control flow graph - {@link ControlFlowGraph}
     * */
    Map<String, List<String>> getDost(String projectFolderPath);
}