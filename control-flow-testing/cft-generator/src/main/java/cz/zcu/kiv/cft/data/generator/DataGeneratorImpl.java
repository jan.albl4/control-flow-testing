package cz.zcu.kiv.cft.data.generator;

import cz.zcu.kiv.cft.analyzer.loader.SootClassLoader;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoaderImpl;
import cz.zcu.kiv.cft.data.generator.data.EnumVariable;
import cz.zcu.kiv.cft.data.generator.enums.EnumValueGenerator;
import cz.zcu.kiv.cft.data.generator.enums.EnumValueGeneratorImpl;
import cz.zcu.kiv.cft.data.generator.object.ObjectGenerator;
import cz.zcu.kiv.cft.data.generator.object.ObjectGeneratorImpl;
import cz.zcu.kiv.cft.data.generator.primitive.PrimitiveValueGenerator;
import cz.zcu.kiv.cft.data.generator.primitive.PrimitiveValueGeneratorImpl;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.data.generator.data.ObjectVariable;
import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.tools.log.Logger;

import java.util.*;

/**
 *
 * Class for evaluates branching conditions. It assigns values to parameter methods
 *
 * @author Jan Albl
 */
public class DataGeneratorImpl implements DataGenerator {

    /** Evaluator for primitive data types */
    private PrimitiveValueGenerator primitiveValueGenerator;

    /** Evaluator for enum data types */
    private EnumValueGenerator enumValueGenerator;

    /** Evaluator for object data types */
    private ObjectGenerator objectGenerator;

    /**
     * Generated values for primitive data types of parameters. The key is the evaluated
     * symbol and the value is a list of values for this symbol.
     *  */
    private Map<String, PrimitiveVariable> primitiveVariables;

    /**
     * Generated values for object data types of parameters. The key is the evaluated
     * symbol and the value is a list of values for this symbol.
     *  */
    private Map<String, ObjectVariable> objectDataType;

    /**
     * Generated values for enum data types of parameters. The key is the evaluated
     * symbol and the value is a list of values for this symbol.
     *  */
    private Map<String, EnumVariable> enumVariable;

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    /**
     * Constructor to create an instance of this class.
     * **/
    public DataGeneratorImpl(){
        this.primitiveValueGenerator = new PrimitiveValueGeneratorImpl();
        this.enumValueGenerator = new EnumValueGeneratorImpl();
        this.objectGenerator = new ObjectGeneratorImpl();
        clearMemory();
    }

    /**
     * Empties the possible values memory
     * */
    private void clearMemory(){
        this.primitiveVariables = new HashMap<>();
        this.objectDataType = new HashMap<>();
        this.enumVariable = new HashMap<>();
    }

    @Override
    public void generateData(ControlFlowAnalysisOutput controlFlowAnalysisOutput) {
        logger.info("I evaluate possible values for branching conditions");
        this.generateDataForBranching(controlFlowAnalysisOutput);
    }

    /**
     * Generates test data for branching conditions dependent on input parameters
     * @param cfao {@link ControlFlowAnalysisOutput}
     * */
    private void generateDataForBranching(ControlFlowAnalysisOutput cfao){
        if (cfao.getClasses() != null
                && cfao.getClasses().size() != 0) {

            for (Clazz clazz: cfao.getClasses()) {
                if(clazz.getMethods() != null){
                    logger.info("Processing \""+clazz.getClassName()+"\" class");

                    SootClassLoader sootClassLoader = SootClassLoaderImpl.getInstance(clazz.getClassPath(), true);
                    DataUtil.setSootClassLoader(sootClassLoader);

                    for (Method method : clazz.getMethods()) {
                        this.clearMemory();

                        logger.debug("Processing \""+method.getName()+"\" method");

                        for (ConditionElement condition: method.getConditions()) {

                            if (condition instanceof SwitchConditions) {
                                for(Condition c : ((SwitchConditions) condition).getBranches()) {
                                    processCondition(c);
                                }
                            } else {
                                processCondition((Condition) condition);
                            }
                        }

                        addGeneratedValues(method);
                        randomValuesToEmptySymbols(method);
                    }
                }
            }
        }
    }


   /**
    * For the input condition and its sub-conditions, which are
    * dependent on input parameters, try to generate test data
    * @param c condition
    * */
    private void processCondition(Condition c){
        if(c != null){

            for(ConditionalExpression ce : c.getConditionalExpressions()){
                if(ce.isInputDependent()){
                    removeCmplCmpq(ce);
                    primitiveValueGenerator.calculateInterval(ce, primitiveVariables);
                    enumValueGenerator.evalInstanceOf(ce, enumVariable);
                    objectGenerator.evalCondition(ce, objectDataType);
                }
            }


            for (ConditionElement condition: c.getTrueBranch()){
                if (condition instanceof Condition) {
                    processCondition((Condition) condition);
                } else {
                    for (Condition cw :((SwitchConditions)condition).getBranches()) {
                        processCondition(cw);
                    }
                }
            }

            for (ConditionElement condition: c.getFalseBranch()){

                if (condition instanceof Condition) {
                    processCondition((Condition) condition);
                } else {
                    for (Condition cw :((SwitchConditions)condition).getBranches()) {
                        processCondition(cw);
                    }
                }
            }
        }
    }

    /**
     * Replaces cmpl, cmpg, and cmpg from the condition expression
     * @param conditionalExpression condition
     * */
    private void removeCmplCmpq(ConditionalExpression conditionalExpression){
        conditionalExpression.setExpression(conditionalExpression.getExpression().replace("cmpl", "-"));
        conditionalExpression.setExpression(conditionalExpression.getExpression().replace("cmpg", "-"));
        conditionalExpression.setExpression(conditionalExpression.getExpression().replace("cmpg", "-"));
    }

    /**
     * Adds the generated values to the output method parameters
     * @param method data structure to which the generated values will be stored
     * */
    private void addGeneratedValues(Method method){
        if (method != null) {
            primitiveValueGenerator.fillPrimitiveParameterVariable(method, primitiveVariables);
            objectGenerator.fillObjectParameter(method, objectDataType);
            enumValueGenerator.fillEnumParameter(method, enumVariable);
        }
    }

    /**
     * Adds random values to non-evaluated input method parameters
     * @param method data structure to which the generated random values will be stored
     * */
    private void randomValuesToEmptySymbols(Method method){
        if (method != null) {
            primitiveValueGenerator.randomValuesToEmptySymbols(method);
            enumValueGenerator.randomValuesToEmptySymbols(method);
            objectGenerator.randomValuesToEmptySymbols(method);
        }
    }
}






