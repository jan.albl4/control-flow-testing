package cz.zcu.kiv.cft.test.generator;

import cz.zcu.kiv.cft.output.data.EnumParameter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class generating bodies enum parameters
 *
 * @author Jan Albl
 */
public class GenerateEnumParameter extends GeneratedParameter {

    public GenerateEnumParameter(EnumParameter enumParameter){
        this.setName(enumParameter.getName());

        createBodys(enumParameter);
    }

    private void createBodys(EnumParameter p){

        List<String> out = new ArrayList<>();

        for(String value : p.getPossibleEnums()){
            String body = "";

            body += p.getDataType() + " ";
            body += p.getName();
            body += " = " + value + ";\n";

            out.add(body);
        }

        this.setBody(out);
    }
}
