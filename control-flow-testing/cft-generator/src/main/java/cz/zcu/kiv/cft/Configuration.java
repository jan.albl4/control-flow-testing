package cz.zcu.kiv.cft;

import java.util.List;

/**
 *
 * Class representing possible configuration of library startup
 *
 * @author Jan Albl
 */
public class Configuration {

    /** Name of testing class */
    private String className;

    /** Path to the analyzed project */
    private String projectPath;

    /** Package */
    private String packagePath;

    /** Method name */
    private String methodName;

    /** Maximum depth of search */
    private int maxDepth;

    /** The approximate maximum number of rows of the generated test class */
    private int maximumLines;

    /** Type of analysis used to process the method */
    private AnalyzedType analyzedType;

    /** Data types of input method parameters*/
    private List<String> parameters;

    /** Return type of method */
    private String returnType;

    /** Path where tests or data will be generated */
    private String outputPath;

    /** Flag that determines whether non-public methods will be analyzed */
    private boolean isAnalyzePrivateMethod;

    /** What analysis will be used to create symbol tables */
    private boolean isDetailedAnalysis;

    /** Loads all classes on the project and other referenced ones */
    private boolean isLoadNecessaryClasses;

    /** Only data classes will be generated */
    private boolean isDataAnalysis;

    private Configuration(String className,
                          String methodName,
                          String projectPath,
                          String packagePath,
                          String outputPath,
                          List<String> parameters,
                          String returnType,
                          int maxDepth,
                          int maximumLines,
                          boolean isAnalyzePrivateMethod,
                          boolean isDetailedAnalysis,
                          boolean isLoadNecessaryClasses,
                          boolean isDataAnalysis,
                          AnalyzedType analyzedType) {

        this.className = className;
        this.methodName = methodName;
        this.projectPath = projectPath;
        this.packagePath = packagePath;
        this.maxDepth = maxDepth;
        this.maximumLines = maximumLines;
        this.analyzedType = analyzedType;
        this.outputPath = outputPath;
        this.parameters = parameters;
        this.returnType = returnType;
        this.isAnalyzePrivateMethod = isAnalyzePrivateMethod;
        this.isDetailedAnalysis = isDetailedAnalysis;
        this.isLoadNecessaryClasses=isLoadNecessaryClasses;
        this.isDataAnalysis=isDataAnalysis;
    }

    public String getClassName() {
        return className;
    }

    public String getProjectPath() {
        return projectPath;
    }

    public String getPackagePath() {
        return packagePath;
    }


    public String getMethodName() {
        return methodName;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public AnalyzedType getAnalyzedType() {
        return analyzedType;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public String getReturnType() {
        return returnType;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public boolean isAnalyzePrivateMethod() {
        return isAnalyzePrivateMethod;
    }

    public boolean isDetailedAnalysis() {
        return isDetailedAnalysis;
    }

    public boolean isLoadNecessaryClasses() {
        return isLoadNecessaryClasses;
    }

    public boolean isDataAnalysis() {
        return isDataAnalysis;
    }

    public int getMaximumLines() {
        return maximumLines;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "className='" + className + '\'' +
                ", projectPath='" + projectPath + '\'' +
                ", packagePath='" + packagePath + '\'' +
                ", methodName='" + methodName + '\'' +
                ", maxDepth=" + maxDepth +
                '}';
    }

    public static class Builder{

        private String className;

        private String projectPath;

        private String packagePath;

        private String methodName;

        private int maxDepth;

        private int maximumLines;

        private AnalyzedType analyzedType = AnalyzedType.METHOD;

        private List<String> parameters;

        private String returnType;

        private String outputPath;

        private boolean isAnalyzePrivateMethod;

        private boolean isLoadNecessaryClasses;

        private boolean isDataAnalysis;

        private boolean isDetailedAnalysis;

        public Configuration.Builder setProjectPath(String projectPath) {
            this.projectPath = projectPath;
            return this;
        }

        public Configuration.Builder setPackagePath(String packagePath) {
            this.packagePath = packagePath;
            return this;
        }

        public Configuration.Builder setMethodName(String methodName) {
            this.methodName = methodName;
            return this;
        }

        public Configuration.Builder setMaxDepth(int maxDepth) {
            if(maxDepth >= 0){
                this.maxDepth = maxDepth;
            }
            return this;
        }

        public Configuration.Builder setMaximumLines(int maximumLines) {
            if(maxDepth >= 0){
                this.maximumLines = maximumLines;
            }
            return this;
        }

        public Configuration.Builder setAnalyzedType(AnalyzedType analyzedType) {
            if(analyzedType != null){
                this.analyzedType = analyzedType;
            }
            return this;
        }

        public Configuration.Builder setParameters(List<String> parameters) {
            this.parameters = parameters;
            return this;
        }

        public Configuration.Builder setReturnType(String returnType) {
            this.returnType = returnType;
            return this;
        }

        public Configuration.Builder setOutputPath(String outputPath) {
            this.outputPath = outputPath;
            return this;
        }

        public Configuration.Builder setAnalyzePrivateMethod(boolean analyzePrivateMethod) {
            isAnalyzePrivateMethod = analyzePrivateMethod;
            return this;
        }

        public Configuration.Builder setDetailedAnalysis(boolean detailedAnalysis) {
            isDetailedAnalysis = detailedAnalysis;
            return this;
        }

        public Configuration.Builder setClassName(String className){
            this.className = className;
            return this;
        }

        public Configuration.Builder setLoadNecessaryClasses(boolean loadNecessaryClasses) {
            isLoadNecessaryClasses = loadNecessaryClasses;
            return this;
        }

        public Configuration.Builder setDataAnalysis(boolean isDataAnalysis) {
            this.isDataAnalysis = isDataAnalysis;
            return this;
        }

        public Configuration build(){
            return new Configuration(className, methodName, projectPath, packagePath, outputPath, parameters, returnType, maxDepth, maximumLines, isAnalyzePrivateMethod, isLoadNecessaryClasses, isDetailedAnalysis, isDataAnalysis, analyzedType);
        }
    }
}
