package cz.zcu.kiv.cft.analyzer.symbols;

import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import soot.Unit;
import soot.toolkits.graph.UnitGraph;

import java.util.List;
import java.util.Map;

/**
 *
 * Interface for creating symbol tables
 *
 * @author Jan Albl
 */
public interface SymbolTableAnalyzer {

    /**
     * Creates a symbol table for the control flow graph
     * @param cfg analyzed control flow graph
     * @return symbol table for the graph
     * */
    Map<Unit, Map<UnitGraph, List<SymbolTableStack>>> symbolTableAnalysis(ControlFlowGraph cfg);


    /**
     * Sets the maximum scan depth
     * @param maxDepth value
     * */
    void setMaxDepth(int maxDepth);
}


