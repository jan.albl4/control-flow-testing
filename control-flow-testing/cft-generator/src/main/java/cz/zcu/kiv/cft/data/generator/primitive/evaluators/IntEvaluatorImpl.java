package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class IntEvaluatorImpl implements IntEvaluator {


    @Override
    public void addIntValuesToParameter(PrimitiveParameter primitiveParameter, Range range) {
        if(primitiveParameter != null && range != null) {

            List<String> values = getValueFromRange(range);

            for(String v: values){
                primitiveParameter.getValues().add(v);
            }
        }
    }

    @Override
    public List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter){
        List<Range> ranges = new ArrayList<>();

        if (primitiveParameter.getBorderValue().size()>0) {
           ranges.add(new IntRange(Integer.MIN_VALUE, primitiveParameter.getBorderValue().get(0)));
        }

        for (int i = 0; i < primitiveParameter.getBorderValue().size()-1; i++) {
            ranges.add(new IntRange(primitiveParameter.getBorderValue().get(i), primitiveParameter.getBorderValue().get(1+i)));
        }

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new IntRange(primitiveParameter.getBorderValue().get(primitiveParameter.getBorderValue().size()-1), Integer.MAX_VALUE));
        }

        return ranges;
    }

    @Override
    public String getRandomValue(){
        return String.valueOf(DataUtil.getRandomNumberInRange(Short.MIN_VALUE, Short.MAX_VALUE));
    }

    private List<String> getValueFromRange(Range range){
        List<String> values = new ArrayList<>();

        values.add(String.valueOf(range.getMinimumInteger()));


        int min = 1; //aby se nestalo ze dostanu cislo k
        int max = range.getMaximumInteger() - range.getMinimumInteger() - 1;

        if (range.getMaximumInteger() - range.getMinimumInteger() < 0){
            //preteklo cislo je vetsi nez Integer.Maxint
            max = Integer.MAX_VALUE - 1;
        }

        if (range.getMaximumInteger() - range.getMinimumInteger() != 0 && max > min) {
            Random random = new Random();
            int between  = random.nextInt(max + 1 - min) + min;
            values.add(String.valueOf(between + range.getMinimumInteger()));
        }

        values.add(String.valueOf(range.getMaximumInteger()));

        return values;
    }
}
