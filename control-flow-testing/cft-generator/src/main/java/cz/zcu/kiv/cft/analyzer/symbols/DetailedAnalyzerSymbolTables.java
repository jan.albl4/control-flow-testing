package cz.zcu.kiv.cft.analyzer.symbols;

import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import cz.zcu.kiv.cft.analyzer.CycleDetector;
import cz.zcu.kiv.cft.tools.Utils;
import cz.zcu.kiv.cft.analyzer.branches.BranchStack;
import cz.zcu.kiv.cft.analyzer.branches.BranchStackItem;
import cz.zcu.kiv.cft.analyzer.branches.SwitchStackItem;
import cz.zcu.kiv.cft.tools.log.Logger;
import soot.Unit;
import soot.Value;
import soot.jimple.*;
import soot.toolkits.graph.UnitGraph;

import java.util.*;

/**
 *
 * Class for detailed analysis of creation of symbol tables from the control flow graph
 *
 * @author Jan Albl
 */
public class DetailedAnalyzerSymbolTables implements SymbolTableAnalyzer {

    /** Maximum depth of search */
    private int maxDepth = 0;

    /** Edges that create cycles in the flow control graph */
    private Map<Unit,Unit> cycleEdge;

    /** List of method parameters */
    private List<Unit> parameters;

    /** Analyzed control flow graph */
    private ControlFlowGraph cfg;

    /** Symbol table for node branching in  control flow graph */
    private Map<Unit, Map<UnitGraph, List<SymbolTableStack>>> globalVariableTables;

    /** Currently processed symbol table */
    private SymbolTableStack symbolTableStack;

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    @Override
    public Map<Unit,Map<UnitGraph, List<SymbolTableStack>>> symbolTableAnalysis(ControlFlowGraph cfg){

        logger.info("create a symbol table");

        this.cfg = cfg;
        this.cycleEdge = CycleDetector.detectEdgesOfCycle(cfg);
        this.createSymbolTables(cfg);

        return globalVariableTables;
    }

    @Override
    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    /**
     * Creates a symbol table for the control flow graph
     * @param cfg analyzed control flow graph
     * */
    private void createSymbolTables(ControlFlowGraph cfg){
        this.globalVariableTables = new HashMap<Unit, Map<UnitGraph, List<SymbolTableStack>>>();
        this.parameters = new ArrayList<Unit>();

        symbolTableStack = new SymbolTableStack();
        SymbolTable symbolTable = new SymbolTable();
        symbolTableStack.push(symbolTable);

        createSymbolTables(cfg.getUnitGraph(), symbolTableStack);
    }

    /**
     * Creates a symbol table for unitGraph and saves the result in {@link SymbolTableStack}
     * @param unitGraph analyzed control flow graph
     * @param symbolTableStack {@link SymbolTableStack}
     * */
    private void createSymbolTables(UnitGraph unitGraph, SymbolTableStack symbolTableStack){

        Iterator i = unitGraph.iterator();
        Unit u = (Unit)i.next();

        BranchStack branchStack = new BranchStack();

        goTrueBranche(u,unitGraph, branchStack, symbolTableStack);

        while (branchStack.size() != 0){

            symbolTableStack.replaceTop(branchStack.pop().getVariableTableLast());
            branchStack.setSymbolTableStack(symbolTableStack);

            u = branchStack.getNextUnprocessedNode(unitGraph);

            goTrueBranche(u,unitGraph, branchStack, symbolTableStack);

            logger.debug("count of unprocessed branches: " + branchStack.size());
        }
    }

    /**
     * Searches for method calls if the maximum depth is not exceeded
     * @param u the unit that calls the method
     * @param actual current control flow graph
     * @param args list of method arguments
     * */
    private void methodCall(Unit u, UnitGraph actual, List<Value> args){
        if(maxDepth > 0){
            maxDepth--;

            symbolTableStack.push(new SymbolTable());
            symbolTableStack.pop().setLastParam(args);

            if(cfg.getUnitGraphMap().containsKey(u)
                    && cfg.getUnitGraphMap().get(u).containsKey(actual)){

                createSymbolTables(cfg.getUnitGraphMap().get(u).get(actual), symbolTableStack);
            }

            symbolTableStack.popRm();
            maxDepth++;
        }
    }

    /**
     * If the node is calling the method, it scans it to maximum depth
     * @param u unit
     * @param actual control flow graph
     * */
    private void invokeStmt(Unit u, UnitGraph actual){
        if (u instanceof InvokeStmt) {
            InvokeStmt invokeStmt = (InvokeStmt)u;

            methodCall(u, actual, invokeStmt.getInvokeExpr().getArgs());
        }
    }

    /**
     * If the node is DefinitionStmt, creates a symbol table entry and stores it
     * @param u unit
     * @param actual control flow graph
     * */
    private void  definitionStmt(Unit u, UnitGraph actual){
        if (u instanceof DefinitionStmt) {
            DefinitionStmt assign = (DefinitionStmt) u;
            Value rightOp = assign.getRightOp();
            Value leftOp = assign.getLeftOp();

            SymbolItem symbolItem = new SymbolItem();
            symbolItem.setType(leftOp.getType());
            symbolItem.setName(leftOp.toString());
            symbolItem.setSootValue(leftOp);
            symbolItem.setRightOp(rightOp);

            if (rightOp instanceof ParameterRef){

                if(symbolTableStack.getSymbolTableStack().size() == 1){
                    symbolItem.setParam(true);
                    symbolItem.setParameterRef((ParameterRef)rightOp);

                    if(!parameters.contains(u)){
                        parameters.add(u);
                    }
                }else{
                    ParameterRef parameterRef = (ParameterRef) rightOp;
                    parameterRef.getIndex();

                    if(symbolTableStack.pop().getLastParam().size() >= parameterRef.getIndex()){
                        symbolItem.setRightOp(symbolTableStack.pop().getLastParam().get(parameterRef.getIndex()));
                    }
                }

            } else if(rightOp instanceof VirtualInvokeExpr){

                if (!Utils.isGetField((VirtualInvokeExpr) rightOp)) {
                    VirtualInvokeExpr virtualInvokeExpr = (VirtualInvokeExpr) rightOp;
                    methodCall(u, actual, virtualInvokeExpr.getArgs());
                }

            } else if(rightOp instanceof StaticInvokeExpr){
                StaticInvokeExpr staticInvokeExpr = (StaticInvokeExpr)rightOp;
                methodCall(u, actual, staticInvokeExpr.getArgs());

            }else if(rightOp instanceof SpecialInvokeExpr){

                SpecialInvokeExpr specialInvokeExpr = (SpecialInvokeExpr)rightOp;
                methodCall(u, actual, specialInvokeExpr.getArgs());
            }

            symbolTableStack.pop().addSymbolItem(symbolItem, symbolTableStack);
        }
    }

    /**
     * If the branch unit stores the branching into the stack for further processing
     * @param u unit
     * @param branchStack stack for branches
     * @param symbolTableStack stack for symbol tables
     * @param symbolTableStack control flow graph
     *
     * */
    private void ifStmt(Unit u, BranchStack branchStack, SymbolTableStack symbolTableStack, UnitGraph unitGraph){
        if(u instanceof IfStmt && unitGraph.getSuccsOf(u) != null && unitGraph.getSuccsOf(u).size() == 2){
            BranchStackItem branchStackItem = new BranchStackItem(u, null);

            SymbolTableStack clon = symbolTableStack.clone();

            branchStackItem.setSymbolTableStack(clon);
            branchStack.push(branchStackItem, false);

            addToGlobaVariableTableStack(u, clon, unitGraph);
        }
    }

    /**
     * If the branch unit stores the branching into the stack for further processing
     * @param u unit
     * @param branchStack stack for branches
     * @param symbolTableStack stack for symbol tables
     * @param unitGraph control flow graph
     * */
    private void switchStmt(Unit u, BranchStack branchStack, SymbolTableStack symbolTableStack, UnitGraph unitGraph) {
        if(u instanceof TableSwitchStmt || u instanceof LookupSwitchStmt) {
            SwitchStackItem stackItem = new SwitchStackItem(u);

            SymbolTableStack clon = symbolTableStack.clone();

            stackItem.setSymbolTableStack(clon);
            branchStack.push(stackItem, false);

            addToGlobaVariableTableStack(u, clon, unitGraph);
        }
    }

    /**
     * By graph and unit adds the symbol table to the global store
     * @param u unit
     * @param symbolTableStack stack for symbol tables
     * @param unitGraph control flow graph
     * */
    private void addToGlobaVariableTableStack(Unit u, SymbolTableStack symbolTableStack, UnitGraph unitGraph){
        if (globalVariableTables.containsKey(u)) {

            if (!globalVariableTables.get(u).containsKey(unitGraph)) {
                globalVariableTables.get(u).put(unitGraph, new ArrayList<SymbolTableStack>());
            }

            globalVariableTables.get(u).get(unitGraph).add(symbolTableStack);

        }else {
            List<SymbolTableStack> vt = new ArrayList<SymbolTableStack>();
            vt.add(symbolTableStack);
            Map<UnitGraph, List<SymbolTableStack>> variableTableStackMap = new HashMap<>();
            variableTableStackMap.put(unitGraph, vt);
            globalVariableTables.put(u,variableTableStackMap);
        }
    }

    /**
     * Analyzing branch to end node of the method. When analyzing creates a symbol table
     * @param u unit
     * @param unitGraph control flow graph
     * @param branchStack stack for branches
     * @param symbolTableStack stack for symbol tables
     * */
    private void goTrueBranche(Unit u, UnitGraph unitGraph, BranchStack branchStack, SymbolTableStack symbolTableStack){

        while (u != null) {
            if (u instanceof DefinitionStmt){
                definitionStmt(u, unitGraph);
            } else if (u instanceof IfStmt){
                ifStmt(u, branchStack, symbolTableStack, unitGraph);
            } else if (u instanceof TableSwitchStmt || u instanceof LookupSwitchStmt){
                switchStmt(u, branchStack, symbolTableStack, unitGraph);
            } else if(u instanceof InvokeStmt){
                invokeStmt(u, unitGraph);
            }

            if(unitGraph.getSuccsOf(u) != null && unitGraph.getSuccsOf(u).size() > 0){

                Unit next = unitGraph.getSuccsOf(u).get(0);

                if(cycleEdge.containsKey(u) && cycleEdge.get(u).equals(next)){
                    logger.debug("cycle detected");
                    u = null;
                }else{
                    u = next;
                }
            }else{
                u = null;
            }
        }

        branchStack.markProcessedNode();
    }


}
