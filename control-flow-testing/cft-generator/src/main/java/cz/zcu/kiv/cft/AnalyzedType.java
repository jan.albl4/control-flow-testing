package cz.zcu.kiv.cft;

/**
 *
 * The type of analysis used to process the method. It decides whether the class, method, or entire folder is analyzed
 *
 * @author Jan Albl
 */
public enum AnalyzedType {
    FOLDER,
    CLASS,
    METHOD
}
