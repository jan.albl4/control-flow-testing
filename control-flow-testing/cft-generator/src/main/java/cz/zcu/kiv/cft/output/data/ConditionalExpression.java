package cz.zcu.kiv.cft.output.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.zcu.kiv.cft.output.adapters.MapAdapter;
import cz.zcu.kiv.cft.output.adapters.MapTypeAdapter;
import soot.Type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Class containing a condition expression
 *
 * @author Jan Albl
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ConditionalExpression {

    /** Expression */
    private String expression;

    /** Flag whether the condition is dependent only on input parameters */
    private boolean isInputDependent = false;

    /** Map of possible data types of used symbols */
    @JsonIgnore
    @XmlJavaTypeAdapter(MapTypeAdapter.class)
    private Map<String, List<Type>> argTypeMap = new HashMap<>();

    /** Map of generated values of used symbols */
    @XmlJavaTypeAdapter(MapAdapter.class)
    private Map<String, List<String>> argLimitValue = new HashMap<>();

    /** Possible symbol data types */
    @XmlJavaTypeAdapter(MapAdapter.class)
    private Map<String, List<String>> possibleDataType = new HashMap<>();

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Map<String, List<Type>> getArgTypeMap() {
        return argTypeMap;
    }

    public void setArgTypeMap(Map<String, List<Type>> argTypeMap) {
        this.argTypeMap = argTypeMap;
    }

    public Map<String, List<String>> getArgLimitValue() {
        return argLimitValue;
    }

    public void setArgLimitValue(Map<String, List<String>> argLimitValue) {
        this.argLimitValue = argLimitValue;
    }

    public Map<String, List<String>> getPossibleDataType() {
        return possibleDataType;
    }

    public void setPossibleDataType(Map<String, List<String>> possibleDataType) {
        this.possibleDataType = possibleDataType;
    }

    public boolean isInputDependent() {
        return isInputDependent;
    }

    public void setInputDependent(boolean inputDependent) {
        isInputDependent = inputDependent;
    }
}
