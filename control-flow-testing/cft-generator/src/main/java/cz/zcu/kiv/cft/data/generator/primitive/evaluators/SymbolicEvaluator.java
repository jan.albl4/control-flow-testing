package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;

import java.util.Map;

/**
 *
 * Interface that serves to evaluate the values of primitive equations.
 *
 * @author Jan Albl
 */
public interface SymbolicEvaluator {

    /**
     * If possible, evaluate the values of primitive equations
     * @param condition condition containing the equation
     * @param primitiveVariable generated data will be stored here
     * */
    void evaluate(ConditionalExpression condition, Map<String, PrimitiveVariable> primitiveVariable);
}
