package cz.zcu.kiv.cft.data.generator.utils;

import cz.zcu.kiv.cft.analyzer.exception.ClassLoaderNotSetException;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoader;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import soot.ArrayType;
import soot.RefType;
import soot.SootClass;
import soot.Type;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class DataUtil {

    private static SootClassLoader sootClassLoader;

    public static void setSootClassLoader(SootClassLoader scl){
        sootClassLoader = scl;
    }

    public static SootClass getSootClassFor(String type){
        if(sootClassLoader != null){
            return sootClassLoader.getSootClass(type);
        }else {
            throw new ClassLoaderNotSetException("Class loader not set.");
        }
    }

    public static List<SootClass> getSubClass(Type type){
        if(sootClassLoader != null){
            return sootClassLoader.getSubClass(type);
        }else {
            throw new ClassLoaderNotSetException("Class loader not set.");
        }
    }

    public static boolean isEnumType(Type type){

        SootClass sootClass = null;

        if(type instanceof RefType &&  ((RefType)type).getSootClass() != null){
            sootClass = ((RefType)type).getSootClass();
        }else if(type instanceof ArrayType && ((ArrayType)type).getElementType() instanceof RefType){
            sootClass = ((RefType)((ArrayType)type).getElementType()).getSootClass();
        }

        if(sootClass != null){
            try{
                sootClass = ((RefType)type).getSootClass();
                while(sootClass != null){
                    if(sootClass.getName().equals("java.lang.Enum")){
                        return true;
                    }else if(sootClass.getName().equals("java.lang.Object")){
                        return false;
                    }

                    sootClass = sootClass.getSuperclass();
                }
            }catch (Exception e){
                //TODO log
            }
        }

        return false;
    }

    public static boolean isEnumConditon(ConditionalExpression condition){

        boolean isEnumTypes = true;

        for (Map.Entry<String, List<Type>> entry : condition.getArgTypeMap().entrySet()) {

            //samostatny datovy typ
            if(!isEnumType(entry.getValue().get(entry.getValue().size()-1))){

                //muze to byt jeste primitivni field
                if(!(entry.getValue().size() > 0 && isEnumType(entry.getValue().get(entry.getValue().size()-1)))){
                    isEnumTypes = false;
                }
            }
        }


        return isEnumTypes;
    }


    public static boolean isPrimitive(Type type){
        return type != null && isPrimitive(type.toString());
    }

    public static boolean isPrimitive(String type) {

        return type != null && (type.equals("int")
                || type.equals("char")
                || type.equals("double")
                || type.equals("float")
                || type.equals("short")
                || type.equals("long")
                || type.equals("byte")
                || type.equals("boolean"));
    }

    //musi  byt min vetsi nez max, a generuje to vcetne cisel
    public static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static boolean isPrimitiveCondition(ConditionalExpression condition){

        boolean isAllPrimitive = true;

        for (Map.Entry<String, List<Type>> entry : condition.getArgTypeMap().entrySet()) {

            //samostatny datovy typ
            if(!DataUtil.isPrimitive(entry.getValue().get(entry.getValue().size()-1))){

                //muze to byt jeste primitivni field
                if(!(entry.getValue().size() > 0 && DataUtil.isPrimitive(entry.getValue().get(entry.getValue().size()-1).toString()))){
                    isAllPrimitive = false;
                }
            }
        }

        return isAllPrimitive;
    }
}
