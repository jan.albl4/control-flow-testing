package cz.zcu.kiv.cft.data.generator.data;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class BaseVariable {

    /**  */
    private String name;

    /**  */
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
