package cz.zcu.kiv.cft.analyzer.evaluator;

import cz.zcu.kiv.cft.analyzer.branches.BranchStackItem;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolItem;
import cz.zcu.kiv.cft.analyzer.symbols.SymbolTableStack;
import cz.zcu.kiv.cft.analyzer.symbols.HistorySymbolItem;
import cz.zcu.kiv.cft.analyzer.symbols.HistoryClassField;
import soot.SootMethod;
import soot.Value;
import soot.jimple.*;
import soot.jimple.internal.JInstanceFieldRef;

import java.util.List;

/**
 *
 * Class that evaluates whether the condition is dependent only on input parameters and will be further analyzed
 *
 * @author Jan Albl
 */
public class ParameterReferencer {

    /** The symbol table to be used for the evaluation */
    private SymbolTableStack symbolTableStack;

    /**
     * Constructor to create an instance of this class.
     *
     * @param symbolTableStack The symbol table to be used for the evaluation
     * **/
    public ParameterReferencer(SymbolTableStack symbolTableStack){
        this.symbolTableStack = symbolTableStack;
    }

    /**
     * evaluates whether the condition is dependent only on input parameters and will be further analyzed
     * @param indexSts what index in the symbol table will start searching
     * @param op expression that is being evaluated
     * @return true if it is
     * */
    public boolean isParameterRef(Value op, int indexSts){

        if(op instanceof IntConstant || op instanceof DoubleConstant || op instanceof FloatConstant || op instanceof LongConstant){
            return false;
        }

        List<SymbolItem> symbolItems = symbolTableStack.getSymbolTableStack().get(indexSts).getCurrentSymbolTable();

        for(int i = 0; i < symbolItems.size(); i++){

            SymbolItem vi = symbolItems.get(i);

            if (vi.getSootValue().equals(op)) {

                if (vi.getHistory().getSootValue() instanceof JInstanceFieldRef) {
                    JInstanceFieldRef jInstanceFieldRef = (JInstanceFieldRef) vi.getHistory().getSootValue();

                    if (vi.getHistory().getLeftOp() == null
                            && vi.getHistory().getRightOp() == null
                            && vi.getHistory().getNext() == null) {

                        for (int j = 0; j < symbolItems.size(); j++) {
                            if (symbolItems.get(j).getSootValue().equals(jInstanceFieldRef.getBase())) {
                                return isObjectParameterRef(symbolItems.get(j).getSootValue(), indexSts);
                            }

                        }
                    } else {
                        return isParameterRef(symbolItems.get(i).getHistory());
                    }
                } else if(vi.getHistory().getSootValue() instanceof VirtualInvokeExpr) {
                    VirtualInvokeExpr virtualInvokeExpr = (VirtualInvokeExpr) vi.getHistory().getSootValue();

                    if (isStringRef(virtualInvokeExpr)) {
                        return isStringParameterRef(symbolItems.get(i).getHistory(), indexSts);
                    } else {
                        return isParameterRef(symbolItems.get(i).getHistory());
                    }

                } else{
                    return isParameterRef(symbolItems.get(i).getHistory());
                }
            }
        }
        return false;
    }

    /**
     * The branching condition can be evaluated. It determines whether
     * the branch condition is dependent only on the input parameters
     * @param branchStackItem branch node
     * @param indexSts what index in the symbol table will start searching
     * @return true if it is
     * */
    public boolean canBeEvaluated(BranchStackItem branchStackItem, int indexSts){

        Value op1 = ((ConditionExpr)((IfStmt)branchStackItem.getUnit()).getCondition()).getOp1();
        Value op2 = ((ConditionExpr)((IfStmt)branchStackItem.getUnit()).getCondition()).getOp2();

        if ((isParameterRef(op1, indexSts) || isParameterRef(op2, indexSts)) && onlyParameterConstants(op1, indexSts) && onlyParameterConstants(op2, indexSts)){
            return true;
        }

        return false;
    }

    /**
     * Determines if the expression is a reference to a string object
     * @param virtualInvokeExpr invoke expression
     * @return true if it is
     * */
    private boolean isStringRef(VirtualInvokeExpr virtualInvokeExpr){

        SootMethod sootMethod = virtualInvokeExpr.getMethod();

        if (sootMethod.getDeclaringClass().getName().equals("java.lang.String")
                && sootMethod.getName().equals("equals")) {
            return true;
        }
        return false;
    }

    /**
     * Is an entry dependent on the string input parameter
     * @param object {@link HistorySymbolItem}
     * @param indexSts what index in the symbol table will start searching
     * @return true if it is
     * */
    private boolean isStringParameterRef(HistorySymbolItem object, int indexSts){

        List<SymbolItem> symbolItems = symbolTableStack.getSymbolTableStack().get(indexSts).getCurrentSymbolTable();

        if(object.getSootValue() instanceof VirtualInvokeExpr){

            Value base =  ((VirtualInvokeExpr) object.getSootValue()).getBase();

            for (int i = 0; i < symbolItems.size(); i++) {
                SymbolItem vi = symbolItems.get(i);

                if (vi.getSootValue().equals(base)) {
                   return isParameterRef(vi.getHistory());
                }
            }
        }

        return false;
    }

    /**
     * Is an input entry dependent on the method input parameter
     * @param object item analyzed
     * @param indexTss what index in the symbol table will start searching
     * @return true if it is
     * */
    private boolean isObjectParameterRef(Value object, int indexTss){

        List<SymbolItem> symbolItems = symbolTableStack.getSymbolTableStack().get(indexTss).getCurrentSymbolTable();

        for (int j = 0; j < symbolItems.size(); j++){
            if (symbolItems.get(j).getSootValue().equals(object)) {
                return isParameterRef(symbolItems.get(j).getHistory());
            }

        }

        return false;
    }

    /**
     * Is an input entry dependent on the method input parameter
     * @param op item analyzed
     * @return true if it is
     * */
    private static boolean isParameterRef(HistorySymbolItem op){

        if (op.getSootValue() instanceof IntConstant
                || op.getSootValue()instanceof DoubleConstant
                || op.getSootValue() instanceof FloatConstant
                || op.getSootValue() instanceof LongConstant) {
            return false;
        }

        if (op.getSootValue() instanceof ParameterRef) {
            return true;
        }


        boolean isDepend = false;

        if (op.getLeftOp() != null) {
            if(isParameterRef(op.getLeftOp())){
                isDepend = true;
            }
        }
        if (op.getRightOp() != null) {
            if(isParameterRef(op.getRightOp())) {
                isDepend = true;
            }
        }

        if (op instanceof HistoryClassField
                && op.getLeftOp() == null
                && op.getRightOp() == null) {

            //is a parent object by reference to a method parameter?
            HistoryClassField vf = (HistoryClassField)op;

            if (vf.getParentHSI().getSootValue() instanceof ParameterRef) {
                return true;
            } else {
                //parrent.son.age
                if (vf.getParentHSI().getLeftOp() != null) {
                    if(isParameterRef(vf.getParentHSI().getLeftOp())){
                        isDepend = true;
                    }
                }
                if (vf.getParentHSI().getRightOp() != null) {
                    if(isParameterRef(vf.getParentHSI().getRightOp())){
                        isDepend = true;
                    }
                }
            }
        }

        return isDepend;
    }

    /**
     * tests whether the values in the entry are only input parameters or constants
     * @param op item analyzed
     * @param indexSts what index in the symbol table will start searching
     * @return true if it is
     * */
    private boolean onlyParameterConstants(Value op, int indexSts){
        if(op instanceof IntConstant
                || op instanceof DoubleConstant
                || op instanceof FloatConstant
                || op instanceof LongConstant
                || op instanceof NullConstant){
            return true;
        }

        List<SymbolItem> symbolItems = symbolTableStack.getSymbolTableStack().get(indexSts).getCurrentSymbolTable();

        for(int i = 0; i < symbolItems.size(); i++){
            SymbolItem vi = symbolItems.get(i);

            if(vi.getSootValue().equals(op)){

                if(vi.getHistory().getSootValue() instanceof JInstanceFieldRef){
                    JInstanceFieldRef jInstanceFieldRef = (JInstanceFieldRef) vi.getHistory().getSootValue();

                    if(vi.getHistory().getLeftOp() == null
                            && vi.getHistory().getRightOp() == null
                            && vi.getHistory().getNext() == null){

                        for(int j = 0; j < symbolItems.size(); j++){
                            if(symbolItems.get(j).getSootValue().equals(jInstanceFieldRef.getBase())){

                                List<SymbolItem> symbolItemssses = symbolTableStack.getSymbolTableStack().get(indexSts).getCurrentSymbolTable();

                                for(int k = 0; k < symbolItemssses.size(); k++){
                                    if(symbolItemssses.get(k).getSootValue().equals(symbolItems.get(j).getSootValue())){
                                        return isOnlyParameterConstants(symbolItems.get(k).getHistory());
                                    }

                                }
                                return false;
                            }
                        }
                    }else {
                        return isOnlyParameterConstants(symbolItems.get(i).getHistory());
                    }
                } else if(vi.getHistory().getSootValue() instanceof VirtualInvokeExpr){
                    VirtualInvokeExpr virtualInvokeExpr = (VirtualInvokeExpr) vi.getHistory().getSootValue();

                    if(isStringRef(virtualInvokeExpr)){
                        return isStringParameterRef(symbolItems.get(i).getHistory(), indexSts);
                    }else {
                        return isOnlyParameterConstants(symbolItems.get(i).getHistory());
                    }

                } else{
                    return isOnlyParameterConstants(symbolItems.get(i).getHistory());
                }
            }
        }
        return false;
    }

    /**
     * tests whether the values in the entry are only input parameters or constants
     * @param op item analyzed
     * @return true if it is
     * */
    private static boolean isOnlyParameterConstants(HistorySymbolItem op){

        if(op.getSootValue() instanceof IntConstant
                || op.getSootValue()instanceof DoubleConstant
                || op.getSootValue() instanceof FloatConstant
                || op.getSootValue() instanceof LongConstant){
            return true;
        }

        if(op.getSootValue() instanceof ParameterRef){
            return true;
        }

        boolean onlyPorConst = true;

        if(op.getLeftOp() != null){
            if(!isOnlyParameterConstants(op.getLeftOp())){
                onlyPorConst = false;
            }
        }
        if(op.getRightOp() != null){
            if(!isOnlyParameterConstants(op.getRightOp())){
                onlyPorConst = false;
            }
        }

        if(op instanceof HistoryClassField
                && op.getLeftOp() == null
                && op.getRightOp() == null){

            //is a parent object by reference to a method parameter?
            HistoryClassField vf = (HistoryClassField)op;

            if(!(vf.getParentHSI().getSootValue() instanceof ParameterRef)){
                //parrent.son.age
                if(vf.getParentHSI().getLeftOp() != null){
                    if(!isOnlyParameterConstants(vf.getParentHSI().getLeftOp())){
                        onlyPorConst = false;
                    }
                }
                if(vf.getParentHSI().getRightOp() != null){
                    if(!isOnlyParameterConstants(vf.getParentHSI().getRightOp())){
                        onlyPorConst = false;
                    }
                }
            }
        } else if( op.getLeftOp() == null && op.getRightOp() == null) {
            if(op.getSootValue() instanceof FieldRef){

                FieldRef fieldRef = (FieldRef)op.getSootValue();

                if(!(fieldRef.getField().getDeclaringClass() != null && fieldRef.getField().getDeclaringClass().getSuperclass().getName().equals("java.lang.Enum"))){
                    onlyPorConst = false;
                }
            }


        }

        if(op.getSootValue() instanceof InvokeExpr && op.getLeftOp() == null && op.getRightOp() == null){
            onlyPorConst = false;
        }

        return onlyPorConst;
    }

}
