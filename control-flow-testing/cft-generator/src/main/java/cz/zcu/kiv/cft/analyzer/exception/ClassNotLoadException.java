package cz.zcu.kiv.cft.analyzer.exception;

/**
 *
 * Exception that is thrown when a problem occurs when loading a test class
 *
 * @author Jan Albl
 */
public class ClassNotLoadException extends RuntimeException {

    /**
     * Constructor to create an instance of this class.
     * *
     * @param message message exceptions
     * */
    public ClassNotLoadException(String message){
        super(message);
    }
}

