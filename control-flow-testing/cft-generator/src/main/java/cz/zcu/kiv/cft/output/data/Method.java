package cz.zcu.kiv.cft.output.data;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Data class representing the method analyzed
 *
 * @author Jan Albl
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Method {

    /** Method name*/
    private String name;

    /** count of parameters */
    private int parameterCount;

    /** Is static */
    private boolean isStatic;

    /** Is public */
    private boolean isPublic;

    /** Is constructor */
    private boolean isConstructor;

    /** return data type */
    private String returnType;

    /** Interaction strength of input parameters */
    private int interactionStrength;

    /** Control flow graph in dot format */
    private String graphDot;

    /** List of method input parameters */
    @XmlElementWrapper(name = "methodParameters")
    @XmlElement(name = "methodParameter")
    private List<Parameter> methodParameter = new ArrayList();

    /** List of branching methods */
    @XmlElementWrapper(name = "conditions")
    @XmlElement(name = "condition")
    private List<ConditionElement> conditions = new ArrayList();

    public List<ConditionElement> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionElement> conditions) {
        this.conditions = conditions;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getParameterCount() {
        return parameterCount;
    }

    public void setParameterCount(int parameterCount) {
        this.parameterCount = parameterCount;
    }

    public List<Parameter> getMethodParameter() {
        return methodParameter;
    }

    public String getGraphDot() {
        return graphDot;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public void setGraphDot(String graphDot) {
        this.graphDot = graphDot;
    }

    public void setMethodParameter(List<Parameter> methodParameter) {
        this.methodParameter = methodParameter;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public int getInteractionStrength() {
        return interactionStrength;
    }

    public void setInteractionStrength(int interactionStrength) {
        this.interactionStrength = interactionStrength;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isConstructor() {
        return isConstructor;
    }

    public void setConstructor(boolean constructor) {
        isConstructor = constructor;
    }
}
