package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.output.data.PrimitiveParameter;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public interface BooleanEvaluator extends IntervalCalculator {

    void addBooleanValuesToParameter(PrimitiveParameter primitiveParameter);
}
