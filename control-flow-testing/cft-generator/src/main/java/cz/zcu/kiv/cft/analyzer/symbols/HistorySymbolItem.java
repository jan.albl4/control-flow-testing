package cz.zcu.kiv.cft.analyzer.symbols;

import soot.Value;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Data class for symbol history in the symbol table
 *
 * @author Jan Albl
 */
public class HistorySymbolItem {

    /** Item expression */
    private Value sootValue;

    /** Next history item */
    private HistorySymbolItem next;

    /** Previous history item */
    private HistorySymbolItem previous;

    /** Left operator reference to next history item */
    private HistorySymbolItem leftOp;

    /** Right operator reference to next history item */
    private HistorySymbolItem rightOp;

    /** Field list if the item is an object */
    private List<HistoryClassField> fields = new ArrayList<>();

    /** If the method call entry has a list of input parameters */
    private List<HistorySymbolItem> parameters = new ArrayList<>();

    /** Called object */
    private HistorySymbolItem callObject;

    public HistorySymbolItem getCallObject() {
        return callObject;
    }

    public void setCallObject(HistorySymbolItem callObject) {
        this.callObject = callObject;
    }

    public Value getSootValue() {
        return sootValue;
    }

    public void setSootValue(Value sootValue) {
        this.sootValue = sootValue;
    }

    public HistorySymbolItem getNext() {
        return next;
    }

    public void setNext(HistorySymbolItem next) {
        this.next = next;
    }

    public HistorySymbolItem getPrevious() {
        return previous;
    }

    public void setPrevious(HistorySymbolItem previous) {
        this.previous = previous;
    }

    public HistorySymbolItem getLeftOp() {
        return leftOp;
    }

    public void setLeftOp(HistorySymbolItem leftOp) {
        this.leftOp = leftOp;
    }

    public HistorySymbolItem getRightOp() {
        return rightOp;
    }

    public void setRightOp(HistorySymbolItem rightOp) {
        this.rightOp = rightOp;
    }

    public List<HistoryClassField> getFields() {
        return fields;
    }

    public void setFields(List<HistoryClassField> fields) {
        this.fields = fields;
    }

    public List<HistorySymbolItem> getParameters() {
        return parameters;
    }

    public void setParameters(List<HistorySymbolItem> parameters) {
        this.parameters = parameters;
    }
}
