package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.tools.Utils;
import cz.zcu.kiv.cft.tools.log.Logger;
import guru.nidi.graphviz.engine.Graphviz;
import soot.*;
import soot.jimple.*;
import soot.toolkits.graph.*;

import java.io.File;
import java.util.*;
import java.util.List;


/**
 * Class representing control flow graph of input method
 *
 * @author Jan Albl
 */
public class ControlFlowGraph {

    /** Root UnitGraph **/
    private UnitGraph unitGraph;

    /** Map of all UnitGraph obtained from the analyzed method to the specified depth */
    private Map<Unit, Map<UnitGraph, UnitGraph>> unitGraphMap;

    /** Edges that create cycles in the flow control graph */
    private Map<Unit, Unit> cycleEdge;

    /** Logger {@link Logger} */
    private final Logger logger = Logger.getLogger();

    /** Node counter */
    private static int unitCounter;

    /**
     * Constructor to create an instance of this class.
     *
     * @param metod analyzed method
     * @param maxDepth maximum depth of search
     * **/
    public ControlFlowGraph(SootMethod metod, int maxDepth){
        logger.info("generate control flow graph of method: \"" + metod.getName() + "\" to maximum depth: \"" + maxDepth+"\"");

        this.unitGraphMap = new HashMap<>();
        this.unitGraph  = getUnitGraphMethod(metod);
        doAnalysisDepth(maxDepth, this.unitGraph);

        this.cycleEdge = CycleDetector.detectEdgesOfCycle(this);
    }

    /**
     * From the input metod creates a control flow graph
     *
     * @param metod analyzed soot method
     * @return control flow graph represented of class UnitGraph
     * **/
    private UnitGraph getUnitGraphMethod(SootMethod metod){
        logger.debug("generate control flow graph of method: \"" + metod.getName()+"\"");
        logger.debug("the body of method \""+metod.getName()+ "\" is \"" + metod.retrieveActiveBody()+"\"");
        return new ExceptionalUnitGraph(metod.retrieveActiveBody());
    }


    /**
     * Generates control flow graph to a certain depth.
     *
     * @param maxDepth maximum depth of search
     * @param unitGraph analyzed UnitGraph
    * **/
    private void doAnalysisDepth(int maxDepth, UnitGraph unitGraph){

        if(maxDepth != 0){
            int nextDeep = --maxDepth;

            Map<Unit, UnitGraph> pom = this.getAllUnitGrapFromGrap(unitGraph);

            logger.debug("found "+pom.size()+" called methods at depth " + maxDepth);

            for (Map.Entry<Unit, UnitGraph> entry : pom.entrySet()) {

                if(unitGraphMap.containsKey(entry.getKey())){
                    unitGraphMap.get(entry.getKey()).put(unitGraph, entry.getValue());
                }else{
                    Map<UnitGraph, UnitGraph> unitGraphMap = new HashMap<>();
                    unitGraphMap.put(unitGraph, entry.getValue());
                    this.unitGraphMap.put(entry.getKey(), unitGraphMap);
                }

                doAnalysisDepth(nextDeep, entry.getValue());
            }
        }
    }

    /**
     * Obtains additional control flow graphs of called methods from the input control flow graph
     * @param unitGraph input control flow graph
     * @return the map where the key is the unit node calling the next method and the value is control flow graph of the called method
     * */
    private Map<Unit, UnitGraph> getAllUnitGrapFromGrap(UnitGraph unitGraph){
        if (unitGraph != null) {
            Map<Unit, UnitGraph> unitGrapMap = new HashMap<>();

            Iterator i = unitGraph.iterator();
            Unit startingNode = (Unit)i.next();

            List<Unit> result = new ArrayList<Unit>();
            Set<Unit> closed = new HashSet<Unit>();
            Stack<Unit> open = new Stack<Unit>();

            open.push(startingNode);

            while (!open.isEmpty()) {
                Unit active = open.pop();

                if (!closed.contains(active)) {
                    result.add(active);
                    closed.add(active);

                    open.addAll(unitGraph.getSuccsOf(active));

                    if (active instanceof DefinitionStmt) {
                        DefinitionStmt assign = (DefinitionStmt) active;
                        Value right = assign.getRightOp();

                        SootMethod sootMethod = null;

                        if (right instanceof VirtualInvokeExpr){

                            if(!(Utils.isGetField((VirtualInvokeExpr) right))){
                                sootMethod = ((VirtualInvokeExpr) right).getMethod();
                            }

                        } else if(right instanceof StaticInvokeExpr){
                            sootMethod = ((StaticInvokeExpr) right).getMethod();
                        } else if(right instanceof SpecialInvokeExpr){
                            sootMethod = ((SpecialInvokeExpr) right).getMethod();
                        }

                        if(sootMethod != null && !sootMethod.isPhantom() && !sootMethod.isNative() && sootMethod.retrieveActiveBody() != null){
                            unitGrapMap.put(active, getUnitGraphMethod(sootMethod));
                        }
                    }else if(active instanceof InvokeStmt){
                        InvokeStmt invokeStmt = (InvokeStmt)active;

                        SootMethod sootMethod = invokeStmt.getInvokeExpr().getMethod();

                        if(sootMethod != null && !sootMethod.isPhantom() && !sootMethod.isNative() && sootMethod.retrieveActiveBody() != null){
                            unitGrapMap.put(active, getUnitGraphMethod(sootMethod));
                        }
                    }
                }
            }

            return unitGrapMap;
        }

        return null;
    }

    /**
     * From the control flow graph it creates a DOT format, which then returns
     *
     * @return DOT format of control flow graph stored in string
     * */
    public String getDot(){
        unitCounter = 0;
        return getDot(this.unitGraph, null);
    }

    /**
     * From the control flow graph it creates a DOT format, which then returns
     *
     * @param unitGraph control flow graph
     * @param connectionNode the connection node between the called graphs
     * @return DOT format of control flow graph stored in string
     * */
     private String getDot(UnitGraph unitGraph, String connectionNode){
        Iterator i = unitGraph.iterator();
        Map<Unit, Integer> mapUnit = new HashMap<Unit,Integer>();

        String output = "";

        if(connectionNode == null){
            output = "digraph \"callgraph\" {\n";
        }

        Unit startingNode = (Unit)i.next();

        Set<Unit> closed = new HashSet<Unit>();
        Stack<Unit> open = new Stack<Unit>();

        //init
        open.push(startingNode);

        while (!open.isEmpty()) {
            // get fist from top
            Unit active = open.pop();

            UnitPrinter unitPrint1 = new NormalUnitPrinter(unitGraph.getBody());
            unitPrint1.setIndent("");
            active.toString(unitPrint1);

            if (!closed.contains(active)) {

                if (!mapUnit.containsKey(active)) {
                    mapUnit.put(active, unitCounter++);
                    output += "\t\"" + mapUnit.get(active)+": "+String.valueOf(unitPrint1.output()) + "\"\n";

                    if (connectionNode != null){
                        output += "\t\"" + connectionNode + "\"->\"" + mapUnit.get(active)+": "+String.valueOf(unitPrint1.output()) + "\";\n";
                    } else {
                        output += "\t\"" + "entry" + "\"->\"" + mapUnit.get(active)+": "+String.valueOf(unitPrint1.output()) + "\";\n";
                    }
                }

                // visited
                closed.add(active);

                List<Unit> neighbors = unitGraph.getSuccsOf(active);
                String first = mapUnit.get(active)+": "+String.valueOf(unitPrint1.output());

                if (connectionNode != null && (neighbors == null  || neighbors.size() == 0)) {
                    output += "\t\"" + first + "\"->\"" + connectionNode + "\";\n";
                } else if(neighbors == null  || neighbors.size() == 0){
                    output += "\t\"" + first + "\"->\"exit\";\n";
                }

                for (Unit u: neighbors) {

                    UnitPrinter unitPrint2 = new NormalUnitPrinter(unitGraph.getBody());
                    unitPrint2.setIndent("");
                    u.toString(unitPrint2);

                    if(!mapUnit.containsKey(u)){
                        mapUnit.put(u, unitCounter++);
                        output += "\t\"" + mapUnit.get(u)+": "+String.valueOf(unitPrint2.output()) + "\"\n";
                    }

                    String second = mapUnit.get(u)+": "+String.valueOf(unitPrint2.output());

                    output += "\t\"" + first + "\"->\"" + second + "\";\n";
                }

                if(unitGraphMap.containsKey(active)
                        && unitGraphMap.get(active).containsKey(unitGraph)){
                    output += getDot(unitGraphMap.get(active).get(unitGraph), first);
                }

                open.addAll(neighbors);
            }
        }

        if(connectionNode == null){
            output+= "}";
        }

        return output;
    }

    /**
     * Saves the control flow chart to the image, which it then saves to disk.
     * @param filename output file name
     * */
    public void saveGraphGraphviz(String filename){
        try{

            if(filename != null){

                if(!filename.contains(".png")){
                    filename += ".png";
                }

                Graphviz output = Graphviz.fromString(this.getDot());
                output.renderToFile(new File(filename), "png", 1000, 1000);
            }
        }catch (Exception e){
            logger.error("control flow graph failed to save to file");
        }
    }

    /**
     * Returns the branch end node for the node (immediate post dominator)
     *
     * @param u the node for which the immediate postdominator is being searched
     * @param unitGraph control flow graph
     * @return returns the branch end node for the node (immediate post dominator)
     * */
    public Unit endConditionNodes(Unit u, UnitGraph unitGraph){

        while (u != null) {
            if (u instanceof IfStmt || u instanceof TableSwitchStmt || u instanceof LookupSwitchStmt) {
                if(!cycleEdge.containsKey(u)){
                    return AnalyzerImmediatePostDominator.getImmediatePostDominatorFor(unitGraph, u);
                }
            }
            if (unitGraph.getSuccsOf(u) != null && unitGraph.getSuccsOf(u).size() > 0) {
                u = unitGraph.getSuccsOf(u).get(0);
            } else {
                u = null;
            }
        }

        return null;
    }

    public UnitGraph getUnitGraph() {
        return unitGraph;
    }

    public Map<Unit, Map<UnitGraph, UnitGraph>> getUnitGraphMap() {
        return unitGraphMap;
    }

    public Map<Unit, Unit> getCycleEdge() {
        return cycleEdge;
    }
}
