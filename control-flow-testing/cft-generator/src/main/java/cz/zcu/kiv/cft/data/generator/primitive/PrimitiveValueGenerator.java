package cz.zcu.kiv.cft.data.generator.primitive;

import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.output.data.Method;
import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;

import java.util.Map;

/**
 *
 * Interface that is used to generate values for primitive data symbols
 *
 * @author Jan Albl
 */
public interface PrimitiveValueGenerator {

   /**
    * If primitive data types are used in the condition, they
    * try to generate test data, which they then store in the second parameter
    * @param condition condition
    * @param primitiveVariable the generated data will be stored here
    * */
   void calculateInterval(ConditionalExpression condition, Map<String, PrimitiveVariable> primitiveVariable);

   /**
    * Saves the generated test data to the input method
    * @param method method
    * @param primitiveVariable generated values
    * */
   void fillPrimitiveParameterVariable(Method method, Map<String, PrimitiveVariable> primitiveVariable);

   /**
    * Adds random values to non-evaluated input method primitive parameters
    * @param method data structure to which the generated random values will be stored
    * */
   void randomValuesToEmptySymbols(Method method);
}
