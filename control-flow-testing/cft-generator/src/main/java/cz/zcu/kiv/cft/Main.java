package cz.zcu.kiv.cft;

import cz.zcu.kiv.cft.tools.log.Logger;
import org.apache.commons.cli.*;

import java.util.Arrays;
import java.util.List;

/**
 *
 * Control the library from the command line
 *
 * @author Jan Albl
 */
public class Main {

    private static Options getOptions(){
        Options options = new Options();

        Option option = new Option("a", "analyze", true, "Type analysis. Values can be [f, c, m]. The value of \"f\" analyzes the entire application that is in the projectPath argument. The \"c\" parameter parses the entire class in the projectPath with the name in className. The last value \"m\" analyzing a specific method of a specific class.");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("pfp", "projectFolderPath", true, "Path to the project folder to be analyzed. Other subfolders are treated as individual packages.");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("op", "outputPath", true, "Folder path where test classes will be generated.");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("pp", "packagePath", true, "Class packages that will be analyzed. Each package value is classically separated by dots, for example: some.package.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("c", "className", true, "The name of the tested class.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("m", "methodName", true, "The name of the test method.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("t", "type", true, "Type of analysis used. This parameter has two possible values [d, f]. The value \"d\" is for the detailed and \"f\" for the fast.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("d", "depth", true, "Maximum depth of plunging into called methods.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("ml", "maxLines", true, "Maximum size of generated test class in rows.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("mrt", "methodreturnType", true, "Specified return method of the test method. For example, int, double, object, and so on.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("mpt", "parameters", true, "Specified types of input parameters of the test method separated by a dash. For example, int-doule means that the test method has two parameters that have int and double data types.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("l", "loadNecessaryClasses", false, "Load the set of classes that soot needs, including those specified on the command-line.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("pm", "privateMethod", false, "If the parameter is present in the command line, private methods will be analyzed too.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("da", "dataAnalysis", false, "If the parameter is present in the command line, the library will only generate data files.");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("debug", "debug", false, "Turns on debug mod.");
        option.setRequired(false);
        options.addOption(option);

        return options;
    }

    private static Configuration createConfiguration(CommandLine cmd){

        Configuration.Builder cb = new Configuration.Builder();

        String projectPath = cmd.getOptionValue("projectFolderPath");
        String analyze = cmd.getOptionValue("analyze");
        String outputPath = cmd.getOptionValue("outputPath");
        String type = cmd.getOptionValue("type");

        cb.setProjectPath(projectPath);
        cb.setOutputPath(outputPath);

        if(analyze.equals("f")){
            cb.setAnalyzedType(AnalyzedType.FOLDER);
        }else if(analyze.equals("c")){
            cb.setAnalyzedType(AnalyzedType.CLASS);
        }else if(analyze.equals("m")){
            cb.setAnalyzedType(AnalyzedType.METHOD);
        }else{
            System.out.println("The type of analysis can be just - m, c or f");
            System.exit(1);
        }

        cb.setPackagePath(cmd.getOptionValue("packagePath"));
        cb.setClassName(cmd.getOptionValue("className"));
        cb.setMethodName(cmd.getOptionValue("methodName"));
        cb.setReturnType(cmd.getOptionValue("methodreturnType"));

        if (type != null && type.equals("d")) {
            cb.setDetailedAnalysis(true);
        } else {
            cb.setDetailedAnalysis(false);
        }

        try {
            if(cmd.getOptionValue("depth") != null){
                int depth = Integer.valueOf(cmd.getOptionValue("depth"));
                cb.setMaxDepth(depth);
            }
        } catch (Exception e){
            System.out.println("Wrong argument for attribute - depth");
        }

        try {
            if(cmd.getOptionValue("maxLines") != null){
                int maxLines = Integer.valueOf(cmd.getOptionValue("maxLines"));
                cb.setMaximumLines(maxLines);
            }
        } catch (Exception e){
            System.out.println("Wrong argument for attribute - maxLines");
        }

        if (cmd.hasOption("debug")) {
            Logger.setDebug(true);
        } else {
            Logger.setDebug(false);
        }

        if (cmd.hasOption("loadNecessaryClasses")) {
            cb.setLoadNecessaryClasses(true);
        } else {
            cb.setLoadNecessaryClasses(false);
        }

        if (cmd.hasOption("dataAnalysis")) {
            cb.setDataAnalysis(true);
        } else {
            cb.setDataAnalysis(false);
        }

        if (cmd.hasOption("privateMethod")) {
            cb.setAnalyzePrivateMethod(true);
        } else {
            cb.setAnalyzePrivateMethod(false);
        }

        if(cmd.getOptionValue("parameters") != null){
            String[] parameters = cmd.getOptionValue("parameters").split("-");

            if(parameters.length != 0){
                List<String> pl = Arrays.asList(parameters);
                cb.setParameters(pl);
            }
        }

        return cb.build();
    }

    public static void main(String[] args) {

        Options options = getOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }

        Configuration configuration = createConfiguration(cmd);

        UTGCFG UTGCFG = new UTGCFG.Builder().setConfiguration(configuration).build();

        UTGCFG.run();
    }
}
