package cz.zcu.kiv.cft.tools.statistics;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class Statistics {

    //počet analyzovanych tříd
    private static int countAnalyzedClasses = 0;
    private static int countAnalyzedMethod = 0;


    public static void clear(){
        countAnalyzedClasses = 0;
        countAnalyzedMethod = 0;
    }

    public static void addAnalyzedClass(){
        countAnalyzedClasses++;
    }

    public static void addAnalyzedMethod(){
        countAnalyzedClasses++;
    }

    public static void printStatistics(){
        System.out.println();
        System.out.println("=================================");
        System.out.println("=================================");

        System.out.println("count of analyzed classes: " + countAnalyzedClasses);
        System.out.println("count of analyzed methods: " + countAnalyzedMethod);

        System.out.println("=================================");
        System.out.println("=================================");
    }
}
