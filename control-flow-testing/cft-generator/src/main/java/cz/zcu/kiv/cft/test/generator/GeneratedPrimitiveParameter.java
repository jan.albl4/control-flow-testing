package cz.zcu.kiv.cft.test.generator;

import cz.zcu.kiv.cft.output.data.PrimitiveParameter;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * Class generating bodies primitive parameters
 *
 * @author Jan Albl
 */
public class GeneratedPrimitiveParameter extends GeneratedParameter {

    public GeneratedPrimitiveParameter(PrimitiveParameter primitiveParameter){
        this.setName(primitiveParameter.getName());

        createBodys(primitiveParameter);
    }

    private void createBodys(PrimitiveParameter p){

        List<String> out = new ArrayList<>();

        for(String value : p.getValues()){
            String bodyForPrimitive = "";

            bodyForPrimitive += p.getDataType() + " ";
            bodyForPrimitive += p.getName();
            bodyForPrimitive += " = " + value + ";\n";


            out.add(bodyForPrimitive);
        }

        this.setBody(out);
    }
}
