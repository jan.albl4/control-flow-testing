package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.tools.Utils;
import soot.Unit;
import soot.toolkits.graph.UnitGraph;

import java.util.*;

/**
 * Class for cycling analysis in control flow graph {@link ControlFlowGraph}
 *
 * @author Jan Albl
 */
public class CycleDetector {

    /**
     * Detects edges leading to cycles in the control flow graph
     *
     * @param cfg control flow graph
     * @return edges leading to cycle
     * */
    public static Map<Unit,Unit> detectEdgesOfCycle(ControlFlowGraph cfg){
        Map<Unit,Unit> cycleEdge = new HashMap<Unit, Unit>();

        if( cfg.getUnitGraphMap() != null && cfg.getUnitGraphMap().size() > 0){
            for (Map.Entry<Unit, Map<UnitGraph, UnitGraph>> entry : cfg.getUnitGraphMap().entrySet()) {
                for (Map.Entry<UnitGraph, UnitGraph> entryChild : entry.getValue().entrySet()) {
                    detectEdgesOfCycle(entryChild.getValue(), cycleEdge);
                }
            }
        }

        detectEdgesOfCycle(cfg.getUnitGraph(), cycleEdge);

        return cycleEdge;
    }

    /**
     * Detects edges leading to cycles in the UnitGraph
     *
     * @param unitGraph part of control flow graph
     * @param cycleEdge edges leading to cycle
     * */
    private static void detectEdgesOfCycle(UnitGraph unitGraph,  Map<Unit,Unit> cycleEdge){
        Iterator i = unitGraph.iterator();
        Unit u = (Unit)i.next();

        List<Unit> whiteList = Utils.DFS(unitGraph, u);
        Set<Unit> graySet = new HashSet<Unit>();
        Set<Unit> blackSet = new HashSet<Unit>();

        while (whiteList.size() > 0){

            Unit current = whiteList.iterator().next();

            if(current != null){
                dfs(u, whiteList, graySet, blackSet, unitGraph, cycleEdge);
            }
        }
    }

    /**
     * The depth-first search algorithm used to locate cycles in a graph
     * @param current input node
     * @param whiteList open nodes
     * @param graySet active node
     * @param blackSet close node
     * @param cfg control flow graph
     * @param cycleEdge edges leading to cycles in graph
     * */
    private static void dfs(Unit current, List<Unit> whiteList, Set<Unit> graySet, Set<Unit> blackSet, UnitGraph cfg,  Map<Unit,Unit> cycleEdge) {

        if (current != null) {
            whiteList.remove(current);
            graySet.add(current);

            for (Unit neighbor : cfg.getSuccsOf(current)){

                if(blackSet.contains(neighbor)){
                    continue;
                }

                if (graySet.contains(neighbor)) {
                    cycleEdge.put(current, neighbor);
                    continue;
                }

                dfs(neighbor, whiteList, graySet, blackSet, cfg, cycleEdge);
            }
            graySet.remove(current);
            blackSet.add(current);
        }
    }
}
