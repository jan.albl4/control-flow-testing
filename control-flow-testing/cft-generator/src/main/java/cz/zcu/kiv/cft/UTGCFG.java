package cz.zcu.kiv.cft;


import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.analyzer.symbols.QuickAnalyzerSymbolTables;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import cz.zcu.kiv.cft.output.OutputConverter;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.test.generator.TestGenerator;
import cz.zcu.kiv.cft.test.generator.TestGeneratorImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Unit test generator based on flow control graph
 *
 * @author Jan Albl
 */
public class UTGCFG {

    /** Analyzer control flow graph  */
    private Analyzer analyzer;

    /** Data generator */
    private DataGenerator dataGenerator;

    /** Test generator */
    private TestGenerator testGenerator;

    /** Configuration used */
    private Configuration configuration;

    /**
     * Constructor to create an instance of this class.
     * **/
    private UTGCFG(Analyzer analyzer, DataGenerator dataGenerator, TestGenerator testGenerator, Configuration configuration){
        this.analyzer = analyzer;
        this.dataGenerator = dataGenerator;
        this.testGenerator = testGenerator;
        this.configuration = configuration;

        if(analyzer == null){
            AnalyzerImpl.Builder aib = new AnalyzerImpl.Builder()
                    .setLoadNecessaryClasses(configuration.isLoadNecessaryClasses())
                    .setProjectPath(configuration.getProjectPath())
                    .setPackagePath(configuration.getPackagePath())
                    .setMaxDepth(configuration.getMaxDepth());

            if (configuration.isDetailedAnalysis()) {
                aib.setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables());
            } else {
                aib.setSymbolTableAnalyzer(new QuickAnalyzerSymbolTables());
            }

            this.analyzer = aib.build();
        }

        if(dataGenerator == null){
            this.dataGenerator = new DataGeneratorImpl();
        }

        if(testGenerator == null){
            this.testGenerator = new TestGeneratorImpl(configuration.isAnalyzePrivateMethod());
        }
    }

    /**
     * Launches test data generation based on configuration
     * */
    public ControlFlowAnalysisOutput run(){

        ControlFlowAnalysisOutput cf = null;

        AnalyzedType analyzedType = configuration.getAnalyzedType();

        if(analyzedType == AnalyzedType.METHOD){
            cf = analyzer.analyzeMethod(configuration.getClassName(), configuration.getMethodName(), configuration.getParameters(), configuration.getReturnType());
        }else if(analyzedType == AnalyzedType.CLASS){
            cf = analyzer.analyzeClass(configuration.getClassName());
        }else if(analyzedType == AnalyzedType.FOLDER){
            cf = analyzer.analyzeFolder(configuration.getProjectPath());
        }

        if(cf != null && cf.getClasses() != null && cf.getClasses().size() > 0){
            dataGenerator.generateData(cf);

            if(!configuration.isDataAnalysis()){
                if(configuration.getMaximumLines() > 0){
                    testGenerator.generateClassFor(cf,  configuration.getOutputPath(), configuration.getMaximumLines());
                } else {
                    testGenerator.generateClassFor(cf,  configuration.getOutputPath());
                }
            }else{
                OutputConverter.convertToJsonFile(cf, configuration.getOutputPath() + configuration.getClassName() + ".json");
                OutputConverter.convertToXmlFile(cf, configuration.getOutputPath() + configuration.getClassName() + ".xml");
            }
        }

        return cf;
    }

    public ControlFlowGraph getControlFlowGraph(){

        ControlFlowGraph cfg = null;

        AnalyzedType analyzedType = configuration.getAnalyzedType();

        if(analyzedType == AnalyzedType.METHOD){
            cfg = analyzer.getControlFlowGraph(configuration.getClassName(), configuration.getMethodName(), configuration.getParameters(), configuration.getReturnType());
        }

        return cfg;
    }

    public Map<String, List<String>> getDot(){
        Map<String, List<String>> dots = new HashMap<>();


        if(configuration.getAnalyzedType() == AnalyzedType.METHOD){

            String cfgDots = analyzer.getDots(configuration.getClassName(), configuration.getMethodName(), configuration.getParameters(), configuration.getReturnType());

            dots.put(configuration.getClassName(), new ArrayList<>());
            dots.get(configuration.getClassName()).add(cfgDots);

        } else if(configuration.getAnalyzedType() == AnalyzedType.CLASS){
            List<String> cfgDots = analyzer.getDots(configuration.getClassName());
            dots.put(configuration.getClassName(), cfgDots);
        } else if(configuration.getAnalyzedType() == AnalyzedType.FOLDER){
            return analyzer.getDost(configuration.getProjectPath());
        }

        return dots;
    }

    /**
     * Builder that creates new instance of {@link UTGCFG}
     *
     * @author Jan Albl
     */
    public static class Builder{

        private Analyzer analyzer;
        private DataGenerator dataGenerator;
        private TestGenerator testGenerator;
        private Configuration configuration;

        public UTGCFG.Builder setConfiguration(Configuration configuration){
            this.configuration = configuration;
            return this;
        }

        public UTGCFG.Builder setAnalyzer(Analyzer analyzer) {
            this.analyzer = analyzer;
            return this;
        }

        public UTGCFG.Builder setDataGenerator(DataGenerator dataGenerator) {
            this.dataGenerator = dataGenerator;
            return this;
        }

        public UTGCFG.Builder setTestGenerator(TestGenerator testGenerator) {
            this.testGenerator = testGenerator;
            return this;
        }

        public UTGCFG build(){

            if(this.configuration == null){
                //TODO
                //exception ze neni nastavena configurace
            }
            return new UTGCFG(analyzer, dataGenerator, testGenerator, configuration);
        }
    }

}
