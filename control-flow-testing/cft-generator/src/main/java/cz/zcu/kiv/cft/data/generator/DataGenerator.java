package cz.zcu.kiv.cft.data.generator;

import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;

/**
 * Interface that evaluates branching conditions and assigns values to parameter methods
 *
 * @author Jan Albl
 */
public interface DataGenerator {

    /**
     * Evaluates branching conditions and assigns values to parameter methods
     * @param controlFlowAnalysisOutput data structure storing information about the test method
     * */
    void generateData(ControlFlowAnalysisOutput controlFlowAnalysisOutput);
}
