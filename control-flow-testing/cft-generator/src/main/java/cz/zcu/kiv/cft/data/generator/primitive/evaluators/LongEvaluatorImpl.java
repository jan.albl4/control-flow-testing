package cz.zcu.kiv.cft.data.generator.primitive.evaluators;

import cz.zcu.kiv.cft.data.generator.data.PrimitiveVariable;
import cz.zcu.kiv.cft.data.generator.utils.DataUtil;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;
import org.apache.commons.lang.math.LongRange;
import org.apache.commons.lang.math.Range;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class LongEvaluatorImpl implements LongEvaluator {

    private final String suffix = "L";

    @Override
    public void addLongValuesToParameter(PrimitiveParameter primitiveParameter, Range range){
        if(primitiveParameter != null && range != null) {

            List<String> values = getValueFromRange(range);

            for(String v: values){
                primitiveParameter.getValues().add(v);
            }
        }
    }

    @Override
    public List<Range> calculateIntervalsFromLimitValues(PrimitiveVariable primitiveParameter){
        List<Range> ranges = new ArrayList<>();

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new LongRange(Long.MIN_VALUE, primitiveParameter.getBorderValue().get(0)));
        }

        for (int i = 0; i < primitiveParameter.getBorderValue().size()-1; i++) {
            ranges.add(new LongRange(primitiveParameter.getBorderValue().get(i), primitiveParameter.getBorderValue().get(1+i)));
        }

        if (primitiveParameter.getBorderValue().size()>0) {
            ranges.add(new LongRange(primitiveParameter.getBorderValue().get(primitiveParameter.getBorderValue().size()-1), Long.MAX_VALUE));
        }

        return ranges;
    }

    private List<String> getValueFromRange(Range range){
        List<String> values = new ArrayList<>();

        values.add(String.valueOf(range.getMinimumLong())+suffix);
        values.add(String.valueOf(range.getMaximumLong())+suffix);


        long min = 1; //aby se nestalo ze dostanu cislo k
        long max = range.getMaximumLong() - range.getMinimumLong() - 1;

        if (range.getMaximumLong() - range.getMinimumLong() < 0){
            //preteklo cislo je vetsi nez Integer.Maxint
            max = Long.MAX_VALUE - 1;
        }

        if (range.getMaximumLong() - range.getMinimumLong() != 0) {
            long between  =  (long)(Math.random() * (max - min) + min);
            values.add(String.valueOf(between + range.getMinimumLong())+suffix);
        }

        return values;
    }

    @Override
    public String getRandomValue(){
        return String.valueOf(DataUtil.getRandomNumberInRange(Short.MIN_VALUE, Short.MAX_VALUE))+suffix;
    }
}
