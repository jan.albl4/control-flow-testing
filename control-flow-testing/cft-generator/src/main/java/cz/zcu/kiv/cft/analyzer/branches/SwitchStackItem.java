package cz.zcu.kiv.cft.analyzer.branches;

import soot.Unit;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.TableSwitchStmt;
import soot.toolkits.graph.UnitGraph;

import java.util.List;

/**
 *
 * Class representing the branch node for the switch command
 *
 * @author Jan Albl
 */
public class SwitchStackItem extends StackItem {

    /**
     * Constructor to create an instance of this class.
     *
     * @param u the branch entry unit contains a condition
     * **/
    public SwitchStackItem(Unit u){
        this(u,null);
    }

    /**
     * Constructor to create an instance of this class.
     *
     * @param u the branch entry unit contains a condition
     * @param endNode end branch node
     * **/
    public SwitchStackItem(Unit u, Unit endNode){
        this.setUnit(u);

        if(u instanceof TableSwitchStmt){
            TableSwitchStmt tableSwitchStmt = (TableSwitchStmt)u;
            this.setUnprocessedBranchesCount(tableSwitchStmt.getTargets().size());
        } else if (u instanceof LookupSwitchStmt){
            LookupSwitchStmt lookupSwitchStmt = (LookupSwitchStmt)u;
            this.setUnprocessedBranchesCount(lookupSwitchStmt.getTargetCount());
        }

        this.setEndNode(endNode);
    }

    /**
     * Returns a node that is in the first order for further processing
     * @param unitGraph control flow graph
     * @return node that is in the first order for further processing
     * */
    public Unit getAnotherUnprocessedNode(UnitGraph unitGraph){

        List<Unit> neighbors = unitGraph.getSuccsOf(this.getUnit());

        Unit u = null;
        int ubc = this.getUnprocessedBranchesCount();

        if(ubc == 0){
            if (this.getUnit() instanceof TableSwitchStmt) {
                u = ((TableSwitchStmt)this.getUnit()).getDefaultTarget();
            }else if ( this.getUnit() instanceof LookupSwitchStmt) {
                u = ((LookupSwitchStmt)this.getUnit()).getDefaultTarget();
            }
        }else {
            if (this.getUnit() instanceof TableSwitchStmt) {
                u = ((TableSwitchStmt)this.getUnit()).getTarget(neighbors.size() - ubc -1);
            }else if (this.getUnit() instanceof LookupSwitchStmt) {
                u = ((LookupSwitchStmt)this.getUnit()).getTarget(neighbors.size() - ubc -1);
            }
        }

        return u;
    }
}