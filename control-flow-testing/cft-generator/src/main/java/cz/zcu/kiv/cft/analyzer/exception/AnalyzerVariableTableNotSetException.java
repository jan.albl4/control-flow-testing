package cz.zcu.kiv.cft.analyzer.exception;

/**
 *
 * Exception that is thrown when the analyzer for variable table is not set
 *
 * @author Jan Albl
 */
public class AnalyzerVariableTableNotSetException extends RuntimeException {

    /**
     * Constructor to create an instance of this class.
     *
     * @param message message exceptions
     * */
    public AnalyzerVariableTableNotSetException(String message){
        super(message);
    }
}