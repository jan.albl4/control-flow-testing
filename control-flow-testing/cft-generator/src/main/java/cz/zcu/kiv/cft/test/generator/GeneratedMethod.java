package cz.zcu.kiv.cft.test.generator;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the generated test method
 *
 * @author Jan Albl
 */
public class GeneratedMethod {

    /** name */
    private String name;

    /** index */
    private int index = 0;

    /** list of annotations */
    private List<Annotation> annotations = new ArrayList<>();

    /** body */
    private String body;

    public GeneratedMethod(String name, String body, int index){
        this.body = body;
        this.name = name;
        this.index = index;
        annotations.add(new Annotation("@Test"));
    }

    @Override
    public String toString() {

        String out = "";

        for(Annotation a : annotations){
            out += a.toString();
        }


        out += writeHead();
        out += body;
        out  += "}\n";

        return out;
    }

    private String writeHead(){
        return   "public void " + name + "Test" + (++index) + "(){\n";
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


}
