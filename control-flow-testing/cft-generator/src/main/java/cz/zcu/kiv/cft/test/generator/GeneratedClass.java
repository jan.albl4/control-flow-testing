package cz.zcu.kiv.cft.test.generator;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the generated test class
 *
 * @author Jan Albl
 */
public class GeneratedClass {

    /** package */
    private String classPackage;

    /** name */
    private String name;

    /** list of imports */
    private List<String> imports = new ArrayList<>();

    /** list of generated methods */
    private List<GeneratedMethod> methods = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public List<GeneratedMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<GeneratedMethod> methods) {
        this.methods = methods;
    }

    public String getClassPackage() {
        return classPackage;
    }

    public void setClassPackage(String classPackage) {
        this.classPackage = classPackage;
    }

    @Override
    public String toString() {

        String output = "";

        if(classPackage != null){
            output += "package " + classPackage + ";\n";
        }


        for (String imp : imports) {
            output += "import " + imp + ";\n";
        }


        output  += "\n public class "+name +"Test{\n\n";

        for (GeneratedMethod genm : methods) {
            output += genm.toString();
        }

        output  += "\n}\n";

        return output;
    }
}
