package cz.zcu.kiv.cft.tools.log;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * Own logger library
 * @author Jan Albl
 */
public class Logger {

    private static boolean debug = false;

    private String name = "";

    private static final Logger LOG = Logger.getLogger();

    private Logger(String name){
        this.name = name;
    }

    public static void setDebug(boolean d){
        debug = d;
    }

    public static Logger getLogger()
    {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        // stacktrace[0] is getStackTrace()
        // stacktrace[1] is getLogger()
        StackTraceElement element = stacktrace[2];
        String name = element.getClassName();

        String[] packageItems = name.split("\\.");

        if(packageItems.length > 0){
            return new Logger(packageItems[packageItems.length-1]);
        }
        return new Logger(name);

    }


    /**
     * Logs a message if DEBUG logging is enabled.
     *
     * @param message a message
     */
    public final void debug(String message) {
        if(debug){
            print(LoggerLevel.DEBUG, message);
        }
    }


    /**
     * Logs a message if INFO logging is enabled.
     *
     * @param message a message
     */
    public final void info(String message)
    {
        print(LoggerLevel.INFO, message);
    }


    /**
     * Logs a message if WARN logging is enabled.
     *
     * @param message a message
     */
    public final void warn(String message) {
        print(LoggerLevel.WARN, message);

    }


    /**
     * Logs a message if ERROR logging is enabled.
     *
     * @param message a message
     */
    public final void error(String message)
    {
        print(LoggerLevel.ERROR,  message);
    }

    private void print(LoggerLevel level, String message){

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
        Date date = new Date(System.currentTimeMillis());

        System.out.println(formatter.format(date) + level  + " " + this.name +": "+ message);
    }
}
