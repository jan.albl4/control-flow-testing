package cz.zcu.kiv.cft.analyzer.exception;

/**
 *
 * Exception that is thrown when the path to the tested project is not set
 *
 * @author Jan Albl
 */
public class ProjectPathNotSetException extends RuntimeException {

    /**
     * Constructor to create an instance of this class.
     *
     * @param message message exceptions
     * */
    public ProjectPathNotSetException(String message){
        super(message);
    }
}