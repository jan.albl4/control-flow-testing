package cz.zcu.kiv.cft.data.generator.data;

import soot.Type;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class for
 *
 * @author Jan Albl
 */
public class ObjectVariable extends BaseVariable {

    /**  */
    private boolean isNullable;

    /**  */
    private List<Type> parrentType;

    /**  */
    private List<Type> dataType;

    public ObjectVariable(){
        this.parrentType = new ArrayList<>();
        this.dataType = new ArrayList<>();
        this.isNullable = false;
    }

    public List<Type> getParrentType() {
        return parrentType;
    }

    public void setParrentType(List<Type> parrentType) {
        this.parrentType = parrentType;
    }

    public List<Type> getDataType() {
        return dataType;
    }

    public void setDataType(List<Type> dataType) {
        this.dataType = dataType;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public void setNullable(boolean nullable) {
        isNullable = nullable;
    }
}
