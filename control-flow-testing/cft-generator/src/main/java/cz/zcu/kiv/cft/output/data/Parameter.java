package cz.zcu.kiv.cft.output.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import soot.Type;

import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Data class representing the method parameter
 *
 * @author Jan Albl
 */
public abstract class Parameter {

    /** Parameter index */
    private int index;

    /** Parameter name */
    private String name;

    /** Data type */
    @JsonIgnore
    @XmlTransient
    private Type sootType;

    /** Flag whether the parameter is primitive */
    private boolean isPrimitive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        if(sootType != null){
            return sootType.toString();
        }
        return "";
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

    public void setPrimitive(boolean primitive) {
        isPrimitive = primitive;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @XmlTransient
    public Type getSootType() {
        return sootType;
    }

    public void setSootType(Type sootType) {
        this.sootType = sootType;
    }
}
