package cz.zcu.kiv.cft.analyzer.evaluator;

import cz.zcu.kiv.cft.analyzer.branches.*;
import cz.zcu.kiv.cft.analyzer.loader.SootClassLoader;
import cz.zcu.kiv.cft.output.data.Condition;
import cz.zcu.kiv.cft.output.data.ConditionalExpression;
import cz.zcu.kiv.cft.output.data.SwitchConditions;
import cz.zcu.kiv.cft.analyzer.symbols.*;
import soot.*;
import soot.jimple.*;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * It evaluates the expression in the condition and if it depends only on the input parameters it develops
 * it into the basic form according to the symbol tables
 *
 * @author Jan Albl
 */
public class ConditionEvaluatorImpl {

    /** {@link SootClassLoader} for loading classes in a tested project */
    private static SootClassLoader sootClassLoader;

    /** A note that is added after an expression that cannot be evaluated */
    private static String cannotBeEvaluatedNote = " -> parameter independent or cannot be evaluated";

    /**
     * Expresses the expression in Value into the base form according to the symbol table.
     * Returns the result and stores the symbol data types in the {@link ConditionalExpression} data structure
     *
     * @param op the expression to be developed
     * @param condition here the symbol data types are saved
     * @param sts {@link SymbolTableStack}
     * @return the result of developing the expression
     * */
    private static String parameterExpression(Value op, ConditionalExpression condition, SymbolTableStack sts){
        List<SymbolItem> symbolItems = sts.pop().getCurrentSymbolTable();

        String out = "";

        if(op instanceof IntConstant || op instanceof DoubleConstant || op instanceof FloatConstant || op instanceof LongConstant || op instanceof NullConstant){
            return op.toString();
        }

        for(int i = 0; i < symbolItems.size(); i++){
            if(symbolItems.get(i).getSootValue().equals(op)){

                if(symbolItems.get(i).getHistory().getNext() == null && symbolItems.get(i).getHistory().getSootValue() instanceof ParameterRef){

                    addVariableItemToArgTypeMap(symbolItems.get(i), condition);

                    return symbolItems.get(i).getName();
                }

                ConditionDeveloper conditionDeveloper = new ConditionDeveloper(sootClassLoader);

                return conditionDeveloper.parameterExpression(symbolItems.get(i).getHistory(), condition);
            }
        }
        return out;
    }

    /**
     * Stores the symbol data type in the {@link ConditionalExpression} data structure
     * @param symbolItem symbol
     * @param condition here the symbol data types are saved
     * */
    private static void addVariableItemToArgTypeMap(SymbolItem symbolItem, ConditionalExpression condition){
        if(!condition.getArgTypeMap().containsKey(symbolItem.getName())){
            condition.getArgTypeMap().put(symbolItem.getName(), new ArrayList<>());
        }

        condition.getArgTypeMap().get(symbolItem.getName()).add(symbolItem.getType());
    }

    /**
     * Determines if the condition depends only on input parameters.
     * If so, the expression in the condition unfolds to a basic form.
     * @param branchStackItem branch node
     * @param condition to which the information will be stored
     * */
    private static void evaluateCondition(BranchStackItem branchStackItem, Condition condition){

        for(SymbolTableStack symbolTableStack : branchStackItem.getAllVariableTables()){
            ConditionalExpression conditionalExpression = new ConditionalExpression();
            boolean isInputDep = evaluateConditionalExpression(branchStackItem, conditionalExpression, symbolTableStack);
            conditionalExpression.setInputDependent(isInputDep);
            condition.addConditionalExpression(conditionalExpression);
        }
    }

    /**
     * Determines if the condition depends only on input parameters.
     * If so, the expression in the condition unfolds to a basic form and return true
     * @param branchStackItem branch node
     * @param condition to which the information will be stored
     * @param sts the symbol table by which the expression will be evaluated
     * @return true is condition dependent only on input parameters
     * */
    private static boolean evaluateConditionalExpression(BranchStackItem branchStackItem, ConditionalExpression condition, SymbolTableStack sts) {
        Value op1 = ((ConditionExpr)((IfStmt)branchStackItem.getUnit()).getCondition()).getOp1();
        Value op2 = ((ConditionExpr)((IfStmt)branchStackItem.getUnit()).getCondition()).getOp2();

        int indexVT = sts.getSymbolTableStack().size()-1;
        boolean isInputDependent = false;

        ParameterReferencer parameterReferencer = new ParameterReferencer(sts);

        String exp;

        if (parameterReferencer.canBeEvaluated(branchStackItem, indexVT)) {
            isInputDependent = true;
            exp = parameterExpression(op1, condition, sts) + ((ConditionExpr)((IfStmt)branchStackItem.getUnit()).getCondition()).getSymbol() + parameterExpression(op2, condition, sts);
        } else {
            exp =  op1 + ((ConditionExpr)((IfStmt)branchStackItem.getUnit()).getCondition()).getSymbol() + op2 + cannotBeEvaluatedNote;
        }

        exp = exp.trim().replace("  ", " ");

        condition.setExpression(exp);

        return isInputDependent;
    }

    /**
     * Determines if the condition depends only on input parameters.
     * If so, the expression in the condition unfolds to a basic form.
     * @param branchStackItem branch node
     * @param condition to which the information will be stored
     * */
    private static void evaluateBranch(BranchStackItem branchStackItem, Condition condition){
        evaluateCondition(branchStackItem, condition);
        condition.setUnit(branchStackItem.getUnit());
    }

    /**
     * Determines if the switch items depends only on input parameters.
     * If so, the expression in the condition unfolds to a basic form.
     * @param switchStackItem branch node
     * @param switchCondition to which the information will be stored
     * */
    private static void evaluateSwitchLookup(StackItem switchStackItem, SwitchConditions switchCondition){
        if(switchStackItem instanceof SwitchStackItem) {

            SymbolTableStack sts = switchStackItem.getSymbolTableStack();

            ParameterReferencer pr = new ParameterReferencer(switchStackItem.getSymbolTableStack());

            if (switchStackItem.getUnit() instanceof TableSwitchStmt){

                TableSwitchStmt tableSwitchStmt = (TableSwitchStmt) switchStackItem.getUnit();

                List<Condition> conditions = new ArrayList<Condition>();

                if (pr.isParameterRef(tableSwitchStmt.getKey(), sts.getSymbolTableStack().size()-1)){
                    for(int i = tableSwitchStmt.getLowIndex(); i <= tableSwitchStmt.getHighIndex(); i++){
                        Condition cond = new Condition();
                        ConditionalExpression ce = new ConditionalExpression();
                        ce.setExpression(parameterExpression(tableSwitchStmt.getKey(), ce, sts) + " == " + i);
                        ce.setInputDependent(true);
                        cond.addConditionalExpression(ce);
                        cond.setParentCondition(switchCondition);
                        cond.setUnit(switchStackItem.getUnit());
                        conditions.add(cond);
                        switchCondition.getBranches().add(cond);
                    }
                } else {
                    for(int i = tableSwitchStmt.getLowIndex(); i <= tableSwitchStmt.getHighIndex(); i++){
                        Condition cond = new Condition();
                        ConditionalExpression ce = new ConditionalExpression();
                        ce.setExpression(tableSwitchStmt.getKey() + " == " + i + cannotBeEvaluatedNote);
                        cond.addConditionalExpression(ce);
                        cond.setParentCondition(switchCondition);
                        cond.setUnit(switchStackItem.getUnit());
                        switchCondition.getBranches().add(cond);
                    }
                }

            }else if (switchStackItem.getUnit() instanceof LookupSwitchStmt) {
                LookupSwitchStmt lookupSwitchStmt = (LookupSwitchStmt) switchStackItem.getUnit();

                List<Condition> conditions = new ArrayList<Condition>();

                if (pr.isParameterRef(lookupSwitchStmt.getKey(), sts.getSymbolTableStack().size()-1)){
                    for(int i = 0; i < lookupSwitchStmt.getTargetCount(); i++){
                        Condition cond = new Condition();
                        ConditionalExpression ce = new ConditionalExpression();
                        ce.setExpression(parameterExpression(lookupSwitchStmt.getKey(), ce, sts) + " == " + lookupSwitchStmt.getLookupValue(i));
                        cond.addConditionalExpression(ce);
                        ce.setInputDependent(true);
                        cond.setParentCondition(switchCondition);
                        cond.setUnit(switchStackItem.getUnit());
                        conditions.add(cond);
                        switchCondition.getBranches().add(cond);
                    }
                } else {
                    for(int i = 0; i < lookupSwitchStmt.getTargetCount(); i++){
                        Condition cond = new Condition();
                        ConditionalExpression ce = new ConditionalExpression();
                        ce.setExpression(lookupSwitchStmt.getKey() + " == " + lookupSwitchStmt.getLookupValue(i) + cannotBeEvaluatedNote);
                        cond.addConditionalExpression(ce);
                        cond.setParentCondition(switchCondition);
                        cond.setUnit(switchStackItem.getUnit());
                        switchCondition.getBranches().add(cond);
                    }
                }
            }
        }
    }

    /**
     * Determines if the condition depends only on input parameters.
     * If so, the expression in the condition unfolds to a basic form.
     * @param stackItem branch node
     * @param condition to which the information will be stored
     * */
    public static void evaluate(StackItem stackItem, Condition condition){
        if(stackItem instanceof BranchStackItem){
            evaluateBranch((BranchStackItem) stackItem, condition);
        }
    }

    /**
     * Determines if switch cases depend only on input parameters.
     * If so, the expression in the condition unfolds to a basic form.
     * @param stackItem branch node
     * @param condition to which the information will be stored
     * */
    public static void evaluate(StackItem stackItem, SwitchConditions condition){
        if (stackItem instanceof SwitchStackItem) {
            SwitchStackItem switchStackItem = (SwitchStackItem) stackItem;

            if (switchStackItem.getUnit() instanceof TableSwitchStmt || switchStackItem.getUnit() instanceof LookupSwitchStmt) {
                evaluateSwitchLookup(stackItem, condition);
            }
        }
    }


    public static void setSootClassLoader(SootClassLoader sootClassLoader) {
        ConditionEvaluatorImpl.sootClassLoader = sootClassLoader;
    }
}
