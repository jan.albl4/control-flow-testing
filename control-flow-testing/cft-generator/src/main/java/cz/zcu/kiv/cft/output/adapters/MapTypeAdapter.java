package cz.zcu.kiv.cft.output.adapters;

import soot.Type;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Mapper to convert output Type to XML
 *
 * @author Jan Albl
 */
public class MapTypeAdapter extends XmlAdapter<MapTypeAdapter.AdaptedTypeMap, Map<String, List<Type>>> {

    public static class AdaptedTypeMap {

        public List<MapTypeAdapter.ArgumentEntry> entry = new ArrayList<MapTypeAdapter.ArgumentEntry>();

    }
    public static class ArgumentEntry {

        public String key;

        public List<String> value;

    }

    @Override
    public Map<String, List<Type>> unmarshal(MapTypeAdapter.AdaptedTypeMap adaptedMap) throws Exception {
        Map<String, List<Type>> map = new HashMap<String, List<Type>>();
        for (MapTypeAdapter.ArgumentEntry entry : adaptedMap.entry) {
            map.put(entry.key, null);
        }
        return map;
    }

    @Override
    public MapTypeAdapter.AdaptedTypeMap marshal(Map<String, List<Type>> map) throws Exception {
        MapTypeAdapter.AdaptedTypeMap adaptedMap = new MapTypeAdapter.AdaptedTypeMap();
        for (Map.Entry<String, List<Type>> mapEntry : map.entrySet()) {
            MapTypeAdapter.ArgumentEntry entry = new MapTypeAdapter.ArgumentEntry();
            entry.key = mapEntry.getKey();

            entry.value = new ArrayList<String>();
            for(Type t : mapEntry.getValue()){
                entry.value.add(t.toString());
            }
            adaptedMap.entry.add(entry);
        }
        return adaptedMap;
    }
}