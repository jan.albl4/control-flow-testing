package cz.zcu.kiv.cft.output.data;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *
 * Data class containing information about the analyzed class
 *
 * @author Jan Albl
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Clazz {

    /** Path to class package */
    private String classPath;

    /** Package name */
    private String packageName;

    /** Class name */
    private String className;

    /** Flag of whether the analyzed class has a public constructor */
    private boolean hasPublicConstructor;

    /** List of analyzed class methods */
    @XmlElementWrapper(name = "methods")
    @XmlElement(name = "method")
    private List<Method> methods;

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }


    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


    public List<Method> getMethods() {
        return methods;
    }

    public void setMethods(List<Method> methods) {
        this.methods = methods;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isHasPublicConstructor() {
        return hasPublicConstructor;
    }

    public void setHasPublicConstructor(boolean hasPublicConstructor) {
        this.hasPublicConstructor = hasPublicConstructor;
    }
}
