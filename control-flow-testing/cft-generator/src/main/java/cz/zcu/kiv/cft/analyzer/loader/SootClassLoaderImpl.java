package cz.zcu.kiv.cft.analyzer.loader;

import cz.zcu.kiv.cft.analyzer.exception.LoadingMethodException;
import cz.zcu.kiv.cft.analyzer.exception.ClassNotLoadException;
import cz.zcu.kiv.cft.tools.OSPlatform;
import cz.zcu.kiv.cft.tools.log.Logger;
import soot.*;
import soot.options.Options;

import java.util.*;


/**
 * Class that is used to load SootClass and SootMethod
 *
 * @author Jan Albl
 */
public class SootClassLoaderImpl implements SootClassLoader {

    /** Loads all classes on the project and other referenced ones */
    private boolean loadNecessaryClasses = false;

    /** Instance of this class */
    private static SootClassLoaderImpl instance = null;

    /** Logger {@link Logger} */
    private static final Logger logger = Logger.getLogger();

    /**
     * Constructor to create an instance of this class.
     * *
     * @param projectFolderPath  path to the project folder to be analyzed. Other subfolders are treated as individual packages
     * @param loadNecessaryClasses  {@link #loadNecessaryClasses}
     * */
    private SootClassLoaderImpl(String projectFolderPath, boolean loadNecessaryClasses) {
        this.loadNecessaryClasses = loadNecessaryClasses;
        initSootFramework(projectFolderPath);
    }

    @Override
    public SootMethod getSootMethod(String clazz, String method) {
        try {
            SootMethod sootMethod = getSootClass(clazz).getMethodByName(method);

            if (sootMethod == null || sootMethod.isPhantom()) {
                throw new LoadingMethodException(clazz, method);
            }

            return sootMethod;

        } catch (ClassNotLoadException e) {
            throw e;
        } catch (RuntimeException e) {
            throw new LoadingMethodException(clazz, method);
        }
    }

    @Override
    public SootMethod getSootMethod(String clazz, String method, List<String> parameterType) {

        if(parameterType == null || parameterType.size() == 0){
            return getSootMethod(clazz, method);
        }

        try {
            SootMethod outputMethod = null;

            SootClass sootClass = getSootClass(clazz);
            List<SootMethod> sootMethodList = sootClass.getMethods();

            for(SootMethod sootMethod : sootMethodList){
                if(sootMethod.getName().equals(method) && sootMethod.getParameterCount() == parameterType.size()){

                    boolean isEquals = true;
                    for(int i = 0; i < sootMethod.getParameterCount(); i++){
                        if(!sootMethod.getParameterType(i).toString().equals(parameterType.get(i))){
                            isEquals = false;
                        }
                    }

                    if(isEquals){
                        if(outputMethod == null){
                            outputMethod = sootMethod;
                        } else {
                            throw new LoadingMethodException(clazz, method);
                        }

                    }
                }
            }

            if (outputMethod == null || outputMethod.isPhantom()) {
                throw new LoadingMethodException(clazz, method);
            }

            return outputMethod;

        } catch (ClassNotLoadException e) {
            throw e;
        } catch (RuntimeException e) {
            throw new LoadingMethodException(clazz, method);
        }
    }

    @Override
    public SootMethod getSootMethod(String clazz, String method, String returnType) {

        if(returnType == null || returnType.length() == 0){
            return getSootMethod(clazz, method);
        }

        try {
            SootMethod outputMethod = null;
            SootClass sootClass = getSootClass(clazz);
            List<SootMethod> sootMethodList = sootClass.getMethods();

            for(SootMethod sootMethod : sootMethodList){
                if(sootMethod.getName().equals(method)
                        && sootMethod.getReturnType().toString().equals(returnType)){

                    if(outputMethod == null){
                        outputMethod = sootMethod;
                    } else {
                        //vice method a nelze specifikovat podle vstupnich parametru kterou vybrat
                        throw new LoadingMethodException(clazz, method);
                    }
                }
            }

            return outputMethod;
        } catch (ClassNotLoadException e) {
            throw e;
        } catch (RuntimeException e) {
            throw new LoadingMethodException(clazz, method);
        }

    }

    @Override
    public SootMethod getSootMethod(String clazz, String method, List<String> parameterType, String returnType) {

        if(parameterType == null && returnType == null){
            return getSootMethod(clazz, method);
        }else if(returnType == null){
            return getSootMethod(clazz, method, parameterType);
        }else if(parameterType == null){
            return getSootMethod(clazz, method, returnType);
        }

        try {
            SootMethod outputMethod = null;
            SootClass sootClass = getSootClass(clazz);

            List<SootMethod> sootMethodList = sootClass.getMethods();

            for(SootMethod sootMethod : sootMethodList){
                if(sootMethod.getName().equals(method)
                        && sootMethod.getParameterCount() == parameterType.size()
                        && sootMethod.getReturnType().toString().equals(returnType)){

                    boolean isEquals = true;
                    for(int i = 0; i < sootMethod.getParameterCount(); i++){
                        if(!sootMethod.getParameterType(i).toString().equals(parameterType.get(i))){
                            isEquals = false;
                        }
                    }

                    if(isEquals){
                        if(outputMethod == null){
                            outputMethod = sootMethod;
                        } else {
                            throw new LoadingMethodException(clazz, method);
                        }

                    }
                }
            }

            if (outputMethod == null || outputMethod.isPhantom()) {
                throw new LoadingMethodException(clazz, method);
            }

            return outputMethod;
        } catch (ClassNotLoadException e) {
            throw e;
        } catch (RuntimeException e) {
            throw new LoadingMethodException(clazz, method);
        }
    }

    @Override
    public List<SootClass> getSubClass(SootClass sootClass) {

        if (sootClass != null) {
            List<SootClass> sootClasses = new ArrayList<>();

            FastHierarchy fh = Scene.v().getOrMakeFastHierarchy();
            Collection ce = fh.getSubclassesOf(sootClass);

            Iterator it = ce.iterator();

            while(it.hasNext()) {
                SootClass sc = (SootClass) it.next();
                sootClasses.add(sc);
            }

            return sootClasses;
        }

        return null;
    }

    @Override
    public List<SootClass> getSubClass(Type type) {
        if(type instanceof RefType){
            RefType refType = (RefType) type;
            return getSubClass(refType.getSootClass());

        }
        return null;
    }

    @Override
    public List<SootClass> getSubClassForInterface(SootClass sootClass) {
        if (sootClass != null && sootClass.isInterface()) {
            List<SootClass> sootClasses = new ArrayList<>();

            Scene.v().loadDynamicClasses();

            FastHierarchy fh = Scene.v().getOrMakeFastHierarchy();
            Collection ce = fh.getAllImplementersOfInterface(sootClass);

            Iterator it = ce.iterator();

            while(it.hasNext()) {
                SootClass sc = (SootClass) it.next();
                sootClasses.add(sc);
            }

            return sootClasses;
        }
        return null;
    }

    @Override
    public List<SootClass> getSubClassForInterface(Type type) {
        if(type instanceof RefType){
            RefType refType = (RefType) type;
            return getSubClassForInterface(refType.getSootClass());

        }
        return null;
    }

    @Override
    public SootClass getSootClass(String name){
        try {
            SootClass sootClass  = Scene.v().loadClassAndSupport(name);

            if (loadNecessaryClasses) {
                Scene.v().loadNecessaryClasses();
            }

            if (sootClass == null || sootClass.isPhantom()) {
                throw new ClassNotLoadException("Class "+name+" not found or unable to load");
            }else if (sootClass.isInterface()){
                throw new ClassNotLoadException("Class "+name+" not found or unable to load. Make sure it's not an interface.");
            }

            return sootClass;

        } catch (Exception e) {
            throw new ClassNotLoadException("Class "+name+" not found or unable to load");
        }
    }

    /**
     * Return instance of this class
     * @param projectPath path to the project folder to be analyzed. Other subfolders are treated as individual packages
     * @param loadNecessaryClasses {@link #loadNecessaryClasses}
     * @return instance of this class
     * */
    public static SootClassLoaderImpl getInstance(String projectPath, boolean loadNecessaryClasses) {
        if(instance == null) {
            instance = new SootClassLoaderImpl(projectPath, loadNecessaryClasses);
        }
        return instance;
    }

    /**
     * Return instance of this class
     * @return instance of this class
     * */
    public static SootClassLoaderImpl getInstance() {
        return instance;
    }

    /**
     * Initializes soot framework for loading classes and methods
     * @param projectPath path to the project folder to be analyzed. Other subfolders are treated as individual packages
     * */
    private static void initSootFramework(String projectPath){

        logger.debug("initialize the soot framework");

        String separator = ":";

        if (OSPlatform.isWindows()) {
            separator = ";";
        }

        String dir = System.getProperty("user.dir") + separator + projectPath;

        Options.v().set_whole_program(true);
        Options.v().set_full_resolver(true);
        //Options.v().set_verbose(true);
        Options.v().set_allow_phantom_refs(true);
        Options.v().setPhaseOption("jb", "use-original-names:true");
        Options.v().set_output_format(Options.output_format_jimple);
        Options.v().set_process_dir(Collections.singletonList(projectPath));

        //Options.v().set_soot_classpath(Scene.v().defaultClassPath() + dir);
        Scene.v().setSootClassPath(Scene.v().getSootClassPath() + separator + dir);

        logger.debug("Soot scene class path: " + Scene.v().getSootClassPath());
    }
}