package cz.zcu.kiv.cft.tools.log;

/**
 *
 * Class for representing logger level
 *
 * @author Jan Albl
 */
public enum  LoggerLevel {
    INFO,
    DEBUG,
    ERROR,
    WARN
}
