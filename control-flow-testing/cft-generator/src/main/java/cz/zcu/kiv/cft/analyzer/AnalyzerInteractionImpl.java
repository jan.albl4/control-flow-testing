package cz.zcu.kiv.cft.analyzer;

import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.tools.log.Logger;

import java.util.*;

/**
 *
 * Class for analyzing the interaction strength of input parameters of method in {@link ControlFlowAnalysisOutput}
 *
 * @author Jan Albl
 */
public class AnalyzerInteractionImpl implements AnalyzerInteraction {

    /** Logger {@link Logger} */
    private static final Logger logger = Logger.getLogger();

    @Override
    public void analyzeInteraction(ControlFlowAnalysisOutput controlFlowAnalysisOutput) {
        if(controlFlowAnalysisOutput.getClasses() != null){

            for(Clazz clazz : controlFlowAnalysisOutput.getClasses()) {
                if(clazz.getMethods() != null) {
                    for (Method method : clazz.getMethods()) {

                        logger.debug("analyze interaction strength input parameters for \"" + method.getName() + "\" class \"" + clazz.getClassName() + "\"");

                        int interaction = 0;

                        for (ConditionElement condition: method.getConditions()){
                            int localInteraction = 0;

                            Set<String> interactionSet = new HashSet<>();

                            if(condition instanceof SwitchConditions){
                                for(Condition c : ((SwitchConditions) condition).getBranches()){
                                    localInteraction = processCondition(c, interactionSet);

                                    if(localInteraction > interaction){
                                        interaction = localInteraction;
                                    }
                                }
                            }else{
                                localInteraction = processCondition((Condition) condition, interactionSet);

                                if(localInteraction > interaction){
                                    interaction = localInteraction;
                                }
                            }
                        }

                        method.setInteractionStrength(interaction);
                    }
                }
            }
        }
    }

    /**
     * Calculates interaction strength of input condition and nested conditions.
     *
     * @param c input condition
     * @param interactionSet set of input parameter names found
     * @return interaction strength of input conditions and nested conditions
     * */
    private int processCondition(Condition c, Set<String> interactionSet){
        if(c != null){

            int interaction = 0;

            for(ConditionalExpression ce : c.getConditionalExpressions()){
                interactionSet.addAll(ce.getArgTypeMap().keySet());

                if(interactionSet.size() > interaction && ce.isInputDependent()){
                    interaction = interactionSet.size();
                }
            }

            for (ConditionElement condition: c.getTrueBranch()){
                if (condition instanceof Condition) {
                    Set<String> interactionSetBranch = new HashSet<>();
                    interactionSetBranch.addAll(interactionSet);

                    int localInteraction = processCondition((Condition) condition, interactionSetBranch);

                    if(localInteraction > interaction){
                        interaction = localInteraction;
                    }
                } else {
                    for (Condition cw :((SwitchConditions)condition).getBranches()) {
                        int localInteraction = processCondition(cw, interactionSet);

                        if(localInteraction > interaction){
                            interaction = localInteraction;
                        }
                    }
                }
            }

            for (ConditionElement condition: c.getFalseBranch()){

                if (condition instanceof Condition) {
                    Set<String> interactionSetBranch = new HashSet<>();
                    interactionSetBranch.addAll(interactionSet);

                    int localInteraction = processCondition((Condition) condition, interactionSetBranch);

                    if(localInteraction > interaction){
                        interaction = localInteraction;
                    }
                } else {
                    for (Condition cw :((SwitchConditions)condition).getBranches()) {

                        Set<String> interactionSetBranch = new HashSet<>();
                        interactionSetBranch.addAll(interactionSet);

                        int localInteraction = processCondition(cw, interactionSetBranch);

                        if(localInteraction > interaction){
                            interaction = localInteraction;
                        }
                    }
                }
            }

            return interaction;
        }

        return 0;
    }
}
