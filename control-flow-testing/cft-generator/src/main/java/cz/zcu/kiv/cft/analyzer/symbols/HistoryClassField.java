package cz.zcu.kiv.cft.analyzer.symbols;

import soot.SootField;
import soot.Type;

/**
 * Data structure for storing class field information
 *
 * @author Jan Albl
 */
public class HistoryClassField extends HistorySymbolItem {

    /** Field data type */
    private Type type;

    /** Field */
    private SootField fieldRef;

    /** Field name */
    private String name;

    /** Is the input parameter */
    private Boolean param = false;

    /** Reference to the parent class in the symbol table */
    private HistorySymbolItem parentHSI;

    /** Field history in the symbol table */
    private SymbolItem symbolItem;

    public Boolean getParam() {
        return param;
    }

    public void setParam(Boolean param) {
        this.param = param;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public SootField getFieldRef() {
        return fieldRef;
    }

    public void setFieldRef(SootField fieldRef) {
        this.fieldRef = fieldRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HistorySymbolItem getParentHSI() {
        return parentHSI;
    }

    public void setParentHSI(HistorySymbolItem parentHSI) {
        this.parentHSI = parentHSI;
    }

    public SymbolItem getSymbolItem() {
        return symbolItem;
    }

    public void setSymbolItem(SymbolItem symbolItem) {
        this.symbolItem = symbolItem;
    }
}
