package cz.zcu.kiv.cft.output.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.HashSet;
import java.util.Set;

/**
 * Class representing the enum method parameter
 *
 * @author Jan Albl
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EnumParameter extends Parameter {

    /** Flag whether the parameter should be null-tested */
    private boolean checkedNull;

    /** Possible enum values */
    @XmlElementWrapper(name = "enums")
    @XmlElement(name = "enum")
    private Set<String> possibleEnums = new HashSet<>();

    public Set<String> getPossibleEnums() {
        return possibleEnums;
    }

    public void setPossibleEnums(Set<String> possibleEnums) {
        this.possibleEnums = possibleEnums;
    }

    public boolean isCheckedNull() {
        return checkedNull;
    }

    public void setCheckedNull(boolean checkedNull) {
        this.checkedNull = checkedNull;
    }
}
