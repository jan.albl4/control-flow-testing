package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.output.data.Condition;
import cz.zcu.kiv.cft.output.data.ConditionElement;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.output.data.SwitchConditions;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.util.Enumeration;

public class ConditionTree extends JTree {

    private MainPanel mainPanel;

    public ConditionTree(ControlFlowAnalysisOutput controlFlowAnalysisOutput, MainPanel mainPanel){
        this.mainPanel = mainPanel;
        this.removeAll();

        DefaultTreeModel model = (DefaultTreeModel)this.getModel();
        DefaultMutableTreeNode root    = new DefaultMutableTreeNode("root");


        for(int i = 0; i < controlFlowAnalysisOutput.getClasses().get(0).getMethods().get(0).getConditions().size(); i++){
            treeConditionNode(controlFlowAnalysisOutput.getClasses().get(0).getMethods().get(0).getConditions().get(i), root);
        }

        this.putClientProperty("JTree.lineStyle", "Angled");

        model.reload(root);
        model.setRoot(root);
        expandTree(this,true);

        this.addTreeSelectionListener(new treeListener(this));
        this.setRootVisible(false);
    }

    private void treeConditionNode(ConditionElement c, DefaultMutableTreeNode parrent){
        if (c != null) {

            if (c instanceof Condition) {
                DefaultMutableTreeNode cNode = new DefaultMutableTreeNode( ((Condition) c));

                for(ConditionElement conditionElement : ((Condition) c).getTrueBranch()){
                    treeConditionNode(conditionElement, cNode);
                }

                for(ConditionElement conditionElement : ((Condition) c).getFalseBranch()){
                    treeConditionNode(conditionElement, cNode);
                }

                parrent.add(cNode);
            } else if (c instanceof SwitchConditions) {
                for(ConditionElement conditionElement : ((SwitchConditions) c).getBranches()){
                    treeConditionNode(conditionElement, parrent);
                }
            }
        }
    }

    private void expandTree(JTree tree, boolean expand) {
        TreeNode root = (TreeNode) tree.getModel().getRoot();
        expandAll(tree, new TreePath(root), expand);
    }

    private void expandAll(JTree tree, TreePath path, boolean expand) {
        TreeNode node = (TreeNode) path.getLastPathComponent();

        if (node.getChildCount() >= 0) {
            Enumeration enumeration = node.children();
            while (enumeration.hasMoreElements()) {
                TreeNode n = (TreeNode) enumeration.nextElement();
                TreePath p = path.pathByAddingChild(n);

                expandAll(tree, p, expand);
            }
        }

        if (expand) {
            tree.expandPath(path);
        } else {
            tree.collapsePath(path);
        }
    }

    private void changeField(String trueCondition, String falseCondition){
        mainPanel.selectCondition(trueCondition, falseCondition);
    }

    class treeListener implements TreeSelectionListener {

        ConditionTree conditionTree;

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)conditionTree.getLastSelectedPathComponent();
            String conditionStringTrue = "";
            String conditionStringFalse = "";
            if(selectedNode.getUserObject() instanceof Condition){
                ConditionElement c = (Condition) selectedNode.getUserObject();

                conditionStringTrue = "("+((Condition)c).getConditionalExpressions().get(0).getExpression()+")";
                conditionStringFalse = "!("+((Condition)c).getConditionalExpressions().get(0).getExpression()+")";

                while (c.getParentCondition() != null){
                    if (c.getParentCondition() instanceof Condition) {
                        if(((Condition) c.getParentCondition()).getTrueBranch().contains(c)){
                            conditionStringTrue =  "(" + ((Condition) c.getParentCondition()).getConditionalExpressions().get(0).getExpression() + ") && "  + conditionStringTrue;
                            conditionStringFalse =  "(" + ((Condition) c.getParentCondition()).getConditionalExpressions().get(0).getExpression() + ") && "  + conditionStringFalse;
                        }else{
                            conditionStringTrue =  "!(" + ((Condition) c.getParentCondition()).getConditionalExpressions().get(0).getExpression() + ") && " + conditionStringTrue;
                            conditionStringFalse =  "!(" + ((Condition) c.getParentCondition()).getConditionalExpressions().get(0).getExpression() + ") && " + conditionStringFalse;
                        }
                        c = c.getParentCondition();
                    } else {
                        c = c.getParentCondition();
                    }
                }
            }

            conditionTree.changeField(conditionStringTrue, conditionStringFalse);
        }

        public treeListener(ConditionTree conditionTree) {
            this.conditionTree = conditionTree;
        }
    }
}
