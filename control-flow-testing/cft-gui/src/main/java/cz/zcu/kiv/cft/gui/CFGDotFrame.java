package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import javax.swing.*;
import java.awt.*;

public class CFGDotFrame extends JFrame{

    private static CFGDotFrame instance;

    private CFGDotFrame(ControlFlowGraph controlFlowGraph){
        setTitle("Control flow graph - DOT");
        setSize(800,600);

        setMinimumSize(new Dimension(800,600));
        Container pane = getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

        String dot = controlFlowGraph.getDot();

        JTextArea dotTextArea = new JTextArea();
        dotTextArea.setColumns(countLines(dot));
        dotTextArea.setText(dot);
        dotTextArea.setEditable(false);

        JScrollPane scroll = new JScrollPane(dotTextArea);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        pane.add(scroll, BorderLayout.CENTER);

        this.setVisible(true);
        this.setResizable(false);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    public static CFGDotFrame getInstance(ControlFlowGraph controlFlowGraph){
        if(instance == null){
            instance =  new CFGDotFrame(controlFlowGraph);
        }
        return instance;
    }

    private static int countLines(String str){
        String[] lines = str.split("\r\n|\r|\n");
        return  lines.length;
    }
}
