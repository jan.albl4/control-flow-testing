package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.Configuration;
import cz.zcu.kiv.cft.analyzer.Analyzer;
import cz.zcu.kiv.cft.analyzer.AnalyzerImpl;
import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import cz.zcu.kiv.cft.analyzer.symbols.DetailedAnalyzerSymbolTables;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;

public class MenuBar extends JMenuBar {

    private ControlFlowAnalysisOutput controlFlowAnalysisOutput;

    private ControlFlowGraph controlFlowGraph;

    public MenuBar(MainFrame mainFrame){
        JMenuItem menuItem;

        JMenu menu;

        menu = new JMenu("Configuration");
        menuItem = new JMenuItem(new MenuBar.configuration(mainFrame));
        menu.add(menuItem);
        this.add(menu);

        menu = new JMenu("Run");
        menuItem = new JMenuItem(new MenuBar.runSearch(mainFrame));
        menu.add(menuItem);
        this.add(menu);

        menu = new JMenu("Show");
        menuItem = new JMenuItem(new MenuBar.saveGraphPng());
        menuItem.setEnabled(false);
        menu.add(menuItem);

        menuItem = new JMenuItem(new MenuBar.showGraphDot());
        menuItem.setEnabled(false);
        menu.add(menuItem);

        this.add(menu);

        menu = new JMenu("Help");
        menuItem = new JMenuItem(new MenuBar.helpAction());
        menu.add(menuItem);
        this.add(menu);
    }

    /**=========AKCE ======*/
    /*******************************************************************************
     *
     *
     *
     *
     * @author
     * @version   1.00.000
     */
    class helpAction extends AbstractAction {


        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(null,"This gui is for easier manipulation with the library for generating unit tests based on the control flow graph.\nYou can load one class method in the configuration. After generating the data, you can add any additional\nparameter values. You can save the resulting generated test to any folder.","Help", JOptionPane.INFORMATION_MESSAGE );
        }


        public helpAction() {
            putValue(NAME, "Help");
            putValue(MNEMONIC_KEY, new Integer(KeyEvent.VK_I));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        }
    }

    class configuration extends AbstractAction {

        private MainFrame mainFrame;

        @Override
        public void actionPerformed(ActionEvent e) {
            ConfigFrame.setMainFrame(mainFrame);
            ConfigFrame configFrame = ConfigFrame.getInstance();
            configFrame.setVisible(true);
        }

        public configuration(MainFrame mainFrame) {
            this.mainFrame = mainFrame;
            putValue(NAME, "Configuration");
        }
    }

    class showGraphDot extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            CFGDotFrame configFrame = CFGDotFrame.getInstance(controlFlowGraph);
            configFrame.setVisible(true);
        }

        public showGraphDot() {
            putValue(NAME, "Show CFG - dot");
        }
    }

    class runSearch extends AbstractAction {
        private MainFrame mainFrame;

        @Override
        public void actionPerformed(ActionEvent e) {

            if(mainFrame.getActualConfiguratin() != null
                    && mainFrame.getActualConfiguratin().getClassName() != null
                    && mainFrame.getActualConfiguratin().getClassName().length() > 0
                    && mainFrame.getActualConfiguratin().getMethodName() != null
                    && mainFrame.getActualConfiguratin().getMethodName().length() > 0) {

                mainFrame.initLoading();

                Worker w = new Worker();
                w.mainFrame = mainFrame;

                w.start();
            }else{
                JOptionPane.showMessageDialog(null,"The test method is not selected","Information", JOptionPane.INFORMATION_MESSAGE );
            }
        }

        public runSearch(MainFrame mainFrame) {
            putValue(NAME, "Run");

            this.mainFrame = mainFrame;
            //putValue(MNEMONIC_KEY, new Integer(KeyEvent.VK_N));
            //putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        }
    }

    class Worker extends Thread{
        public MainFrame mainFrame;
        @Override
        public void run(){
            try {
                Configuration configuration = this.mainFrame.getActualConfiguratin();

                System.out.println("Configuratino : " + configuration);

                Analyzer analyzer = new AnalyzerImpl.Builder()
                        .setProjectPath(configuration.getProjectPath())
                        .setPackagePath(configuration.getPackagePath())
                        .setMaxDepth(configuration.getMaxDepth())
                        .setSymbolTableAnalyzer(new DetailedAnalyzerSymbolTables()).build();

                //Analyzer cz.zcu.kiv.cz.zcu.kiv.jop.cft.analyzer = new AnalyzerImpl(sootClassLoaderImpl, true);
                controlFlowAnalysisOutput = analyzer.analyzeMethod(configuration.getClassName(), configuration.getMethodName());

                DataGenerator dataGenerator = new DataGeneratorImpl();
                dataGenerator.generateData(controlFlowAnalysisOutput);

                controlFlowGraph = analyzer.getControlFlowGraph(configuration.getClassName(), configuration.getMethodName(), null, null);

                mainFrame.initControlFlowOutput(controlFlowAnalysisOutput);

                //fillParameters(cz.zcu.kiv.jop.cft
            } catch (Exception exception){

                JOptionPane.showMessageDialog(null, exception,"Information", JOptionPane.ERROR_MESSAGE );

                mainFrame.initControlFlowOutput( null);


            } catch (Error error) {

                JOptionPane.showMessageDialog(null, error,"Information", JOptionPane.ERROR_MESSAGE );

                mainFrame.initControlFlowOutput( null);
            }
        }
    }


    class saveGraphPng extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser chooser = new JFileChooser();

            chooser.setFileFilter(new FileNameExtensionFilter("Image Files", "png"));
            chooser.setCurrentDirectory(new java.io.File("."));
            chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            chooser.setSelectedFile(new File("control-flow-graph.png"));

            switch (chooser.showSaveDialog(chooser)) {
                case JFileChooser.APPROVE_OPTION:
                    // Open file...
                    if(chooser.getSelectedFile().isFile() && chooser.getSelectedFile().getName().toLowerCase().endsWith(".png")){
                        controlFlowGraph.saveGraphGraphviz(chooser.getSelectedFile().getPath());
                    }else if(chooser.getSelectedFile().isDirectory()){
                        controlFlowGraph.saveGraphGraphviz(chooser.getSelectedFile().getPath() + "control-flow-graph.png");
                    }else {
                        if (chooser.getSelectedFile().getName().toLowerCase().endsWith(".png")) {
                            controlFlowGraph.saveGraphGraphviz(chooser.getSelectedFile().getPath());
                        }else{
                            controlFlowGraph.saveGraphGraphviz(chooser.getSelectedFile().getPath() + ".png");
                        }
                    }

                    break;
            }


        }

        public saveGraphPng() {
            putValue(NAME, "Save CFG");
            //putValue(MNEMONIC_KEY, new Integer(KeyEvent.VK_N));
            //putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        }
    }
}
