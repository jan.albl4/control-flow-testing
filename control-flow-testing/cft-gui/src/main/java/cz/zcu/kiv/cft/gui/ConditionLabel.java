package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.output.data.Condition;

import javax.swing.*;

public class ConditionLabel extends JLabel {

    private Condition condition;

    public ConditionLabel(String text, Condition condition){
        super(text);
        this.condition = condition;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }
}
