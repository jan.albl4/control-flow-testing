package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.AnalyzedType;
import cz.zcu.kiv.cft.Configuration;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ConfigFrame extends JFrame {

    private static ConfigFrame instance;

    private static MainFrame mainFrame;

    private ConfigFrame()  {
        super("Configuration");
        this.getContentPane().setLayout(new BorderLayout());

        createAllComponents();

        this.setPreferredSize(new Dimension(500, 250));
        this.setResizable(false);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void createAllComponents(){
        JPanel settingPanel = new JPanel(new GridLayout(5, 3));
        JPanel nextPanel = new JPanel(new GridLayout(5, 1));

        JLabel classFileLabel = new JLabel("Input class (file):");
        classFileLabel.setBorder(new EmptyBorder(0,10,0,0));
        settingPanel.add(classFileLabel);

        JTextField classFileTextField = new JTextField();
        classFileTextField.setEnabled(false);
        settingPanel.add(classFileTextField);

        JLabel methodNameLabel = new JLabel("Name of the method:");
        methodNameLabel.setBorder(new EmptyBorder(0,10,0,0));
        settingPanel.add(methodNameLabel);

        JTextField methodNameTextField = new JTextField();
        methodNameTextField.getDocument().addDocumentListener(new methodListener(methodNameTextField));

        settingPanel.add(methodNameTextField);

        JLabel packageClassLabel = new JLabel("Package class:");
        packageClassLabel.setBorder(new EmptyBorder(0,10,0,0));
        settingPanel.add(packageClassLabel);

        JTextField packageClassTextField = new JTextField();
        packageClassTextField.getDocument().addDocumentListener(new packageListener(packageClassTextField));
        settingPanel.add(packageClassTextField);

        JLabel projectFolderLabel = new JLabel("Project folder:");
        projectFolderLabel.setBorder(new EmptyBorder(0,10,0,0));
        settingPanel.add(projectFolderLabel);

        JTextField projectFolderTextField = new JTextField();
        projectFolderTextField.getDocument().addDocumentListener(new projectFolderListener(projectFolderTextField));
        settingPanel.add(projectFolderTextField);

        JLabel maximumSearchLabel = new JLabel("Maximum search depth:");
        maximumSearchLabel.setBorder(new EmptyBorder(0,10,0,0));
        settingPanel.add(maximumSearchLabel);

        JTextField maximumSearchTextField = new JTextField();
        maximumSearchTextField.getDocument().addDocumentListener(new deepListener(maximumSearchTextField));

        settingPanel.add(maximumSearchTextField);

        JButton chooseButton = new JButton("Choose");
        chooseButton.addActionListener(new loadFile(classFileTextField, packageClassTextField, projectFolderTextField));
        nextPanel.add(chooseButton);

        JPanel buttonSavePanel = new JPanel(new BorderLayout());
        JButton buttonSave = new JButton("Save");
        buttonSave.setMargin(new Insets(10, 20, 10, 20));

        buttonSavePanel.add(buttonSave);

        this.getContentPane().add(settingPanel, BorderLayout.CENTER);
        this.getContentPane().add(nextPanel, BorderLayout.EAST);
        this.getContentPane().add(buttonSavePanel, BorderLayout.SOUTH);
    }

    public static MainFrame getMainFrame() {
        return mainFrame;
    }

    public static void setMainFrame(MainFrame mainFrame) {
        ConfigFrame.mainFrame = mainFrame;
    }

    public static ConfigFrame getInstance(){
        if(instance == null){
            instance =  new ConfigFrame();
        }

        return instance;
    }


    //TODO - validace
    class loadFile extends AbstractAction {

        private JTextField classFileTextField;

        private JTextField packageClassTextField;

        private JTextField projectFolderTextField;

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setMultiSelectionEnabled(false);

            JFrame frame = new JFrame();
            switch (fileChooser.showOpenDialog(frame)) {
                case JFileChooser.APPROVE_OPTION:
                    // Open file...

                    String className = fileChooser.getSelectedFile().getName();
                    String classPath = fileChooser.getSelectedFile().getPath();
                    String packagePath = this.packagePath(fileChooser.getSelectedFile());

                    int i = className.lastIndexOf('.');
                    if (i > 0) {
                        classPath = classPath.replace(className, "");
                        className = className.substring(0, i);

                        String packageReplace = packagePath.replace(".", File.separator);
                        classPath = classPath.replace(packageReplace, "");
                        classPath = classPath.substring(0, classPath.length()-1);
                    }


                    Configuration oldConfig = mainFrame.getActualConfiguratin();

                    String methodName = "";

                    int maxDeep = 0;


                    if(oldConfig != null && oldConfig.getMaxDepth() >= 0){
                        maxDeep = oldConfig.getMaxDepth();
                    }

                    if(oldConfig != null && oldConfig.getMethodName() != null){
                        methodName = oldConfig.getMethodName();
                    }

                    Configuration configuration = new Configuration.Builder()
                            .setMethodName(methodName)
                            .setClassName(className)
                            .setProjectPath(classPath)
                            .setPackagePath(packagePath)
                            .setAnalyzedType(AnalyzedType.METHOD)
                            .setMaxDepth(maxDeep)
                            .build();

                    System.out.println(configuration);

                    mainFrame.setActualConfiguratin(configuration);

                    classFileTextField.setText(fileChooser.getSelectedFile().getName());
                    packageClassTextField.setText(packagePath);
                    projectFolderTextField.setText(classPath);

                    break;
            }
        }

        public loadFile(JTextField classFileTextField, JTextField packageClassTextField, JTextField projectFolderTextField) {
            this.classFileTextField = classFileTextField;
            this.packageClassTextField = packageClassTextField;
            this.projectFolderTextField = projectFolderTextField;
            putValue(NAME, "Načíst soubor");
            //putValue(MNEMONIC_KEY, new Integer(KeyEvent.VK_N));
            //putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        }

        private String packagePath(File file){

            String output = "";

            try {
                BufferedReader b = new BufferedReader(new FileReader(file));

                String readLine = "";

                while ((readLine = b.readLine()) != null) {

                    if(readLine.contains("package")){
                        output = readLine.replace("package", "").trim();
                        output = output.replace(";", "");
                        break;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return output;
        }
    }

    class deepListener implements DocumentListener {

        private JTextField textField;

        deepListener(JTextField textField){
            this.textField = textField;
        }

        public void changedUpdate(DocumentEvent e) {
            warn();
        }
        public void removeUpdate(DocumentEvent e) {
            warn();
        }
        public void insertUpdate(DocumentEvent e) {
            warn();
        }

        public void warn() {
            try{
                int deep = Integer.valueOf(textField.getText());

                if(deep >= 0 && deep < 20){
                    Configuration configuration = mainFrame.getActualConfiguratin();

                    String className = "";
                    String methodName = "";
                    String projectPath = "";
                    String packagePath = "";

                    if(configuration != null && configuration.getClassName() != null){
                        className = configuration.getClassName();
                    }

                    if(configuration != null && configuration.getProjectPath() != null){
                        projectPath = configuration.getProjectPath();
                    }

                    if(configuration != null && configuration.getPackagePath() != null){
                        packagePath = configuration.getPackagePath();
                    }

                    if(configuration != null && configuration.getMethodName() != null){
                        methodName = configuration.getMethodName();
                    }


                    configuration = new Configuration.Builder()
                            .setMethodName(methodName)
                            .setClassName(className)
                            .setProjectPath(projectPath)
                            .setPackagePath(packagePath)
                            .setAnalyzedType(AnalyzedType.METHOD)
                            .setMaxDepth(deep)
                            .build();


                    mainFrame.setActualConfiguratin(configuration);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class methodListener implements DocumentListener {

        private JTextField textField;

        methodListener(JTextField textField){
            this.textField = textField;
        }

        public void changedUpdate(DocumentEvent e) {
            warn();
        }
        public void removeUpdate(DocumentEvent e) {
            warn();
        }
        public void insertUpdate(DocumentEvent e) {
            warn();
        }

        public void warn() {

            Configuration configuration = mainFrame.getActualConfiguratin();

            String className = "";
            String projectPath = "";
            String packagePath = "";
            int maxDeep = 0;

            if(configuration != null && configuration.getClassName() != null){
                className = configuration.getClassName();
            }

            if(configuration != null && configuration.getProjectPath() != null){
                projectPath = configuration.getProjectPath();
            }

            if(configuration != null && configuration.getPackagePath() != null){
                packagePath = configuration.getPackagePath();
            }

            if(configuration != null && configuration.getMaxDepth() >= 0){
                maxDeep = configuration.getMaxDepth();
            }

            configuration = new Configuration.Builder()
                    .setMethodName(textField.getText().trim())
                    .setClassName(className)
                    .setProjectPath(projectPath)
                    .setPackagePath(packagePath)
                    .setAnalyzedType(AnalyzedType.METHOD)
                    .setMaxDepth(maxDeep)
                    .build();

            mainFrame.setActualConfiguratin(configuration);
        }
    }

    class packageListener implements DocumentListener {

        private JTextField textField;

        packageListener(JTextField textField){
            this.textField = textField;
        }

        public void changedUpdate(DocumentEvent e) {
            warn();
        }
        public void removeUpdate(DocumentEvent e) {
            warn();
        }
        public void insertUpdate(DocumentEvent e) {
            warn();
        }

        public void warn() {
            Configuration configuration = mainFrame.getActualConfiguratin();

            String className = "";
            String methodName = "";
            String projectPath = "";

            int maxDeep = 0;

            if(configuration != null && configuration.getClassName() != null){
                className = configuration.getClassName();
            }

            if(configuration != null && configuration.getMethodName() != null){
                methodName = configuration.getMethodName();
            }

            if(configuration != null && configuration.getProjectPath() != null){
                projectPath = configuration.getProjectPath();
            }

            if(configuration != null && configuration.getMaxDepth() >= 0){
                maxDeep = configuration.getMaxDepth();
            }

            configuration = new Configuration.Builder()
                    .setMethodName(methodName)
                    .setClassName(className)
                    .setProjectPath(projectPath)
                    .setPackagePath(textField.getText().trim())
                    .setAnalyzedType(AnalyzedType.METHOD)
                    .setMaxDepth(maxDeep)
                    .build();

            mainFrame.setActualConfiguratin(configuration);
        }
    }

    class projectFolderListener implements DocumentListener {

        private JTextField textField;

        projectFolderListener(JTextField textField){
            this.textField = textField;
        }

        public void changedUpdate(DocumentEvent e) {
            warn();
        }
        public void removeUpdate(DocumentEvent e) {
            warn();
        }
        public void insertUpdate(DocumentEvent e) {
            warn();
        }

        public void warn() {
            Configuration configuration = mainFrame.getActualConfiguratin();

            String className = "";
            String methodName = "";
            String packagePath = "";

            int maxDeep = 0;

            if(configuration != null && configuration.getClassName() != null){
                className = configuration.getClassName();
            }

            if(configuration != null && configuration.getMethodName() != null){
                methodName = configuration.getMethodName();
            }

            if(configuration != null && configuration.getPackagePath() != null){
                packagePath = configuration.getPackagePath();
            }

            if(configuration != null && configuration.getMaxDepth() >= 0){
                maxDeep = configuration.getMaxDepth();
            }

            configuration = new Configuration.Builder()
                    .setMethodName(methodName)
                    .setClassName(className)
                    .setProjectPath(textField.getText().trim())
                    .setPackagePath(packagePath)
                    .setAnalyzedType(AnalyzedType.METHOD)
                    .setMaxDepth(maxDeep)
                    .build();

            mainFrame.setActualConfiguratin(configuration);
        }
    }
}




