package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.output.data.EnumParameter;
import cz.zcu.kiv.cft.output.data.ObjectParameter;
import cz.zcu.kiv.cft.output.data.Parameter;
import cz.zcu.kiv.cft.output.data.PrimitiveParameter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ParameterPanal extends JPanel {

    private Parameter parameter;

    public ParameterPanal(Parameter p, JPanel parrent, String fullName){
        super();

        this.parameter = p;

        JPanel oneVariableP = new JPanel();
        oneVariableP.setLayout(new BorderLayout(0, 0));

        JPanel panel_9 = new JPanel();
        oneVariableP.add(panel_9, BorderLayout.SOUTH);
        panel_9.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JButton btnNewButton = new JButton("Add");

        panel_9.add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Remove");
        panel_9.add(btnNewButton_1);

        DefaultListModel<String> model = new DefaultListModel<>();
        JList<String> list = new JList<>( model );

        JScrollPane scrollBar = new JScrollPane(list);

        if(p.isPrimitive()){
            for(String value : ((PrimitiveParameter)p).getValues()){
                model.addElement(value);
            }
        }else if(p instanceof ObjectParameter){
            for(String value : ((ObjectParameter)p).getPossibleObjectTypes()){
                model.addElement(value);
            }
        }else if(p instanceof EnumParameter){
            for(String value : ((EnumParameter)p).getPossibleEnums()){
                model.addElement(value);
            }
        }

        btnNewButton.addActionListener(new addValueListener(list, p.getDataType(), model, p));
        btnNewButton_1.addActionListener(new removeValueListener(list, p.getDataType(), model, p));

        JLabel lblAint;

        if(fullName == null  || fullName.length() == 0){
            lblAint = new JLabel(p.getName() +  "(" + p.getDataType() + ")");
        } else {
            lblAint = new JLabel(fullName +"."+p.getName() +  "(" + p.getDataType() + ")");
        }

        lblAint.setHorizontalAlignment(SwingConstants.CENTER);
        oneVariableP.add(lblAint, BorderLayout.NORTH);

        //scrollBar.add(list);
        //scrollBar.setPreferredSize(new Dimension(250, 80));
        oneVariableP.add(scrollBar, BorderLayout.CENTER);

        parrent.add(oneVariableP);
    }

    class addValueListener extends AbstractAction {

        JList<String> list;
        DefaultListModel<String> model;
        String dataType;
        Parameter parameter;

        public addValueListener(JList<String> list, String dataType, DefaultListModel<String> model,Parameter parameter) {
            this.dataType = dataType;
            this.list = list;
            this.model = model;
            this.parameter = parameter;
        }

        public void actionPerformed(ActionEvent e) {
            JDialog jd = new JDialog();
            jd.setDefaultCloseOperation(1);

            JOptionPane jop = new JOptionPane();

            String name = JOptionPane.showInputDialog(null, "enter value ("+dataType+"):", "INFO", JOptionPane.INFORMATION_MESSAGE);

            if(name != null){

                name = name.trim();

                if(name.length() > 0){
                    model.addElement(name);

                    if(parameter instanceof PrimitiveParameter){
                        ((PrimitiveParameter) parameter).getValues().add(name);
                    }else if(parameter instanceof ObjectParameter){
                        ((ObjectParameter) parameter).getPossibleObjectTypes().add(name);
                    }else if(parameter instanceof EnumParameter){
                        ((EnumParameter) parameter).getPossibleEnums().add(name);
                    }
                }
            }


            //System.out.println(name);

            //jd.add(jop);
        }
    }

    class removeValueListener extends AbstractAction {

        JList<String> list;
        DefaultListModel<String> model;
        String dataType;
        Parameter parameter;

        public removeValueListener(JList<String> list, String dataType, DefaultListModel<String> model,  Parameter parameter) {
            this.dataType = dataType;
            this.list = list;
            this.model = model;
            this.parameter = parameter;
        }

        public void actionPerformed(ActionEvent e) {

            int selectedIndex = list.getSelectedIndex();


            if(model.getSize() > selectedIndex && selectedIndex >= 0){

                String value = model.get(selectedIndex);

                if(parameter instanceof PrimitiveParameter){
                    if(((PrimitiveParameter) parameter).getValues().contains(value)){
                        ((PrimitiveParameter) parameter).getValues().remove(value);
                    }
                }else if(parameter instanceof ObjectParameter){
                    ((ObjectParameter) parameter).getPossibleObjectTypes().add(value);
                }else if(parameter instanceof EnumParameter){
                    ((EnumParameter) parameter).getPossibleEnums().add(value);
                }

                model.remove(selectedIndex);
            }
        }
    }
}
