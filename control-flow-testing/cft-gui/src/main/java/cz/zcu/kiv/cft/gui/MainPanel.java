package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import cz.zcu.kiv.cft.output.*;
import cz.zcu.kiv.cft.output.data.*;
import cz.zcu.kiv.cft.data.generator.DataGenerator;
import cz.zcu.kiv.cft.data.generator.DataGeneratorImpl;
import cz.zcu.kiv.cft.test.generator.TestGenerator;
import cz.zcu.kiv.cft.test.generator.TestGeneratorImpl;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.InputStream;
import java.util.*;
import java.util.List;

public class MainPanel extends JPanel {

    private MenuBar menuBar;
    private ConditionTree conditionsTree;
    private JPanel firstPanel;
    private JLabel trueLabel;
    private JLabel falseLabel;

    private List<ParameterPanal> parameterPanals = new ArrayList<>();

    public MainPanel(MenuBar menuBar){
        this.menuBar = menuBar;
        this.startBacground();
    }

    public void initLoading(){

        this.removeAll();

        try{
            ImageIcon loading = new ImageIcon(getClass().getClassLoader().getResource("icons/ajax-loader.gif"));

            this.setLayout(new FlowLayout());
            this.add(new JLabel("loading... ", loading, JLabel.CENTER));
            this.setBorder(new EmptyBorder(this.getHeight()/2 - 50,0,0,0));
            this.updateUI();
            this.repaint();

        }catch (Exception e){
            e.printStackTrace();
        }


        this.updateUI();
        this.repaint();
    }

    public void initControlFlow(ControlFlowAnalysisOutput controlFlowAnalysisOutput) {
        if(controlFlowAnalysisOutput != null){


            JMenuItem menuItem = menuBar.getMenu(2);

            JMenuItem showGraph = ((JMenu) menuItem).getItem(0);
            showGraph.setEnabled(true);

            showGraph = ((JMenu) menuItem).getItem(1);
            showGraph.setEnabled(true);

            this.removeAll();
            controlFlowBacground(controlFlowAnalysisOutput);
            this.updateUI();


            DataGenerator dataGenerator = new DataGeneratorImpl();
            dataGenerator.generateData(controlFlowAnalysisOutput);
            System.out.println(OutputConverter.convertToXml(controlFlowAnalysisOutput));

        } else {
            this.removeAll();
            this.setBorder(new EmptyBorder(0,0,0,0));
            startBacground();
            this.updateUI();
        }
    }

    public void startBacground(){
        try{
            InputStream stream = getClass().getClassLoader().getResourceAsStream("icons/control-flow-background.png");
            ImageIcon icon = new ImageIcon(ImageIO.read(stream));

            int original_width = icon.getIconWidth();
            int original_height = icon.getIconHeight();
            int bound_width = 600;
            int bound_height = 500;
            int new_width = original_width;
            int new_height = original_height;

            // first check if we need to scale width
            if (original_width > bound_width) {
                //scale width to fit
                new_width = bound_width;
                //scale height to maintain aspect ratio
                new_height = (new_width * original_height) / original_width;
            }

            // then check if we need to scale even with the new height
            if (new_height > bound_height) {
                //scale height to fit instead
                new_height = bound_height;
                //scale width to maintain aspect ratio
                new_width = (new_height * original_width) / original_height;
            }

            Image image = icon.getImage().getScaledInstance(new_width, new_height,  java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(image);

            JLabel thumb = new JLabel(icon, JLabel.CENTER);

            JPanel nameLabelPanel = new JPanel();
            nameLabelPanel.setPreferredSize(new Dimension(new_width, new_height));
            nameLabelPanel.add(thumb);

            Dimension dimension = new Dimension(new_height, new_height);

            thumb.setMinimumSize(dimension);
            thumb.setPreferredSize(dimension);
            thumb.setMaximumSize(dimension);

            //thumb.setIcon(icon);
            //mainTb.add(thumb, BorderLayout.CENTER);

            //Border border = BorderFactory.createLineBorder(Color.BLUE, 5);
            //thumb.setBorder(border);

            this.add(nameLabelPanel);

        }catch (Exception e){
            //TODO
            System.out.println(e.toString());
        }
    }

    private void controlFlowBacground(ControlFlowAnalysisOutput controlFlowAnalysisOutput){
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.setLayout(new BorderLayout(0, 0));


        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, "Info", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        this.add(panel, BorderLayout.NORTH);
        panel.setLayout(new BorderLayout(0, 0));

        JPanel panel_16 = new JPanel();
        panel.add(panel_16, BorderLayout.WEST);
        panel_16.setLayout(new GridLayout(2, 0, 0, 0));

        JPanel panel_18 = new JPanel();
        panel_16.add(panel_18);

        JLabel lblNewLabel = new JLabel("class:");
        panel_18.add(lblNewLabel);

        JLabel lblPerson = new JLabel(controlFlowAnalysisOutput.getClasses().get(0).getClassName());
        panel_18.add(lblPerson);

        JPanel panel_19 = new JPanel();
        panel_16.add(panel_19);

        JLabel lblNewLabel_1 = new JLabel("method:");
        panel_19.add(lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel(controlFlowAnalysisOutput.getClasses().get(0).getMethods().get(0).getName());
        panel_19.add(lblNewLabel_2);

        JPanel panel_17 = new JPanel();
        panel.add(panel_17);
        panel_17.setLayout(new GridLayout(2, 1, 0, 0));

        JPanel panel_20 = new JPanel();
        panel_17.add(panel_20);
        panel_20.setLayout(new GridLayout(0, 1, 0, 0));

        trueLabel = new JLabel("");
        trueLabel.setBorder( new EmptyBorder(0, 30,0,0));
        panel_20.add(trueLabel);

        JPanel panel_21 = new JPanel();
        panel_17.add(panel_21);
        panel_21.setLayout(new GridLayout(0, 1, 0, 0));

        falseLabel = new JLabel("");
        falseLabel.setBorder( new EmptyBorder(0, 30,0,0));
        panel_21.add(falseLabel);

        JPanel panel_1 = new JPanel();
        this.add(panel_1, BorderLayout.CENTER);
        panel_1.setLayout(new BorderLayout(0, 0));

        JPanel panel_2 = new JPanel();
        panel_1.add(panel_2, BorderLayout.WEST);
        panel_2.setLayout(new BorderLayout(0, 0));

        JPanel panel_5 = new JPanel();
        panel_2.add(panel_5, BorderLayout.NORTH);
        panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JLabel lblParameters = new JLabel("  Control flow conditions");
        panel_5.add(lblParameters);

        JPanel panel_6 = new JPanel();
        panel_2.add(panel_6, BorderLayout.CENTER);
        panel_6.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setPreferredSize(new Dimension(300, 0));
        panel_6.add(scrollPane_1, BorderLayout.CENTER);

        conditionsTree = new ConditionTree(controlFlowAnalysisOutput, this);
        scrollPane_1.setViewportView(conditionsTree);

        JPanel panel_3 = new JPanel();
        panel_1.add(panel_3, BorderLayout.CENTER);
        panel_3.setLayout(new BorderLayout(0, 0));

        JPanel panel_4 = new JPanel();
        panel_3.add(panel_4, BorderLayout.NORTH);
        panel_4.setLayout(new FlowLayout());

        JLabel lblOutput = new JLabel("output");
        panel_4.add(lblOutput);

        JPanel panel_7 = new JPanel();
        panel_3.add(panel_7, BorderLayout.SOUTH);
        panel_7.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        //JButton btnCheck = new JButton("check");
        //panel_7.add(btnCheck);

        JButton btnSaveData = new JButton("export test data");
        btnSaveData.addActionListener(new saveData(controlFlowAnalysisOutput));
        panel_7.add(btnSaveData);

        JButton btnSaveTest = new JButton("export test class");
        btnSaveTest.addActionListener(new saveAndGenerateTest(controlFlowAnalysisOutput));
        panel_7.add(btnSaveTest);

        JScrollPane scrollPane = new JScrollPane();
        panel_3.add(scrollPane, BorderLayout.CENTER);

        JPanel panel_8 = new JPanel();
        scrollPane.setViewportView(panel_8);

        panel_8.setLayout(new GridLayout(0, 1, 0, 0));
        JPanel panel_9 = new JPanel();

        panel_8.add(panel_9);
        panel_9.setLayout(new BorderLayout(0, 0));

        JPanel panel_11 = new JPanel();
        panel_9.add(panel_11, BorderLayout.NORTH);
        panel_11.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        firstPanel = initVariablePanel(controlFlowAnalysisOutput);
        panel_9.add(firstPanel, BorderLayout.CENTER);
    }

    private JPanel initVariablePanel(ControlFlowAnalysisOutput controlFlowAnalysisOutput){
        JPanel truePanel = new JPanel();

        int pc = parameterCount(controlFlowAnalysisOutput);

        truePanel.setLayout(new GridLayout(1, pc, 0, 0));

        for(Parameter p: controlFlowAnalysisOutput.getClasses().get(0).getMethods().get(0).getMethodParameter()){
            initParameterPanal(p, truePanel, null);
        }

        //truePanel.add(list);
        return truePanel;
    }


    private void initParameterPanal(Parameter parameter, JPanel parrent, String fullName){
        if(parameter instanceof PrimitiveParameter){
            ParameterPanal panal = new ParameterPanal(parameter, parrent, fullName);
            this.parameterPanals.add(panal);

        }else if(parameter instanceof ObjectParameter){

            ParameterPanal panal = new ParameterPanal(parameter, parrent, fullName);
            this.parameterPanals.add(panal);

            if (fullName == null || fullName.length() == 0) {
                fullName = parameter.getName();
            } else{
                fullName = fullName+"."+parameter.getName();
            }


            for(Parameter par : ((ObjectParameter)parameter).getObjects()){
                initParameterPanal(par, parrent, fullName);
            }

            for(Parameter par : ((ObjectParameter)parameter).getPrimitiveFields()){
                initParameterPanal(par, parrent, fullName);
            }
        }
    }

    public void selectCondition(String trueString, String falseString){
        trueLabel.setText("true:  " + trueString);
        falseLabel.setText("false: " + falseString);
        repaint();
    }

    private int parameterCount(ControlFlowAnalysisOutput cfo){
        int output = 0;

        for(Parameter p :cfo.getClasses().get(0).getMethods().get(0).getMethodParameter()){
            output += parameterCount(p);
        }

        return output;
    }

    private int parameterCount(Parameter p){
        int output = 0;

        if(p instanceof PrimitiveParameter){
            output++;
        }else if(p instanceof ObjectParameter){
            output += ((ObjectParameter)p).getPrimitiveFields().size();

            for(Parameter par : ((ObjectParameter)p).getObjects()){
                output += parameterCount(par);
            }
        }

        return output;
    }

    class saveAndGenerateTest extends AbstractAction {

        ControlFlowAnalysisOutput cfao;

        public saveAndGenerateTest(ControlFlowAnalysisOutput cfao) {
            this.cfao = cfao;
        }

        public void actionPerformed(ActionEvent e) {
            //System.out.println("aaa");

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setMultiSelectionEnabled(false);

            fileChooser.setCurrentDirectory(new java.io.File("."));
            fileChooser.setDialogTitle("Generate test class");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //
            // disable the "All files" option.
            //
            fileChooser.setAcceptAllFileFilterUsed(false);
            JFrame frame = new JFrame();
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                TestGenerator testGenerator = new TestGeneratorImpl(true);

                //TODO podle systému
                String path = fileChooser.getSelectedFile()+"/";
                path = path.replace("./", "");

                testGenerator.generateClassFor(cfao,  path);
            }
            else {
                System.out.println("No Selection ");
            }




            // testGenerator.generateClassFor(cf);
            //

            //System.out.println(name);

            //jd.add(jop);
        }
    }

    class saveData extends AbstractAction {

        ControlFlowAnalysisOutput cfao;

        public saveData(ControlFlowAnalysisOutput cfao) {
            this.cfao = cfao;
        }

        public void actionPerformed(ActionEvent e) {
            //System.out.println("aaa");

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setMultiSelectionEnabled(false);

            fileChooser.setCurrentDirectory(new java.io.File("."));
            fileChooser.setDialogTitle("export test data");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //
            // disable the "All files" option.
            //
            fileChooser.setAcceptAllFileFilterUsed(false);
            JFrame frame = new JFrame();
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {

                //TODO podle systému vyzkoušet na windows
                String path = fileChooser.getSelectedFile()+"/";
                path = path.replace("./", "");

                OutputConverter.convertToJsonFile(cfao, path + "output.json");
                OutputConverter.convertToXmlFile(cfao, path + "output.xml");

            }
            else {
                System.out.println("No Selection ");
            }




            // testGenerator.generateClassFor(cf);
            //

            //System.out.println(name);

            //jd.add(jop);
        }
    }
}
