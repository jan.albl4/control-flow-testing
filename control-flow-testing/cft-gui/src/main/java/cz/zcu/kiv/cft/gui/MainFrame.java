package cz.zcu.kiv.cft.gui;

import cz.zcu.kiv.cft.Configuration;
import cz.zcu.kiv.cft.analyzer.ControlFlowGraph;
import cz.zcu.kiv.cft.output.data.ControlFlowAnalysisOutput;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private MainPanel centerPanel;

    private MenuBar menuBar;

    private Configuration actualConfiguratin = null;


    public void initControlFlowOutput(ControlFlowAnalysisOutput controlFlowAnalysisOutput){
        centerPanel.initControlFlow(controlFlowAnalysisOutput);
    }

    public void initLoading(){
        centerPanel.initLoading();
    }

    public MainFrame(){
        setTitle("Control flow testing");
        setSize(800,600);
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(800,600));

        Container pane = getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

        menuBar = new MenuBar(this);
        this.setJMenuBar(menuBar);

        centerPanel = new MainPanel(menuBar);
        add(centerPanel);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void setActualConfiguratin(Configuration actualConfiguratin) {
        this.actualConfiguratin = actualConfiguratin;
    }

    public Configuration getActualConfiguratin() {
        return actualConfiguratin;
    }


}
